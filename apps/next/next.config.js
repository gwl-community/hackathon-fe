/** @type {import('next').NextConfig} */

const { withNativebase } = require('@native-base/next-adapter');
module.exports = {
  images: {
    domains: ['images.netskill.com', 's3.ap-south-1.amazonaws.com', 'page-generator-images.netskill.com'],
  },
}
module.exports = withNativebase({
  dependencies: [
    '@expo/next-adapter',
    'react-native-vector-icons',
    'react-native-vector-icons-for-web',
    "react-native-web-linear-gradient",
    'solito',
    'app',
    '@react-native-async-storage/async-storage',
    '@react-native-picker/picker',
    '@react-navigation/drawer',
    '@react-navigation/native',
    '@react-navigation/native-stack',
    "antd",
    // "expo-image-picker",
    "html-react-parser",
    "react-helmet",
    "react-native-element-dropdown",
    "react-native-gesture-handler",
    "react-native-keyboard-aware-scroll-view",
    "react-native-progress",
    "react-native-reanimated",
    "react-native-render-html",
    "react-native-tab-view",
    "react-phone-input-2",
    "react-player",
    "react-rte",
    "@expo/vector-icons",
    "expo-linear-gradient",
    'react-razorpay',
    'react-images-uploading'

  ],
  nextConfig: {
    // projectRoot: __dirname,
    // reactStrictMode: true,
    // webpack5: true,
    webpack: (config, options) => {
      config.module.rules.push({
        test: /\.ttf$/,
        loader: "url-loader",
      });
      config.resolve.alias = {
        ...(config.resolve.alias || {}),
        'react-native$': 'react-native-web',
        '@expo/vector-icons': 'react-native-vector-icons',
        "react-native-linear-gradient": "react-native-web-linear-gradient",
        "expo-image-picker":'expo-image-picker'
      }
      config.resolve.extensions = [
        '.web.js',
        '.web.ts',
        '.web.tsx',
        ...config.resolve.extensions,
      ]
      return config
    },
    async rewrites() {
      return [
        {
          source: '/college',
          destination: '/eee',
        },
      ]
    },
  },
})
