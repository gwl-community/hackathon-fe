// import TestPage from "app/features/TestPage";
// export default TestPage;
import dynamic from 'next/dynamic'
import { getBaseUrls } from 'app/lib/apiConfig'
import page_generator_api from 'app/lib/page_api'
const Test = dynamic(() => import('app/features/TestPage'), { ssr: true })
export default function Tes({ seo }) {
  return (
    <Test
      testData={seo?.data?.data?.[0]?.attributes}
      cmsBaseUrl={seo?.cmsBaseUrl}
      pageGeneratorBaseUrl={seo?.pageGeneratorBaseUrl}
    />
  )
}
export const getServerSideProps = async (context) => {
  const { params, req, res } = context
  const { cmsBaseUrl, pageGeneratorBaseUrl } = getBaseUrls(
    context?.req?.headers?.host
  )
  let { data } = await page_generator_api.get(
    `${pageGeneratorBaseUrl}api/tests-pages?filters[slug]=${params?.slugName}`
  )

  return {
    props: {
      // this will get passed to pageProps
      seo: {
        courseName:
          params?.slugName === 'fullstack-development-test'
            ? 'Full Stack Developer Assessment Test & Exam Online'
            : params?.slugName === 'frontend-development-test'
            ? 'Front End Developer Certification Assessment Test & Exam | NetSkill'
            : params?.slugName === 'backend-development-test'
            ? 'Backend Development Test | Test Structure, Skills Required & Jobs'
            : '',
        description:
          params?.slugName === 'fullstack-development-test'
            ? 'Take test to get hired for Full Stack Developer Assessment Online which offers CTC upto 30 Lacs with 200+ Jobs opportunities & 50+ Hiring Partners.'
            : params?.slugName === 'frontend-development-test'
            ? 'Take test to get hired for Front End Developer Certification Assessment Online which offers CTC upto 26 Lacs with 150+ Jobs opportunities & 50+ Hiring Partners.'
            : 'Take test to get hired for Backend Development Test Online which offers CTC upto 30 Lacs with 250+ Jobs opportunities & 50+ Hiring Partners.',
        data: data,
        cmsBaseUrl: cmsBaseUrl,
        pageGeneratorBaseUrl: pageGeneratorBaseUrl,
      },
    },
    //revalidate: 100,
  }
}
