import 'raf/polyfill'
import { useEffect } from 'react'

import Head from 'next/head'
import { Provider } from 'app/provider'
import React from 'react';
import gtag from "ga-gtag";
import { getBaseUrls } from 'app/lib/apiConfig'

function MyApp({ Component, pageProps, router, baseUrls }) {
  var reqUrl = router.asPath ? router.asPath.split('?') : ['/']
  useEffect(() => {
    if (window) {
      const script = window.document.createElement("script");
      script.src = `https://www.googletagmanager.com/gtag/js?id=AW-10924340029`;
      script.async = true;
      window.document.body.appendChild(script);
      window.dataLayer = window.dataLayer || [];
      gtag("js", new Date());
      gtag("config", "AW-10924340029");
      gtag("event", "page_view");
    }
  }, []);

  useEffect(() => {
    if (window) {
      const script = window.document.createElement("script");
      script.src = `https://www.googletagmanager.com/gtag/js?id=G-F2YCLTGE2B`;
      script.async = true;
      window.document.body.appendChild(script);
      window.dataLayer = window.dataLayer || [];
      // gtag("js", new Date());
      // gtag("config", "G-F2YCLTGE2B");
      gtag("event", "page_view");
    }
  }, []);
  useEffect(() => {
    let s = "smatbot-chatbot";
    if (!document.getElementById("chatbot_id_define")) {
      let script_one = document.createElement("script");
      script_one.id = "chatbot_id_define";
      script_one.innerHTML = "let chatbot_id = 11673;";
      document.head.appendChild(script_one);
    }
    if (!document.getElementById(s)) {
    
      let script = document.createElement("script");
      script.src = "https://smatbot.s3.amazonaws.com/files/smatbot_plugin.js.gz";
      script.async = true;
      document.head.appendChild(script);
      script = document.createElement("script");
      script.src =
        "https://cdnjs.cloudflare.com/ajax/libs/fingerprintjs2/1.5.1/fingerprint2.min.js";
      script.async = true;
      document.head.appendChild(script);
    }
    // }
  }, []);

  useEffect(() => {
    if (window) {
      !(function (f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function () {
          n.callMethod
            ? n.callMethod.apply(n, arguments)
            : n.queue.push(arguments);
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = "2.0";
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s);
      })(
        window,
        document,
        "script",
        "https://connect.facebook.net/en_US/fbevents.js"
      );
      fbq("init", "2007092499489173");
      fbq("track", "PageView");
    }
  }, []);


  return (
    <>
      <Head>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <link
          href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@400;600;700;800&display=swap"
          rel="stylesheet"
        />
        <title>{pageProps?.seo?.courseName}</title>
        <meta name="description" content={pageProps?.seo?.description} />
        <link rel="canonical" href={`https://www.netskill.com${reqUrl[0]}`} />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1.00001, viewport-fit=cover"
        />
        <meta
          name="google-site-verification"
          content="tt_fasyXz2IuJtvhY4uY_ciQZq4jUNsF9c8NkmDvUlM"
        />
        <link rel="icon" href="/netskill_favicon.png" />
      </Head>
      <Provider>
        <Component router={router} {...pageProps} baseUrls={baseUrls} />
      </Provider>
    </>
  )
}

MyApp.getInitialProps = async ({ ctx }) => {
  const { req } = ctx
  const baseUrls = getBaseUrls(req?.headers?.host)
  return { baseUrls };
}

export default MyApp
