import dynamic from 'next/dynamic'
import { getBaseUrls } from 'app/lib/apiConfig'
import page_generator_api from 'app/lib/page_api';
const CommunityS = dynamic(()=>import('app/features/Community/index'),{ssr:true})
function PlacementS({ seo, baseUrls }) {
  return (
   
    <>
     {/* <script
        type="application/ld+json"
        dangerouslySetInnerHTML={{
          __html: ` {
            "@context": "http://schema.org",
            "@type": "Organization",
            "name": "Netskill",
            "description": "Netskill offers Web Development & Programming Certification Courses Online with 100% placement record at 100+ top companies. Select courses, learn by mentors & get certified",
            "url": "https://www.netskill.com/",
            "logo": "https://images.netskill.com/frontend/netskill/b2b/netskill_updated_logo.webp",
            "email": "hello@netskill.com",
            "address": [
            {
                            "@type": "PostalAddress",
                            "addressCountry": "IN",
                            "postalCode": "560066",
                            "streetAddress": "3rd Floor, GoodWorks Cowork, Akshay Tech Park, EPIP Zone, Whitefield, Bengaluru - 560066, Karnataka, Indial "
            },
            
            ],
            "contactPoint" : [
                {
                    "@type" : "ContactPoint",
                    "contactType" : "customer service",
                    "email": "hello@netskill.com",
                    "url": "https://www.netskill.com"
                }
            ]
        }
  `,
        }}
      ></script> */}
      <CommunityS  baseUrls={baseUrls} />
    </>
  )
}

export default PlacementS
export async function getServerSideProps(context) {
  const { pageGeneratorBaseUrl } = getBaseUrls(context?.req?.headers?.host)
  // let { data } = await page_generator_api.get(`${pageGeneratorBaseUrl}api/home`)

  return {
    props: {
      seo: {
        courseName:
          'Netskill: Web Development & Programming Certification Courses Online',
        description: 'Netskill offers Web Development & Programming Certification Courses Online with 100% placement record at 100+ top companies. Select courses, learn by mentors & get certified',
        // data: data,
        // pageGeneratorBaseUrl: pageGeneratorBaseUrl,
      },
    },
  }
}
