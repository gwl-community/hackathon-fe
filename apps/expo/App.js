import { NativeBaseProvider, Text, VStack } from "native-base";
import {
  NunitoSans_400Regular,
  NunitoSans_600SemiBold,
  NunitoSans_700Bold,
  NunitoSans_800ExtraBold,
  useFonts,
} from "@expo-google-fonts/nunito-sans";
import React, { Suspense, useEffect } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createDrawerNavigator } from "@react-navigation/drawer";
import theme from "app/config/theme";


const TestPage = React.lazy(() => import('app/screens/TestPage'));
const Loader = React.lazy(() => import('app/components/Loader'))
const Community = React.lazy(() => import("app/screens/Community"))

const Drawer = createDrawerNavigator();
console.reportErrorsAsExceptions = false;

const config = {
  screens: {
    TestPage: "test/:slug",
    Community:"community"
  },
};

export default function App() {
  
  // useEffect(() => {
  //   let s = "smatbot-chatbot";
  //   if (!document.getElementById("chatbot_id_define")) {
  //     let script_one = document.createElement("script");
  //     script_one.id = "chatbot_id_define";
  //     script_one.innerHTML = "let chatbot_id = 11673;";
  //     document.head.appendChild(script_one);
  //   }
  //   if (!document.getElementById(s)) {
  //     let script = document.createElement("script");
  //     script.src = "https://smatbot.s3.amazonaws.com/files/smatbot_plugin.js.gz";
  //     script.async = true;
  //     document.head.appendChild(script);
  //     script = document.createElement("script");
  //     script.src =
  //       "https://cdnjs.cloudflare.com/ajax/libs/fingerprintjs2/1.5.1/fingerprint2.min.js";
  //     script.async = true;
  //     document.head.appendChild(script);
  //   }
  //   // }
  // }, []);

 
 

  let [fontsLoaded, error] = useFonts({
    NunitoSans_400Regular,
    NunitoSans_600SemiBold,
    NunitoSans_700Bold,
    NunitoSans_800ExtraBold,
  });

  return (
    <NativeBaseProvider theme={theme}>
      {!fontsLoaded ? (
        <Suspense fallback={<></>}>
          <Loader />
        </Suspense>
      ) : (
        <Suspense fallback={<></>}>
          <NavigationContainer linking={{ config }}>
            <Drawer.Navigator screenOptions={{ headerShown: false, lazy: true }}>
           
              <Drawer.Screen
              name={"Community"}
              component={Community}
          />
              <Drawer.Screen name={"TestPage"} component={TestPage} />
            
            </Drawer.Navigator>
          </NavigationContainer>
        </Suspense>
      )}
    </NativeBaseProvider>
  );
}
