# Install dependencies only when needed
FROM node:16.17-alpine AS deps
# Check https://github.com/nodejs/docker-node/tree/b4117f9333da4138b03a546ec926ef50a31506c3#nodealpine to understand why libc6-compat might be needed.
RUN apk add --no-cache libc6-compat
WORKDIR /app
COPY . .
RUN yarn
RUN yarn run web-build

# Rebuild the source code only when needed
# FROM node:16.17-alpine AS builder
# WORKDIR /app
# COPY --from=deps /app/apps/next/node_modules ./node_modules
# RUN yarn build --no-lint && yarn install --production --ignore-scripts --prefer-offline

# Production image, copy all the files and run next
#FROM node:16.17-alpine AS runner
#WORKDIR /app

ENV NODE_ENV production

RUN addgroup -g 1001 -S nodejs
RUN adduser -S nextjs -u 1001

# You only need to copy next.config.js if you are NOT using the default configuration
#COPY --from=deps /app/apps/next/next.config.js ./
#COPY --from=deps /app/apps/next/public ./public
#COPY --from=deps --chown=nextjs:nodejs /app/apps/next/.next ./.next
#COPY --from=deps /app/apps/next/node_modules ./node_modules
#COPY --from=deps /app/apps/next/package.json ./package.json
COPY . .
USER nextjs

EXPOSE 5000

# Next.js collects completely anonymous telemetry data about general usage.
# Learn more here: https://nextjs.org/telemetry
# Uncomment the following line in case you want to disable telemetry.
# ENV NEXT_TELEMETRY_DISABLED 1

CMD ["yarn","start"]
