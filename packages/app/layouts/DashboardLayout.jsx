import {
  FontAwesome,
} from "@expo/vector-icons";
import { Link } from 'solito/link'
import { useRouter } from 'solito/router'
import {
  Avatar,
  Box,
  Button,
  Divider,
  HStack,
  Hidden,
  Modal,
  Pressable,
  ScrollView,
  StatusBar,
  Text,
  VStack,
  useColorMode,
  useToast
} from "native-base";
import React, { useEffect, useState } from "react";
import { clearData, getData, storeData } from "../lib/utils";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import api from "../lib/api";
import { TouchableOpacity, View } from "react-native";
// import CustomTextField from "../features/CoursePreview/components/CustomTextField";
import Image from 'next/image';
const externaImageLoader = ({ src }: { src: string }) =>
  `${src}`;
export function Sidebar({ pageName, navigation, handleLogout, userData }) {
  const [image, setImage] = useState({ uri: 'https://images.netskill.com/frontend/netskill/home/user.png' });
  const [showImg, setShowImg] = useState(true);
  const router = useRouter()
  const { push } = useRouter()

  const list = [
    {
      iconName: "https://images.netskill.com/frontend/userIconSmall.webp.webp",
      iconText: "Profile",
      selected: pageName == "profile",
      pageName: "profile",
    },

    {
      iconName: "https://images.netskill.com/frontend/settingIcon.webp.webp",
      iconText: "Settings",
      iconColorLight: "primary.900",
      textColorLight: "primary.900",
      iconColorDark: "violet.500",
      textColorDark: "violet.500",
      pageName: "settings",
      selected: pageName == "settings",
    },
    // {
    //   iconName: "notifications-none",
    //   iconText: "MemberShip",
    //   pageName: "MemberShip",
    //   selected: pageName == "MemberShip",
    // },
    // {
    //   iconName: "shield",
    //   iconText: "Privacy Policy",
    // },
    {
      iconName: "https://images.netskill.com/frontend/helpSupport.webp",
      iconText: "Help & Support",
      pageName: "help-support",
      selected: pageName == "help",
    },
    //  {
    //     iconName: "shopping-bag",
    //     iconText: "Payment",
    //     pageName: "payment",
    //     selected: pageName == "payment",
    //   },
  ];

  return (
    <Box
      w="80"
      borderRightWidth="1"
      display="flex"
      _light={{
        bg: "#17181a",
        borderRightColor: "coolGray.200",
      }}
      _dark={{
        bg: "#17181a",
        borderRightColor: "coolGray.800",
      }}
    >
      <ScrollView>
        <VStack
          bg="#17181a"
          pb="40px"
          mt="10"
          space="3"
          alignItems="center"
          borderBottomWidth="1"
          _light={{
            borderBottomColor: "#17181a",
          }}
          _dark={{
            borderBottomColor: "#17181a",
          }}
        >
          {showImg ? (
            <Avatar
              resizeMode="cover"
              source={image}
              width={{
                base: 20,
                md: 40,
              }}
              height={{
                base: 20,
                md: 40,
              }}
            />
          ) : null}
          <HStack alignItems="center" justifyContent="center" space="2">
            <Text
              fontFamily="Nunito Sans"
              fontSize="xl"
              fontWeight="bold"
              _light={{
                color: "coolGray.800",
              }}
            >
              {userData?.user?.username}
            </Text>
          </HStack>
        </VStack>
        <Divider
          _dark={{
            bgColor: "coolGray.800",
          }}
        />
        <VStack px="4" py="4">
          {list.map((item, idx) => {
            return (
              <Button
                key={idx}
                variant="raised"
                justifyContent="flex-start"
                py="4"
                px="5"
                onPress={() =>

                  item.pageName && push(item.pageName)
                }
                borderRadius="4"
                _light={
                  item.selected && {
                    bg: "#19bf79",
                  }
                }
                _dark={
                  item.selected && {
                    bg: "#19bf79",
                    _pressed: {
                      bg: "#19bf79",
                    },
                  }
                }
              >
                <HStack space="4" alignItems="center">
                  <Image
                    loader={externaImageLoader}
                    alt={"image"}
                    width={"6px"}
                    height={"6px"}
                    layout={"fixed"}
                    src={item.iconName}
                  />
                  <Text
                    fontFamily="Nunito Sans"
                    fontSize="md"
                    fontWeight="medium"
                    _dark={{
                      color: "coolGray.50",
                    }}
                    _light={{
                      color: "coolGray.800",
                    }}
                  >
                    {item.iconText}
                  </Text>
                </HStack>
              </Button>
            );
          })}
        </VStack>
        <Divider
          _dark={{
            bgColor: "coolGray.800",
          }}
        />
        <Box px="4" py="2">
          <Button
            variant="raised"
            justifyContent="flex-start"
            py="4"
            px="5"
            onPress={handleLogout}
          >
            <HStack space="4" alignItems="center">
              <Image
                loader={externaImageLoader}
                alt={"image"}
                width={"15px"}
                height={"15px"}
                layout={"fixed"}
                src={'https://images.netskill.com/frontend/logout.webp'}
              />
              <Text
                fontFamily="Nunito Sans"
                fontSize="md"
                fontWeight="medium"
                _dark={{
                  color: "coolGray.50",
                }}
                _light={{
                  color: "coolGray.800",
                }}
              >
                Logout
              </Text>
            </HStack>
          </Button>
        </Box>
      </ScrollView>
    </Box>
  );
}
export function Header(props) {
  const name = props?.userData?.user?.username;
  const initials = name
    ? name
      .match(/(\b\S)?/g)
      .join("")
      .match(/(^\S|\S$)?/g)
      .join("")
      .toUpperCase()
    : null;
  // const { colorMode } = useColorMode();
  // const [modal, showModal] = useState(false);
  // const [userCoords, setUserCoords] = useState(0);
  return (
    <Box
      zIndex={2}
      h="70"
      justifyContent={"center"}
      borderBottomWidth="1"
      _dark={{
        bg: "#17181a",
        borderColor: "#374151",
      }}
      _light={{
        bg: {
          base: "#17181a",
          md: "#17181a",
        },
        borderColor: "#374151",
      }}
    >
      <VStack
        alignSelf="center"
        width="100%"
        maxW={props.menuButton ? null : "1440px"}
      >
        <HStack alignItems="center" justifyContent="space-between">
          <HStack alignItems="center" paddingLeft={100}>
            <Link href={props.userData ? '/mycourses' : '/'}>
              <Button variant={"raised"}>
                <Image
                  loader={externaImageLoader}
                  alt={"image"}
                  width={"110px"}
                  height={"34px"}
                  layout={"fixed"}
                  src={"https://images.netskill.com/frontend/community.png"}
                />
              </Button>
            </Link>
            {/* <Link href='/courses'>
              <Button
                variant="raised"
                justifyContent="flex-start"
                py="4"
                px="5"
                _hover={{
                  _text: { color: "rose.400" },
                }}
                _text={{
                  color: "#B0B5C0",
                  fontFamily: "Nunito Sans",
                  fontSize: '14px'

                }}
              >
                Explore Courses
              </Button>
            </Link> */}
            {/* <Link href='/interview-prep'>
              <Button
                variant="raised"
                justifyContent="flex-start"
                py="4"
                px="5"
                _hover={{
                  _text: { color: "rose.400" },
                }}
                _text={{
                  color: "#B0B5C0",
                  fontFamily: "Nunito Sans"

                }}
              >
                Interview Prep
              </Button>
            </Link> */}

            {/* <Link href='/community'>
              <Button
                variant="raised"
                justifyContent="flex-start"
                // py="4"
                ml="30px"
                _hover={{
                  _text: { color: "rose.400" },
                }}
                _text={{
                  color: "#B0B5C0",
                  fontFamily: "Nunito Sans"
                }}
              >
               Community
              </Button>
            </Link> */}

            {/* <Link href={props.userData ? '/mycourses' : '/colleges'}>
              <Button
                variant="raised"
                justifyContent="flex-start"
                py="4"
                px="5"
                _hover={{
                  _text: { color: "rose.400" },
                }}
                _text={{
                  color: "#B0B5C0",
                  fontFamily: "Nunito Sans",
                  fontSize: '14px'

                }}
              >
                {props.userData ? "My Courses" : "For Colleges & Orgs"}
              </Button>
            </Link> */}
            {/* <Link href={props.userData ? '/mycourses' : '/colleges'}> */}
            {/* {props.userData ?
              <Button
                onPress={() => props?.setIssueModal(true)}
                variant="raised"
                justifyContent="flex-start"
                py="4"
                px="5"
                _hover={{
                  _text: { color: "rose.400" },
                }}
                _text={{
                  color: "#B0B5C0",
                  fontFamily: "Nunito Sans",
                  fontSize: '14px'

                }}
              >
                {props.userData ? "Report an issue" : ""}
              </Button>
              : null} */}
            {/* </Link> */}
          </HStack>
          {/* {props.searchbar && (
            <Input
              px="4"
              w="30%"
              size="sm"
              placeholder="Search"
              InputLeftElement={
                <Icon
                  px="2"
                  size="4"
                  name={"search"}
                  as={FontAwesome}
                  _light={{
                    color: "coolGray.400",
                  }}
                  _dark={{
                    color: "coolGray.100",
                  }}
                />
              }
            />
          )} */}
          {/* {props.userData ? (
            <HStack alignItems="center">
              <Box>
                <IconButton
                  onPress={() => showModal(!modal)}
                  variant="raised"
                  colorScheme="light"
                  icon={
                    <HStack
                      onLayout={(e) => {
                        setUserCoords(e.nativeEvent.layout.left);
                      }}
                      alignItems="center"
                    >
                      <Box
                        h="10"
                        w="10"
                        borderRadius="40"
                        bg="white"
                        justifyContent="center"
                        alignItems="center"
                        p="4"
                      >
                        <Text
                          fontFamily="Nunito Sans"
                          fontSize="md"
                          fontWeight="500"
                          color="black"
                        >
                          {initials}
                        </Text>
                      </Box>
                      <Image
                        loader={externaImageLoader}
                        alt={"image"}
                        width={"18px"}
                        height={"18px"}
                        layout={"fixed"}
                        src={"https://images.netskill.com/frontend/whiteDown.webp"}
                      />

                    </HStack>
                  }
                />
              </Box>
              {modal ?
                <VStack bg={"#1d1e21"} position={"fixed"} top={"75"} w={"140px"} zIndex={100} right={0}>
                  <Button
                    h={12}
                    justifyContent={"center"}
                    onPress={() => {
                      props.handleAccount();
                      showModal(false);
                    }}
                    w={"180px"}
                    variant={"raised"}
                    _hover={{
                      bg: "#26292e",
                    }}
                    py="4"
                  >
                    <Text
                      fontFamily="Nunito Sans"
                      w={"100px"}
                      position={"absolute"}
                      right={-35}
                      top={-8}
                    >
                      Account
                    </Text>
                  </Button>
                  <Button
                    h={12}
                    onPress={() => {
                      props.handleLogout();
                      showModal(false);
                    }}
                    w={"180px"}
                    variant={"raised"}
                    _hover={{
                      bg: "#26292e",
                    }}
                    py="4"
                  >
                    <Text
                      fontFamily="Nunito Sans"
                      w={"100px"}
                      position={"absolute"}
                      right={-35}
                      top={-8}
                    >
                      Logout
                    </Text>
                  </Button>
                </VStack>
                : null}
              <Modal
                height={"140px"}
                isOpen={modal}
                position="fixed"
                // left={userCoords - 15}
                right={"6"}
                top="70"
                w={"140px"}
                onClose={() => showModal(false)}
                _backdrop={{
                  _dark: {
                    bg: "transparent",
                  },
                }}
                bg="#1d1e21"
                size="xl"
                zIndex={10000}
              >
                <VStack w={"140px"}>
                  <Button
                    h={12}
                    justifyContent={"center"}
                    onPress={() => {
                      props.handleAccount();
                      showModal(false);
                    }}
                    w={"180px"}
                    variant={"raised"}
                    _hover={{
                      bg: "#26292e",
                    }}
                    py="4"
                  >
                    <Text
                      fontFamily="Nunito Sans"
                      w={"100px"}
                      position={"absolute"}
                      right={-55}
                      top={-8}
                    >
                      Account
                    </Text>
                  </Button>
                  <Button
                    h={12}
                    onPress={() => {
                      props.handleLogout();
                      showModal(false);
                    }}
                    w={"180px"}
                    variant={"raised"}
                    _hover={{
                      bg: "#26292e",
                    }}
                    py="4"
                  >
                    <Text
                      fontFamily="Nunito Sans"
                      w={"100px"}
                      position={"absolute"}
                      right={-55}
                      top={-8}
                    >
                      Logout
                    </Text>
                  </Button>
                </VStack>
              </Modal>
            </HStack>
          ) : (
            <HStack space={4} px={{ lg: "100px", md: "40px" }}>
              <Link href='/signin'>
                <Button
                  borderColor={"white"}
                  borderWidth="1"
                  h="40px"
                  variant="raised"
                  justifyContent="flex-start"
                  py="4.5"
                  px="5"
                  mr="3"
                  _text={{ fontSize: "14px", fontFamily: "Nunito Sans" }}
                  _hover={{
                    bg: "white",
                    _text: { color: "black" },
                  }}
                >
                  Login
                </Button>
              </Link>
              <Link href='/contact-us'>
                <Button
                  bg="#ff5e63"
                  h="40px"
                  variant="raised"
                  justifyContent="flex-start"
                  py="3"
                  px="6"
                  _hover={{
                    bg: "error.500",
                  }}
                >

                  <Text
                    fontFamily="Nunito Sans"
                    fontSize="14px"
                    fontWeight="medium"
                    _dark={{
                      color: "coolGray.50",
                    }}
                    _light={{
                      color: "coolGray.800",
                    }}
                  >
                    Contact Us
                  </Text>
                </Button>
              </Link>
            </HStack>
          )} */}
        </HStack>
      </VStack>
    </Box>
  );
}

function MainContent(props) {
  return (
    <VStack
      flex={1}
      width={{
        base: "100%",
        md: "100%",
      }}
    >
      {props.displayScreenTitle && (
        <Hidden till="md">
          <HStack alignItems="center">
            <Text
              fontFamily="Nunito Sans"
              fontSize="18"
              color="#f9fafb"
              pl={{ md: "32px" }}
              pt={{ md: "32px" }}
              pb={{ md: "16px" }}
            >
              {props.title}
            </Text>
          </HStack>
        </Hidden>
      )}
      <VStack>{props.children}</VStack>
    </VStack>
  );
}



export function MobileHeader(props) {
  const name = props?.userData?.user?.username;
  const initials = name
    ? name
      .match(/(\b\S)?/g)
      .join("")
      .match(/(^\S|\S$)?/g)
      .join("")
      .toUpperCase()
    : null;
  return (
    <Box
      px="1"
      pt="1"
      pb="1"
      _dark={{
        bg: "#17181a",
        borderColor: "#8C92AC",
      }}
      _light={{
        bg: {
          base: "#17181a",
          md: "#17181a",
        },
        borderColor: "#8C92AC",
      }}
    >
      <HStack space="0" justifyContent="space-between">
        <HStack
          flex="1"
          space="0"
          justifyContent="space-between"
          alignItems="center"
        >
          <>
            {/* <View><Text></Text></View> */}
            {/* {props?.displayLogo ? ( */}

            <Button
              alignSelf="center"
              variant={"raised"}
            >
              <Link href={props.userData ? '/mycourses' : '/colleges'}>
                <Image
                  loader={externaImageLoader}
                  alt={"image"}
                  width={"120px"}
                  height={"20px"}
                  layout={"fixed"}
                  src={"https://images.netskill.com/frontend/netskill/b2b/netskill_updated_logo.webp"}
                />
              </Link>
            </Button>
            <Button
              onPress={() => props.toggleMobileHeader(true)}
              ml="2"
              variant={"raised"}
            >
              <Image
                loader={externaImageLoader}
                alt={"image"}
                width={"28px"}
                height={"28px"}
                layout={"fixed"}
                src={'https://images.netskill.com/frontend/hamb_icon.svg'}
              />

            </Button>
          </>
        </HStack>
      </HStack>
    </Box>
  );
}
export default function DashboardLayout({
  scrollable = true,
  displayScreenTitle = true,
  displaySidebar = true,
  header = {
    searchbar: false,
  },
  mobileHeader = {
    backButton: true,
  },
  navigation,
  pageName,
  hideFooter = false,
  moveToProfile,
  ...props
}) {
  const router = useRouter()
  const { push } = useRouter()
  const toast = useToast();
  const [isSidebarVisible, setIsSidebarVisible] = React.useState(true);
  const [userData, setUserData] = React.useState();
  const [showMobileHeader, setShowMobileHeader] = useState(false);
  const [moveToProfile1, setMoveToProfile] = useState(moveToProfile);
  const [height, setHeight] = useState();
  const [inwdith, setInwidth] = useState();
  const [issueModal, setIssueModal] = useState(false);
  const [issueText, setIssueText] = useState()
  const [issueDesc, setIssueDesc] = useState();
  useEffect(() => {
    const newHeight = window?.innerHeight;
    const newWidth = window?.innerWidth;
    setInwidth(newWidth)
    setHeight(newHeight);
  }
    , [])
  const fetchUserData = async () => {
    let user = await getData("userData", true);
    setUserData(user);
  };
  React.useEffect(() => {
    fetchUserData();
  }, []);
  React.useEffect(async () => {
    let user = await getData("userData", true);
    if (user?.user?.id && moveToProfile1)
      try {
        let { data } = await fetch(
          `api/users?filters[id][$eq]=${user?.user?.id}`)
        const userData = { user: data?.[0] };
        setUserData(userData);
        storeData("userData", JSON.stringify(userData));
        setMoveToProfile(false);
      } catch (error) {
        console.log("error", error);
      }
  }, [moveToProfile1]);

  const toggleMobileHeader = (value) => {
    setShowMobileHeader(value);
  };
  function toggleSidebar() {
    setIsSidebarVisible(!isSidebarVisible);
  }
  const handleLogout = () => {
    clearData();
    push('/signin')
  };
  const handleAccount = () => {
    push('/profile')
  };
  const handleCourse = () => {
    router.push('/courses')
  };
  const handleHome = () => {
    push('/')
  };

  const handleMyCourse = () => {
    router.push('/mycourses')
  };
  const handleFaq = () => {
    router.push('/faq')
  };
  const handleAboutUs = () => {
    push('/about-us')
  };
  const handleContactUs = () => {
    push('/contact-us')
  };
  const handleColleges = () => {
    push('/colleges')
  };

  const handlePlacement = () => {
    push('/community')
  };
  const handleSignup = () => {
    navigation.jumpTo("SignUpEmail");
  };
  const handleLogin = () => {
    push('/signin')
  };
  const handleInterview = () => {
    push('/interview-prep')
  }
  const mobileHeaderList = [
    { text: "Explore Courses", onPress: handleCourse },
    { text: "My Courses", onPress: handleMyCourse },
    { text: "For Colleges & Orgs", onPress: handleColleges },
    { text: "FAQs", onPress: handleFaq },
    { text: "About Us", onPress: handleAboutUs },
    { text: "Account", onPress: handleAccount },
    { text: "Interview Prep", onPress: handleInterview },
    { text: "Placement Tests", onPress: handlePlacement },
  ];
  const closeIssueModal = () => {
    setIssueModal(false);
    setIssueText("")
    setIssueDesc("")
  }
  const submitReport = async () => {
    let user = await getData("userData", true);
    let url = await getData("url", true);
    console.log("-ccc", url)
    try {
      let { data } = await api.post(`${url}api/bug-reports`, {
        data: {
          "feedback": issueText,
          "title": issueDesc,
          "users": user?.user?.id,
          // "course": 17
        },
      });
      toast.show({
        render: () => {
          return <Box bg="emerald.500" px="2" py="1" rounded="sm" mt={30} w="220" alignItems={"center"} justifyContent={"center"} height={"36"} zIndex={200000}>
            Submitted Successfully !!
          </Box>;
        },
        placement: "top",
      });
      closeIssueModal()
    } catch (error) {
      console.log(error);
      toast.show({
        render: () => {
          return <Box bg="red.300" px="2" py="1" rounded="sm" mt={30} w="620" alignItems={"center"} justifyContent={"center"} height={"36"} zIndex={200000}>
            There is an error, Please try again later !!
          </Box>;
        },
        placement: "top",
      });
      closeIssueModal()
    }
  }
  return (
    <>

      <StatusBar
        translucent
        barStyle="light-content"
        backgroundColor="transparent"
      />

      <Box safeAreaTop bg='black' />
      <Box
        zIndex={4}
        w={"100%"}
        overflowY='unset'
        alignSelf={"center"}
        //maxWidth={"1640px"}
        flex={1}
        position={props?.position || "relative"}
        bg={props?.background || "#17181a"}

      >
        <KeyboardAwareScrollView
          contentContainerStyle={{
            width: "100%",
            height: "100%",
          }}
        >
          <Hidden from="md">
            {showMobileHeader ? (
              <></>
            ) : (
              <MobileHeader
                title={props.title}
                backButton={mobileHeader.backButton}
                navigation={navigation}
                menuButton={props.displayMenuButton}
                onBack={props?.onBack}
                searchbar={header.searchbar}
                handleLogout={handleLogout}
                handleAccount={handleAccount}
                handleCourse={handleCourse}
                handleMyCourse={handleMyCourse}
                userData={userData}
                handleLogin={handleLogin}
                handleSignup={handleSignup}
                displayLogo={props?.displayLogo}
                toggleMobileHeader={toggleMobileHeader}
                handleHome={handleHome}
                {...props}
              />
            )}
          </Hidden>
          <Hidden till="md">
            <Header
              txoggleSidebar={toggleSidebar}
              title={props.title}
              menuButton={props.displayMenuButton}
              searchbar={header.searchbar}
              handleLogout={handleLogout}
              handleAccount={handleAccount}
              handleCourse={handleCourse}
              handleHome={handleHome}
              handleMyCourse={handleMyCourse}
              userData={userData}
              handleLogin={handleLogin}
              handleSignup={handleSignup}
              handleContactUs={handleContactUs}
              handleFaq={handleFaq}
              handleColleges={handleColleges}
              handlePlacement={handlePlacement}
              // handleAboutUs={handleAboutUs}
              handleAboutUs={handleAboutUs}
              setIssueModal={setIssueModal}
            />
          </Hidden>

          <Box
            flex={1}
            safeAreaBottom
            flexDirection={{
              base: "column",
              md: "row",
            }}
            _light={{
              borderTopColor: "coolGray.200",
            }}
            _dark={{
              // bg: "#17181a",
              borderTopColor: "coolGray.700",
            }}
          >
            {isSidebarVisible && displaySidebar && (
              <Hidden till="md">
                <Sidebar
                  navigation={navigation}
                  handleLogout={handleLogout}
                  pageName={pageName}
                  userData={userData}
                />
              </Hidden>
            )}

            <Hidden till="md">
              <ScrollView
                flex={1}
                contentContainerStyle={{
                  alignItems: "center",
                  flexGrow: 1,
                }}
              >
                <MainContent
                  {...props}
                  displayScreenTitle={displayScreenTitle}
                  showFooter={!hideFooter}
                  navigation={navigation}
                />
              </ScrollView>
            </Hidden>
            {inwdith < 500 ?
              <Hidden from="md">
                {showMobileHeader ? (
                  <Box
                    w="100%"
                    h="100%"
                    bg="#1D1E21"
                  >
                    <VStack
                      w="100%"
                      h="100%"
                      alignItems={"space-between"}
                    >
                      <HStack
                        alignItems={"center"}
                        justifyContent="space-between"
                        bg="#17181a"
                        px="12px"
                        py="12px"
                      >
                        <Image
                          loader={externaImageLoader}
                          alt={"image"}
                          width={"120px"}
                          height={"20px"}
                          layout={"fixed"}
                          src={'https://images.netskill.com/frontend/netskill/b2b/netskill_updated_logo.webp'}
                        />

                        <Pressable
                          onPress={() => toggleMobileHeader(false)}
                        >
                          <Text fontSize={"14px"} color={"white"}>Back</Text>
                        </Pressable>
                      </HStack>
                      <VStack
                        // h="50%"
                        px="18px"
                        pb="6px"
                        mb={"20%"}
                      >
                        {mobileHeaderList?.map((item, index) =>
                          (index == 1 || index == 5) &&
                            !userData ? null : index == 2 && userData ? null : (
                              <TouchableOpacity
                                onPress={() => {
                                  toggleMobileHeader(false);
                                  item.onPress();
                                }}
                                key={index}
                              >
                                <HStack
                                  justifyContent={"space-between"}
                                  alignItems="center"
                                  py="8px"
                                  my="4px"
                                >
                                  <Text
                                    fontFamily={"Nunito Sans"}
                                    fontSize="16px"
                                    color="#ffffff"
                                    fontWeight={400}
                                    textAlign={"center"}
                                  >
                                    {item.text}
                                  </Text>
                                  <Image
                                    loader={externaImageLoader}
                                    alt={"image"}
                                    width={"13px"}
                                    height={"20px"}
                                    layout={"fixed"}
                                    src={'https://images.netskill.com/frontend/Arrow.svg'}
                                  />
                                </HStack>
                              </TouchableOpacity>
                            )
                        )}
                      </VStack>
                      <HStack
                        position={"absolute"}
                        bottom={0}
                        // alignItems={"space-between"}
                        // justifyContent="space-between"
                        bg="#17181a"
                        px="18px"
                        // py="18px"
                        pb="15px"
                        textAlign={"center"}
                      >
                        <Button
                          ml={"15%"}
                          borderColor={"white"}
                          borderWidth="1"
                          h="40px"
                          variant="raised"
                          onPress={() =>
                            userData ? handleLogout() : handleLogin()
                          }
                          w="49%"
                          justifyContent="center"
                          py="4.5"
                          px="5"
                          fontSize="16px"
                          fontWeight="600"
                          mr="3"
                          _text={{
                            fontSize: "16px",
                            fontFamily: "Nunito Sans",
                            textAlign: "center",
                          }}
                          _hover={{
                            bg: "white",
                            _text: { color: "black" },
                          }}
                          textAlign="center"
                        >
                          {userData ? "Logout" : "Login"}
                        </Button>
                        <Button
                          ml={"10%"}
                          bg="#ff5e63"
                          h="40px"
                          w="49%"
                          onPress={handleContactUs}
                          variant="raised"
                          justifyContent="center"
                          py="3"
                          px="6"
                          _hover={{
                            bg: "error.500",
                          }}
                        >
                          <Text
                            fontFamily="Nunito Sans"
                            fontSize="16px"
                            fontWeight="600"
                            _dark={{
                              color: "coolGray.50",
                            }}
                            _light={{
                              color: "coolGray.800",
                            }}
                            textAlign="center"
                          >
                            {"Contact Us"}
                          </Text>
                        </Button>
                      </HStack>
                    </VStack>
                  </Box>
                ) : (
                  <MainContent
                    {...props}
                    displayScreenTitle={displayScreenTitle}
                    showFooter={!hideFooter}
                    navigation={navigation}
                  />
                )}
              </Hidden>
              : null}
          </Box>
        </KeyboardAwareScrollView>

      </Box>
    </>
  );
}
