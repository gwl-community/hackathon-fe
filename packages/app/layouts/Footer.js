import React from "react";
import { HStack, Hidden, Image, Text, VStack, Pressable } from "native-base";
import CLinkslug from "../config/CLinkslug";
import { Link } from 'solito/link'
import get from "lodash.get";
import { Dimensions, Linking } from "react-native";

export default function Footer({ navigation, ...props }) {
  const getBlogUrl = () => {
    if (typeof window !== "undefined") {
      const url = (typeof window).origin;
      if (url) {
        if (url.includes("dev")) {
          return "https://blog-dev.netskill.com";
        } else if (url.includes("staging")) {
          return "https://blog-staging.netskill.com/";
        } else {
          return "https://blog.netskill.com";
        }
      } else {
        return "https://blog.netskill.com";
      }
    } else {
      return "https://blog.netskill.com";
    }
  };
  const staticData = {
    footer: {
      id: 1,
      smallText: "© 2022 Aumniscient Education Private Limited",
      smallText1: "Privacy Policy",
      smallText2: "Terms of Use",
      logo: {
        data: {
          id: 59,
          attributes: {
            name: "logo.svg",
            alternativeText: "logo.svg",
            caption: "logo.svg",
            width: null,
            height: null,
            formats: null,
            hash: "logo_0991a32929",
            ext: ".svg",
            mime: "image/svg+xml",
            size: 16.1,
            url: "https://images.netskill.com/frontend/community.png",
            previewUrl: null,
            provider: null,
            provider_metadata: null,
            createdAt: "2022-04-10T00:55:54.208Z",
            updatedAt: "2022-04-10T00:55:54.208Z",
          },
        },
      },
      columns: [
        {
          id: 1,
          title: "Courses",
          links: [
            { id: 8, page: "courses", newTab: false, text: "All Courses" },
            {
              id: 9,
              page: "courses",
              slug: "fullstack-development-course",
              newTab: false,
              text: "Fullstack Development Course",
            },
            {
              id: 10,
              page: "courses",
              newTab: false,
              text: "iOS Development with Swift",
              slug: "ios-development-with-swift",
            },
            {
              id: 7,
              page: "courses",
              newTab: false,
              text: "Android Development with Kotlin",
              slug: "android-development-with-kotlin",
            },
            {
              id: 8,
              page: "courses",
              newTab: false,
              text: "Manual Testing Course",
              slug: "manual-testing-course",
            },
            {
              id: 8,
              page: "interview-prep",
              newTab: false,
              text: "Interview Prep Courses",
              slug: "",
            },

          ],
        },
        {
          id: 2,
          title: "Explore",
          links: [
            {
              id: 20,
              page: "placement-tests",
              newTab: false,
              text: "Placement Tests",
            },
            { id: 21, page: "contact-us", newTab: false, text: "Hire from Netskill" },
            // {
            //   id: 21,
            //   newTab: true,
            //   file: "https://docs.google.com/forms/d/e/1FAIpQLSfOKdtGLlQq82UW8z2lHZdPzBMCevL8_s9Es4faI3fOkCQvVg/viewform?edit_requested=true",
            //   text: "Hire from Netskill",
            // },
            {
              id: 22,
              page: "colleges",
              newTab: false,
              text: "Colleges & Orgs Platform",
            },
          ],
        },
        {
          id: 3,
          title: "Company",
          links: [
            { id: 15, newTab: false, page: "about-us", text: "About Us" },
            { id: 16, page: "contact-us", newTab: false, text: "Contact Us" },
            {
              id: 17,
              newTab: false,
              page: "faq",
              text: "FAQs",
            },
            {
              id: 18,
              newTab: true,
              text: "Blog",
              url: getBlogUrl(),
            },
          ],
        },
        {
          id: 4,
          title: "Contact",
          links: [
            {
              id: 15,
              newTab: true,
              file: "tel:8795468559",
              text: "+91 - 7353948100",
              image:
                "https://images.netskill.com/frontend/netskill/contact/phone.svg",
            },
            {
              id: 16,
              newTab: true,
              file: "mailto:hello@netskill.com",
              text: "hello@netskill.com",
              image:
                "https://images.netskill.com/frontend/netskill/contact/email.svg",
            },
            {
              id: 17,
              newTab: false,
              text: "3rd Floor, Goodworks Cowork, Akshay Tech Park, EPIP Zone, Whitefield, Bengaluru, Karnataka 560066, India",
              image:
                "https://images.netskill.com/frontend/netskill/contact/location2.svg",
            },
          ],
        },
      ],
    },
    columns: [
      {
        id: 1,
        title: "Courses",
        links: [
          { id: 8, page: "CourseScreen", newTab: false, text: "All Courses" },
          {
            id: 9,
            page: "CoursePreview",
            slug: "fullstack-development-course",
            newTab: false,
            text: "Fullstack Development Course",
          },
          {
            id: 10,
            page: "CoursePreview",
            newTab: false,
            text: "Backend Development Course",
            slug: "backend-development-course",
          },
          {
            id: 7,
            page: "CoursePreview",
            newTab: false,
            text: "Frontend Development Course",
            slug: "frontend-development-course",
          },
        ],
      },
      {
        id: 2,
        title: "Explore",
        links: [
          {
            id: 20,
            page: "PlacementScreen",
            newTab: false,
            text: "Placement Tests",
          },
          { id: 21, page: "ContactUs", newTab: false, text: "Hire from Netskill" },
          // {
          //   id: 21,
          //   newTab: true,
          //   file: ""
          //   // file: "https://docs.google.com/forms/d/e/1FAIpQLSfOKdtGLlQq82UW8z2lHZdPzBMCevL8_s9Es4faI3fOkCQvVg/viewform?edit_requested=true",
          //   text: "Hire from Netskill",
          // },
          {
            id: 22,
            page: "CollegeScreen",
            newTab: false,
            text: "Colleges & Orgs Platform",
          },
        ],
      },
      {
        id: 3,
        title: "Company",
        links: [
          { id: 15, newTab: false, page: "AboutUs", text: "About Us" },
          { id: 16, page: "ContactUs", newTab: false, text: "Contact Us" },
          {
            id: 17,
            newTab: false,
            page: "faq",
            text: "FAQs",
          },
          {
            id: 18,
            newTab: true,
            text: "Blog",
            url: "https://blogs-dev.netskill.com/",
          },
        ],
      },
      {
        id: 4,
        title: "Contact",
        links: [
          { id: 15, newTab: true, file: "tel:8795468559", text: "+91 - 7353948100", image: 'https://images.netskill.com/frontend/netskill/contact/phone.svg' },
          {
            id: 16,
            newTab: true,
            file: "mailto:hello@netskill.com",
            text: "hello@netskill.com",
            image: 'https://images.netskill.com/frontend/netskill/contact/email.svg'
          },
          { id: 17, newTab: false, text: "3rd Floor, Goodworks Cowork, Akshay Tech Park, EPIP Zone, Whitefield, Bengaluru, Karnataka 560066", image: "https://images.netskill.com/frontend/netskill/contact/location2.svg" },
        ]
      },
    ],
  };
  const footer = staticData?.footer;

  

  const {
    columns = [],
    smallText,
    smallText1,
    smallText2,
    bgColor = "#1d1e21",
    logo,
    description = "Netskill is an AI-driven platform aimed to provide high quality training and guaranteed placement opportunities to graduates and professionals.",
    socialMediaHeader = "Social Media",
    fb = {
      link: "",
      logoUrl: "",
    },
    linkedIn = {
      link: "",
      logoUrl: "",
    },
    twitter = {
      link: "",
      logoUrl: "",
    },
  } = footer;
  const logoUrl = get(
    logo,
    "data.attributes.url",
    "https://page-generator-images.netskill.com/logo_3x_50e668406a.webp"
  );
  let screenWidth = Dimensions.get("window").width;
  return (
    <VStack
      pt={16}
      pb={4}
      paddingX={{ md: 100, base: 10 }}
      width={{ md: "100%", base: "100%" }}
      overflow="visible"
      _light={{
        bg: bgColor,
        // color: "#fcfaf9",
      }}
      _dark={{
        bg: bgColor,
        // color: "#fcfaf9",
      }}
      alignItems={{ base: 'self-start', md: 'center' }}
      justifyContent={"center"}
    >
      <HStack
        // paddingLeft={{ base: 4, md: 0 }}
        flexDirection={{ md: 'row', base: 'column' }}
        w={"100%"}
        maxW={{ md: "1260px", lg: "1440px" }}
      >
        <VStack>
          <Image
            width={"165px"}
            height={"33px"}
            resizeMode="contain"
            alt="Netskill logo image"
            source={{ uri: logoUrl }}
          ></Image>
          <Text
            width={"284px"}
            fontSize={{ md: "xs" }}
            fontFamily="Nunito Sans"
            color={"#cccccc"}
            mt={"24px"}
          >
            {description}
          </Text>
          <HStack space={2} mt={"24px"}>
            <Link href="https://www.facebook.com/netskillacademy" isExternal>
              <Image
                width={"32px"}
                resizeMode="contain"
                alt="Netskill fb image"
                height={"32px"}
                source={{
                  uri: "https://images.netskill.com/frontend/netskill/footer/facebook.webp",
                }}
              ></Image>
            </Link>
            <Link href="https://www.instagram.com/netskillacademy/" isExternal>
              <Image
                width={"32px"}
                height={"32px"}
                resizeMode="contain"
                alt="Netskill instagram image"
                source={{
                  uri: "https://images.netskill.com/frontend/netskill/footer/instagram.webp",
                }}
              ></Image>
            </Link>
            <Link href="https://twitter.com/netskillacademy" isExternal>
              <Image
                width={"32px"}
                height={"32px"}
                resizeMode="contain"
                alt="Netskill course image"
                source={{
                  uri: "https://images.netskill.com/frontend/netskill/footer/twitter.webp",
                }}
              ></Image>
            </Link>
            <Link
              href="https://www.linkedin.com/school/netskillacademy/"
              isExternal
            >
              <Image
                width={"32px"}
                height={"32px"}
                resizeMode="contain"
                alt="Netskill course image"
                source={{
                  uri: "https://images.netskill.com/frontend/netskill/img/2.webp",
                }}
              ></Image>
            </Link>
          </HStack>
        </VStack>
        <HStack
          paddingBottom={{ base: 10, md: 0 }}
          flex={1}
          justifyContent={{md:'space-between', base:'flex-start'}}
        >
          <HStack
            w={"100%"}
            flexDirection="row"
            flexWrap={"wrap"}
            justifyContent={["flex-start", "space-around"]}
          >
            {columns.map((column) => {
              const { title, links = [] } = column;
              return (
                <VStack mr={["17px", 0]} py={["10px", 0]}>
                  <Text
                    style={{
                      fontWeight: "bold",
                    }}
                    // fontFamily={"NunitoSans"}
                    fontSize="14px"
                    fontFamily="Nunito Sans"
                    color={"#ffffff"}
                    pb={"6px"}
                  >
                    {title}
                  </Text>
                  {links.map((link, index) => {
                    const { text, newTab, page, file, url, image, slug } = link;
                    return (
                      <>
                        {slug ?
                          <CLinkslug
                            screenName={page}
                            slug={slug}
                            title={<Pressable
                              key={index}
                            >
                              <HStack>
                                {image ? (
                                  <Image mt={2} size={"14px"} source={image} alt="Netskill image" />
                                ) : null}
                                <Text
                                  maxW="216px"
                                  fontSize={{ md: "xs" }}
                                  color={"#cccccc"}
                                  py={"6px"}
                                  fontFamily="Nunito Sans"
                                  pl={image ? "2" : 0}
                                >
                                  {text}
                                </Text>
                              </HStack>
                            </Pressable>}
                          />
                          :
                          page ?
                            <Link
                              href={`/${page}`}>
                              <Pressable
                                key={index}
                              >
                                <HStack>
                                  {image ? (
                                    <Image mt={2} size={"14px"} source={image} alt="Netskill image" />
                                  ) : null}
                                  <Text
                                    maxW="216px"
                                    fontSize={{ md: "xs" }}
                                    color={"#cccccc"}
                                    py={"6px"}
                                    fontFamily="Nunito Sans"
                                    pl={image ? "2" : 0}
                                  >
                                    {text}
                                  </Text>
                                </HStack>
                              </Pressable>
                              {/* } */}
                            </Link>
                            :
                            <Pressable
                              key={index}
                              onPress={() => {
                                // if (page) {

                                // } else 
                                if (file && newTab) {
                                  // FileViewer.open(filePath);
                                  Linking.openURL(file, "_blank");
                                } else if (url && newTab) {
                                  Linking.openURL(url, "_blank");
                                }
                              }}
                            >
                              <HStack>
                                {image ? (
                                  <Image mt={2} size={"14px"} source={image} alt="Netskill image" />
                                ) : null}
                                <Text
                                  maxW="216px"
                                  fontSize={{ md: "xs" }}
                                  color={"#cccccc"}
                                  py={"6px"}
                                  fontFamily="Nunito Sans"
                                  pl={image ? "2" : 0}
                                >
                                  {text}
                                </Text>
                              </HStack>
                            </Pressable>

                        }
                      </>

                    );
                  })}
                </VStack>
              );
            })}
          </HStack>
        </HStack>
      </HStack>
      <HStack alignItems={'center'} mt={{md:5}} flexDirection={{ base: 'column', md: 'row' }}>
        <HStack>
          <Text>
            <Link href='/privacy'>
              <Text
                p={2}
                fontSize={{ md: "xs" }}
                fontFamily="Nunito Sans"
                color={"#b0b5c0"}
              >
                {" "}
                {smallText1}
              </Text>
            </Link>

            {"   |   "}
            <Link href='/terms'>
              <Text
                p={2}
                fontSize={{ md: "xs" }}
                fontFamily="Nunito Sans"
                color={"#b0b5c0"}
              >
                {smallText2}
              </Text>
            </Link>

            <Hidden till={"md"}>{"   |   "}</Hidden>
          </Text>
        </HStack>
        <Text
          textAlign={"center"}
          fontSize={{ md: "xs" }}
          fontFamily="Nunito Sans"
          color={"#b0b5c0"}
        >
          {smallText}
        </Text>

      </HStack>
    </VStack>
  );
}
