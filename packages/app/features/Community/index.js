import {
  Box,
  Button,
  HStack,
  Link,
  ScrollView,
  Text,
  VStack,
} from "native-base";
import React, { useEffect, useState } from "react";
import {
  SectionSpacingX,
  SectionSpacingY,
} from "../../lib/constants";

import Image from 'next/image';
import dynamic from 'next/dynamic'
import { getToken } from "../../lib/utils";
import { useRouter } from 'solito/router'

const DashboardLayout = dynamic(() => import('../../layouts/DashboardLayout'));
const Topjobs = dynamic(() => import('../../components/section/Topjobs'));
const Banner = dynamic(() => import('../../components/banner/Banner'))
const Footer = dynamic(() => import('../../layouts/Footer'));
const DreamJob = dynamic(() => import('../../components/section/DreamJob'))
const ToptestHire = dynamic(() => import('../../components/section/Top-test-Hire'));
const HiringPartner = dynamic(() => import('../../components/section/HiringPartner'));
const PlacementsPartners = dynamic(() => import('../../components/section/placements-partnership'))
const ExploreCourses = dynamic(() => import('../../components/section/exploreCourses'));
const YemployeenetskillCertifiction = dynamic(() => import('../../components/section/Emp-y-netskill-dev'));
const RelatedPost = dynamic(() => import('../../components/section/RelatedPost'))

export default function Community({ props, pageGeneratorBaseUrl }) {
  const url = "https://community-api.netskill.com/"
  const [inwdith, setInwidth] = useState(0);
  useEffect(() => {
    const newWidth = window?.innerWidth;
    setInwidth(newWidth)
  }, []);
  const { push } = useRouter()
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [load, setLoad] = useState(false);
  const [banner, setBanner] = useState();
  const [title, setTitle] = useState()
  const [partner, setPartner] = useState();
  const [collegeStepFeature, setCollegeStep] = useState();
  const [collegeStepHeader, setCollegeStepHeader] = useState();
  const [topTestContent, setTopTestContent] = useState();
  const [topTestHeader, setTopTestHeader] = useState();
  const [topJobsTitle, setTopJobsTitle] = useState();
  const [topJobsMentors, setTopJobsMentors] = useState();
  const [bottomActions, setBottomAction] = useState();
  const [WhyHire, setWhyHire] = useState();
  const [courseData, setCourseData] = useState();
  const [courseConent, setSourceContent] = useState(null);
  const externaImageLoader = ({ src }: { src: string }) =>
    `${src}`;
  const checkIsLoggedIn = async () => {
    const token = await getToken();
    if (token) {
      setIsLoggedIn(true);
    }
  };
  const fetchData = async () => {
    try {
      var requestOptions = {
        method: 'GET',
        redirect: 'follow'
      };
      setLoad(true);
      fetch(`${url}search/pageConfig`, requestOptions)
        .then(response => response.json())
        .then(data => {
          setLoad(false);
          setBanner(data?.banner);
          setTitle(data?.banner?.title);
          setPartner(data?.partners);
          setCollegeStep(data?.dreamJobSteps?.[0]?.features);
          setCollegeStepHeader(data?.dreamJobSteps?.[0]?.header);
          setTopTestContent(data?.topTest?.[0]?.course_content);
          setTopTestHeader(data?.topTest?.[0]?.header);
          setTopJobsTitle(data?.topJobs?.[0]?.header?.title);
          setTopJobsMentors(data?.topJobs?.[0]?.mentors)
          setBottomAction(data?.bottomActions)
          setWhyHire(data?.WhyHire)
          setBottomBanner(data?.bottomActions)
          initiateData(data?.data?.attributes?.questions);
          
        })
        .catch(error => console.log('error', error));
      fetch(`${url}search/course`, requestOptions)
        .then(response => response.json())
        .then(courseData => {
          setCourseData(courseData);
          setSourceContent(courseData?.banner?.course_content)
        })
        .catch(error => console.log('error', error));

    } catch (error) {
      console.log("error", error);
    }
  };

  useEffect(() => {
    fetchData();
    checkIsLoggedIn();
  }, []);
  const handleViewAll = () => {
    props?.navigation.navigate("CourseScreen");
  };
  const HandleClick = () => {
    push('/contact-us')
    // window.open(
    //   "https://docs.google.com/forms/d/14qfpHcIoL7z1mcyYM7ejGrVMgS2VtLXLpFQKeKpc64E/edit"
    // );
  };

  const handleRequestAccess = () => {
    push(`/test/fullstack-development-test`)
  };
  const requestBtn = (x) => {
    return (
      <Button
        minW="201"
        py={'3'}
        px='0px'
        variant="raised"
        width={{ base: "18%", md: "15%" }}
        height={{ base: '', md: '48px' }}
        onPress={handleRequestAccess}
        size="md"
        bg="#ff5e63"
        _text={{
          color: "white",
          fontSize: 15,
        }}
        _hover={{
          bg: "error.500",
        }}
      >
        {x || banner?.buttons?.[0]?.text}
      </Button>
    );
  };
if(load)
return (
  <VStack w={"100%"} h={"100%"} justifyContent={"center"} alignItems={"center"} bg={"#17181a"}>
  <Image
        loader={externaImageLoader}
        alt={"image"}
        width={"130px"}
        height={"40px"}
        layout={"fixed"}
        src={"https://images.netskill.com/frontend/community.png"}
      />
</VStack>
)
else
  return (
    <Box w={'100%'}>
      <DashboardLayout
        title="Colleges & Organisations"
        displayMenuButton
        displayScreenTitle={false}
        displayAlternateMobileHeader
        displaySidebar={false}
        navigation={props?.navigation}
        showFooter={false}
      >
        
          <ScrollView showsVerticalScrollIndicator={false}>
            {banner?.media[0]?.url &&
              <VStack h={{ base: "600px", md: "430px" }} zIndex={2}>
                {inwdith > 700 ?
                  <Box style={{
                    flex: 1,
                    height: "100%",
                    // width: "50%",
                    position: "absolute",
                    right: 0,
                    justifyContent: "center",
                    resizeMode: "cover",
                  }}>
                    <Image
                      loader={externaImageLoader}
                      alt={"image"}
                      width={inwdith / 2.3}
                      height={"430px"}
                      layout={"fixed"}
                      src={banner?.media[0]?.url}
                    />
                  </Box>
                  :
                  <Box style={{
                    flex: 1,
                    height: "50%",
                    width: "100%",
                    position: "absolute",
                    right: 0,
                    top: "51%",
                    justifyContent: "center",
                    resizeMode: "cover",
                  }}>
                    <Image
                      loader={externaImageLoader}
                      alt={"image"}
                      width={inwdith}
                      height={"600px"}
                      layout={"fixed"}
                      src={banner?.media[0]?.url}
                    />
                  </Box>
                }
                <VStack
                  h={"90%"}
                  pl={{
                    lg: "120px",
                    md: "40px",
                    base: 4,
                  }}
                  justifyContent={{ base: "flex-start", md: "center" }}
                >
                  <VStack
                    // flex={{ base: 0.8, md: 0.5}}
                    justifyContent={{ md: "space-around" }}
                  // bg={"amber.100"}
                  >
                    <VStack>
                      <Text
                        fontSize={{
                          base: "24px",
                          md: "36px",
                        }}
                        color="#ffffff"
                        fontWeight={{ base: 800, md: 700 }}
                        fontFamily="Nunito Sans"
                        style={{
                          fontFamily: "Nunito Sans",
                        }}
                        lineHeight={"40.56px"}
                        letterSpacing="1.5"
                        maxW={{ md: "492px", base: "100%" }}
                        // mt="-10px"
                        pr="12px"
                      // pt={{ base: "18px" }}
                      >
                        {/* {"Join Netskill Community to get started with your dream career"} */}
                        {banner?.header?.title}
                      </Text>

                    </VStack>

                    <Text
                      minW={"220"}
                      width={{
                        base: "100vw",
                        lg: "40%",
                        md: "35%",
                      }}
                      maxW={{ base: "100%", md: "80%" }}
                      fontSize={{
                        base: "16px",
                        md: "16px",
                      }}
                      color="#B0B5C0"
                      fontFamily="Nunito Sans"
                      fontWeight="normal"
                      pr="12px"
                      letterSpacing="0.2"
                    // mb={{ base: 0, md: "42px" }}
                    >
                      {/* {"A place where you can assess yourself, take courses or mentorship, find work opportunities, and meet like minded people to grow with.."} */}
                      {banner?.header?.description ??
                        "World’s top companies hire professionals on Netskill. Select a job role, crack the test, get hired."
                      }
                    </Text>
                    {/* <Box flex={{ md: 0.2 }} /> */}
                    <Box
                      flex={0.3}
                      justifyContent={"flex-end"}
                      pt={{ base: "10px", md: "56px" }}

                    >
                      {requestBtn(null)}
                    </Box>

                    {/* <HStack
                    pt={{ base: 5, md: 10 }}
                    alignItems={"center"}
                  >
                    <Text
                      lineHeight={1.7}
                      fontSize={{
                        base: "12px",
                        md: "14px",
                      }}
                      color="#ffffff"
                      fontWeight={600}
                      fontFamily="Nunito Sans"
                      letterSpacing="0.2"
                    >
                      {banner?.header?.subTitle}{' '}
                    </Text>
  
                    <Text
                      style={{ textDecorationLine: 'underline' }}
                      lineHeight={1.57}
                      fontSize={{
                        base: "12px",
                        md: "14px",
                      }}
                      color="#ffffff"
                      fontWeight={600}
                      fontFamily="Nunito Sans"
                      letterSpacing='normal'
                    >
                      <Link href="https://docs.google.com/forms/d/14qfpHcIoL7z1mcyYM7ejGrVMgS2VtLXLpFQKeKpc64E/edit" isExternal>
                        {banner?.linkText}
                      </Link>
  
                    </Text>
                  </HStack> */}
                  </VStack>
                </VStack>
              </VStack>}

            {/* <VStack
              bg="#17181a"
              zIndex={2}
              w="100%"
              py={SectionSpacingY}
              px={SectionSpacingX}
            >
              <HiringPartner
                isHiringPartner={true}
                data={partner?.media}
                title={partner?.header?.title}
                subtitle=
                {partner?.header?.description ??
                  "100+ Top Companies hire ‘job ready’ engineers with cutting-edge skills from our courses"
                }
              />
            </VStack> */}
            <VStack
              bg="#ffffff"
              zIndex={2}
              w="100%"
              py={SectionSpacingY}
              px={SectionSpacingX}
            >
              <DreamJob
                data={collegeStepFeature}
                title={collegeStepHeader?.title}
                subtitle={collegeStepHeader?.description}
              />
              <Box w={"100%"} paddingTop={"20px"} alignItems={"center"}>
                {requestBtn("Take test to get hired")}
              </Box>
            </VStack>
            <VStack
              bg="#ffffff"
              w="100%"
            >
              <ToptestHire data={topTestContent} title={topTestHeader?.title} nav={props?.navigation} props={props} />
            </VStack>
            <VStack
              bg="#ffffff"
              w="100%"
            >
              <Topjobs data={topJobsMentors} isPlacement={true} title={topJobsTitle} props={props} bottomClick={handleRequestAccess} />
            </VStack>
            <VStack
              bg="#17181a"
              zIndex={2}
              w="100%"
              py={SectionSpacingY}
              px={SectionSpacingX}
            >
              <ExploreCourses
                nav={props?.navigation}
                data={courseData?.courses?.[0]?.course}
                courseConent={courseConent}
                isHomeScreen={true}
                title={"Mastery Courses with Placement"}
                subtitle={"In-depth courses designed to get you job ready and hired by top companies."
                }
                handleViewAll={'/courses'}
              />
            </VStack>
            <VStack
              bg="#ffffff"
              w="100%"
            >
              <YemployeenetskillCertifiction
                HandleClick={HandleClick}
                data={WhyHire}
                shapeImage={
                  "https://images.netskill.com/frontend/netskill/courseDetail/oval.svg"
                }
                mainImage={
                  "https://images.netskill.com/frontend/netskill/courseDetail/laptop.svg"
                }
                title="Our Program Structure"
                showExploreButton={true} />
            </VStack>
            <VStack
              bg="#ffffff"
              w="100%"
              pb={SectionSpacingY}
            >

              <PlacementsPartners data={bottomActions?.header} inwdith={inwdith} />
            </VStack>
            <RelatedPost />
            <Banner
              title="Get ready to land your dream job"
              height={"300px"}
              buttonWidth="205"
              buttonText="Take Test To Get Hired"
              onPressed={handleRequestAccess}
            />
            {/* <Footer /> */}
          </ScrollView>
      </DashboardLayout>
    </Box>
  );
}
