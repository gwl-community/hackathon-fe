import {
    Box,
    HStack,
    Image,
    Text,
    VStack,
    Link,
    Divider,
    Pressable,
    Button,
    Flex,
    Hidden,
} from "native-base";
import React, { useRef, useEffect, useState } from "react";
import { SectionSpacingX, SectionSpacingY, } from "../../lib/constants";
import { NavigationContext } from "@react-navigation/native";
import { useRouter } from 'solito/router'
import Hiring from "../CoursePreview/components/Hiring";
import parse from 'html-react-parser';
import {
    TouchableOpacity,
    Animated,
    ScrollView,
} from "react-native";
import TabView from "../../components/section/StickyTabComponent/TabView";
import TabListHeader from "../../components/section/StickyTabComponent/TabListHeader";
import get from "lodash.get";
export default function TestContent(
    { props, tabList, questions, testStructre, selectedQuestion, selectedQuestionOne, handleToggleQuestion, handleToggleQuestionOne, courseImg, selectedTest, showTestdata, setShowTestdata, setShowBookSlot, content, slugName, showToast }) {
    const navigation = React.useContext(NavigationContext);
    const [height1, setHeight1] = useState(0);
    const [inwdith, setInwidth] = useState(0);
    const [window_, setWindow] = useState();

    useEffect(() => {
        const newHeight = window?.innerHeight;
        const newWidth = window?.innerWidth;
        setHeight1(newHeight);
        setInwidth(newWidth)
        setWindow(window_);
    }, []);
    const { push } = useRouter()
    const [selectedTab, setSelectedTab] = React.useState(0);
    const [tab1Coord, setTab1Coord] = useState(0);
    const [tab2Coord, setTab2Coord] = useState(0);
    const [tab3Coord, setTab3Coord] = useState(0);
    const [tab4Coord, setTab4Coord] = useState(0);
    const [tab5Coord, setTab5Coord] = useState(0);
    const [manuallyClicked, setManuallyClicked] = useState(false);
    const [showStickyHeader, setShowStickyHeader] = useState(false);
    const [changeHeader, setChangeHeader] = useState(false);
    const [scrollToEnd, setScrollToEnd] = useState(false);
    const [selected, setSelected] = React.useState(0);

    const scrollViewRef = useRef();
    const scrollRef = useRef();
    const value = new Animated.Value(0);
    const scrollMargin = inwdith < 700 ? 350 : 250;

    useEffect(() => {
        if (changeHeader !== showStickyHeader) {
            setShowStickyHeader(changeHeader);
            if (!changeHeader) {
                setSelectedTab(0);
            }
        }
    }, [changeHeader]);

    const openModal = () => {
        window.open("https://netskill.com/signin")
        // window.open("http://localhost:3001/signin")
        // window.open("https://developers.goodworksalpha.com/test/java-test")
        // setShowBookSlot(true)
    }
    useEffect(() => {
        window.addEventListener('scroll', handleScroll)
        return () => window.removeEventListener('scroll', handleScroll)
    })
    const handleScroll = (e) => {
        if (e?.nativeEvent?.contentOffset?.y > 350) {
            const coords = e?.nativeEvent?.contentOffset?.y;
            setChangeHeader(true);
            if (showStickyHeader && !manuallyClicked) {
                if (coords > tab1Coord && coords < tab2Coord
                ) {
                    setSelectedTab(0);
                }
                if (coords > tab2Coord && coords < tab3Coord) {
                    setSelectedTab(1);
                }
                if (coords > tab3Coord && coords < tab4Coord) {
                    setSelectedTab(2);
                }
                if (coords > tab4Coord && coords < tab5Coord) {
                    setSelectedTab(3);
                }
                if (coords > tab5Coord) {
                    setSelectedTab(4);
                }
            }
        }
        else if (e?.nativeEvent?.contentOffset?.y < 350)
            setChangeHeader(false);

    };
    const handleChange = (newValue) => {
        setSelectedTab(newValue);
        setManuallyClicked(true);
        try {
            if (newValue == 0) {
                scrollViewRef.current.scrollTo({
                    x: 0,
                    y: 100,
                    animated: true
                });
            }
            if (newValue == 1) {
                scrollViewRef.current.scrollTo({
                    x: 0,
                    y: tab2Coord + scrollMargin,
                    animated: true
                });
            }
            if (newValue == 2) {
                scrollViewRef.current.scrollTo({
                    x: 0,
                    y: tab3Coord + scrollMargin,
                    animated: true
                });
            }
            if (newValue == 3) {
                scrollViewRef.current.scrollTo({
                    x: 0,
                    y: tab4Coord + scrollMargin,
                    animated: true
                });
            }
            if (newValue == 4) {
                scrollViewRef.current.scrollTo({
                    x: 0,
                    y: tab5Coord + scrollMargin,
                    animated: true,
                });
            }
        } catch (err) {
            console.log(err);
        }
        setTimeout(() => {
            setManuallyClicked(false);
        }, 1000);
    };

    const FirstRoute = () => {
        <>
            <VStack pt={"64px"} width={"100%"} pl={["14px", '39px']}>
                <Text pb={"24px"} fontSize={["20px", '24px']} style={{ fontWeight: "bold", color: "#17181a" }} color="#747476" fontFamily={"Nunito Sans"}>
                    Test Structure</Text>
            </VStack>
            <HStack pl={["10px", "39px"]}>
                <VStack width={"100%"} pr={SectionSpacingX}>
                    {content?.test_structure?.[0]?.header?.map((item, index) => {
                        return (
                            <Box
                                borderRadius={"4px"}
                                fontFamily={"Nunito Sans"}
                                mb="16px"
                            >

                                <VStack>
                                    <TouchableOpacity
                                        onPress={
                                            () => handleToggleQuestionOne(index)
                                        }
                                    >
                                        <HStack px="20px" borderWidth={1} borderRadius={"4px"}
                                            borderColor="coolGray.200" py="20px"
                                            justifyContent={"space-between"}
                                            alignItems="center"
                                            mb={selectedQuestion == index ? "18px" : 0}
                                            space={2}
                                        >
                                            <HStack w={["50%", "65%"]}>
                                                <Text w={["10%", "5%"]} fontSize="16px" color={'#747476'} fontFamily={"Nunito Sans"}>{index + 1}</Text>
                                                <Text w={["90%", "95%"]}
                                                    fontSize="16px"
                                                    color={"#17181a"}
                                                    fontFamily={"Nunito Sans"}
                                                    fontWeight="bold"
                                                    textAlign={"left"}
                                                >
                                                    {item?.title}
                                                </Text>
                                            </HStack>
                                            <Text
                                                fontSize="16px"
                                                color={"#17181a"}
                                                fontFamily={"Nunito Sans"}
                                                fontWeight="600"
                                                textAlign={"left"}

                                            >
                                                <HStack
                                                    flexDirection={["column", "row"]}
                                                    justifyContent={"flex-start"} alignItems={["flex-start", "center"]}
                                                >
                                                    <HStack bgalignItems={"center"}>
                                                        <Image
                                                            source={"https://images.netskill.com/frontend/netskill/courses/board.webp"}
                                                            size={["12px", "16px"]}
                                                            alt="BoardImage" />

                                                        <Text color={'#595fff'} fontSize={["12px", "14px"]} semibold pr={[2, 0]} pl={1} fontFamily={"Nunito Sans"}>
                                                            {item?.project}
                                                        </Text>
                                                    </HStack>
                                                    <HStack alignItems={"center"}>
                                                        <Image source={"https://images.netskill.com/frontend/netskill/home/timeIcon.webp"} size={["12px", "16px"]} alt="TimeiconImage" />
                                                        <Text color={'#595fff'} fontSize={["12px", "14px"]} semibold pr={[2, 4]} pl={1} fontFamily={"Nunito Sans"}>
                                                            {item?.duration}
                                                        </Text>
                                                    </HStack>

                                                </HStack>
                                            </Text>
                                        </HStack>
                                    </TouchableOpacity>

                                    {selectedQuestionOne == index ? (
                                        <Box borderWidth={1}
                                            borderColor="coolGray.200" py="20px">
                                            <Text pl={'5%'}
                                                fontSize="14px"
                                                color="#17181a"
                                                textAlign={"left"}
                                                pb="12px"
                                                fontFamily={"Nunito Sans"}
                                                lineHeight={"28px"}
                                            >
                                                {item?.description}
                                            </Text>
                                        </Box>
                                    ) : null}
                                </VStack>
                            </Box>

                        )
                    })}
                </VStack>
            </HStack>
            <Button mt={"40px"}
                variant={"raised"}
                mb={"80px"}
                w={"217px"}
                ml={["5px", "39px"]}
                bg="#ff5e63"
                _hover={{
                    bg: "error.500",
                }}
                onPress={openModal}
            >
                {/* <Text color={"#fff"} fontSize="15px">Take test to get hired</Text> */}
            </Button>
        </>
    };

    const ThirdRoute = () => {
        return (
            <VStack bg={"white"} pt={"80px"} pb={["45px", "93px"]} pl={["12px", '39px']}>
                <VStack pb={"22px"} width={"100%"}>
                    <Text color={'#262627'} fontSize="24px" fontWeight={"700"} fontFamily={"Nunito Sans"}>
                        {content?.react_jobs?.[0]?.title}
                    </Text>
                </VStack>
                <ScrollView
                    ref={scrollRef}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    pagingEnabled={true}
                >
                    {/* <HStack> */}
                    {content?.react_jobs?.[0]?.header?.map((item, i) => {
                        return (
                            <Box
                                mr={"24px"}
                                ml={'0.5'}
                                bg={"#fff"}
                                borderRadius="4px" my={"2px"}
                                style={{ boxShadow: "0 0 6px 0 rgba(0, 0, 0, 0.1)" }}
                                shadowColor='#171717'
                                shadowOpacity="0.4"
                                shadowRadius="4px"
                                maxW="384px"
                                w="384px"
                                h={"260px"}
                                pt={"24px"}
                                pb={'31px'}
                                px={'24px'}
                            >
                                <VStack>
                                    <HStack pb="24px">
                                        <Image size={"60px"} source={item?.subTitleMedia?.data?.attributes?.url} alt="Image" />
                                        <Flex pl={"24px"}>
                                            <Text
                                                fontSize="16px"
                                                fontWeight="bold"
                                                color="#17181a"
                                                fontFamily={"Nunito Sans"}
                                            >
                                                {item?.title}
                                            </Text>
                                            <Text
                                                fontSize="14px"
                                                pt="4px"
                                                color="#17181a"
                                                opacity="0.5"
                                                fontFamily={"Nunito Sans"}
                                            >
                                                {item?.description}
                                            </Text>
                                        </Flex>
                                    </HStack>
                                    <Text color={"#17181a"} textAlign={"left"} fontSize="14px" fontWeight="normal" fontFamily={"Nunito Sans"}>
                                        {item?.subTitle}
                                    </Text>
                                </VStack>
                                <HStack pt={"4"} space={6}>
                                    <HStack pt={"1.5"} alignItems={"center"}>
                                        <HStack alignItems={"center"}>
                                            <Image source={"https://images.netskill.com/frontend/netskill/courses/wallet1.webp"} size={"16px"} alt="WalletImage" />
                                        </HStack>
                                        <Text opacity={'0.8'} fontSize={'12px'} color="#17181a" pl={1} fontFamily={"Nunito Sans"}>{item?.study}</Text>
                                    </HStack>
                                    <HStack pt={"1.5"} alignItems={"center"}>
                                        <HStack alignItems={"center"}>
                                            <Image source={"https://images.netskill.com/frontend/netskill/courses/job1.webp"} size={"16px"} alt="CourseImage" />
                                        </HStack>
                                        <Text opacity={'0.8'} fontSize={'12px'} color="#17181a" pl={1} fontFamily={"Nunito Sans"}>{item?.project}</Text>
                                    </HStack>
                                </HStack>
                            </Box>
                        )
                    })}
                    {/* </HStack> */}
                </ScrollView>
                <Hidden till="md">
                    <Pressable
                        onPress={() => {
                            scrollRef.current.scrollTo({
                                x: !scrollToEnd ? 500 : 0,
                                // x: !scrollToEnd ? selectedTest?.jobList?.length * 100 : 0,
                                y: 0,
                                animated: false,
                            })
                            setScrollToEnd(!scrollToEnd);
                        }
                        }
                         position={"absolute"}
                         left={scrollToEnd ? 20 : undefined}
                         right={!scrollToEnd ? 20 : undefined}
                         top={"250px"}
                         >
                        <VStack justifyContent={"center"} style={{ boxShadow: '0 0 12px 0 rgba(0, 0, 0, 0.1)', transform: !scrollToEnd ? [{ rotateY: "0deg" }] :[{ rotateY: "180deg" }], }}
                            shadowRadius="30px" alignItems={"center"} w={"48px"} h={"48px"} borderRadius={"30px"} bg={"white"}>
                                <Image  source= {{uri: 'https://images.netskill.com/Group+637%403x.webp'}} size={5}  alt={"image"}/>
                        </VStack>
                    </Pressable>
                </Hidden>
                <VStack pl={["5px", "1px"]} pt={6}>
                    <Button w={'300px'}
                        variant={"raised"}
                        _text={{
                            color: "#ffffff",
                            fontSize: "15px",
                            // fontWeight: "600",
                            // fontFamily:"Nunito Sans"
                        }}
                        bg="#ff5e63"
                        _hover={{
                            bg: "error.500",
                        }}
                        onPress={openModal}

                    >
                        Take test to qualify for these jobs
                    </Button>
                </VStack>
            </VStack>
        );
    }
    const getLink = () => {
        return 'https://www.goodworksalpha.com/'
    }
    const OurTeamTemplate = ({ data, theme }) => {

        return (
            <HStack bg={theme === "light" ? "white" : "#17181a"} w={"100%"}
                justifyContent={"space-between"}
                flexDirection={{ base: "column", md: "row" }} py={["40px", undefined]}
            >
                {data?.header.description ?
                    <VStack pl={["12px", "44px"]} pr={["12px", undefined]} py={["10px", "80px"]} w={["100%", "60%"]}>
                        <Text fontWeight={"bold"} fontSize={"24px"} textAlign={"flex-start"} color={theme === "light" ? "#17181a" : "white"}>{data?.header.title}</Text>
                        <Text w={["100%", "97%"]} pt={["12px", "23px"]} textAlign={"flex-start"} fontSize={"16px"} color={theme === "light" ? "#17181a" : "white"}>
                            {<Link href={getLink()} _text={{
                                textDecoration: "underline",
                                fontWeight: "600", fontSize: "16px", color: theme === "light" ? "#17181a" : "white"
                            }}
                            >{"GoodworksAlpha"} </Link>}{data?.header?.description}
                        </Text>
                    </VStack> : null}
                <Hidden till="md">
                    {data?.media ? <VStack h={"100%"} w={["100%", "35%"]} pt={["15px", "0px"]}>
                        <Image w={"100%"} h={'100%'} source={data?.media} alt="CourseImage" />
                    </VStack> : null}
                </Hidden>
                <Link href={getLink()} pr={[0, "33px"]} w={["100%", "45%"]} pb={["20px", "0px"]} justifyContent={"center"}>
                    <HStack alignItems={"center"} flexDirection={["column", "row"]}>
                        <Text ml={"-10"} color={"white"} fontWeight={"300"} fontSize={"40px"}>+</Text>
                        <Image right={0} ml={[0, "25px"]} w={"250px"} h={"70px"} source={{ uri: 'https://images.netskill.com/frontend/netskill/home/ga.webp' }} borderRadius={'4px'} alt="CourseImage" />
                    </HStack>
                </Link>
            </HStack>
        );
    }
    const FourthRoute = () => {
        return (
            <VStack bg={"#f4f4f4"} mt={5} pt={5}  >
                <VStack pl={["13px", '39px']}>
                    <Hiring
                        companyImgSize={"160"}
                        title={content?.partners?.header?.title}
                        subHeading={'From Fortune 500 to startups, everyone trusts GoodWorks developers'}
                        partners={content?.partners?.media.map((item, i) => {
                            return (
                                item?.media?.data?.attributes?.url
                            )
                        })}
                    />
                </VStack>
                <OurTeamTemplate data={get(content, 'bottomActions')} theme={"dark"} />
            </VStack>
        )
    };

    const FifthRoute = () => {
        return (
            <VStack bg={"white"} pt={'80px'} pb={["180px", '80px']} pr={{ base: "14px", md: "160px" }} pl={["14px", '39px']}>
                <Text
                    fontSize="24px"
                    bold
                    pb={"30px"}
                    color="#1f2937"
                >
                    Frequently Asked Questions
                </Text>
                {content?.faq?.map((item, index) => (
                    <Box
                        bg="#f4f4f4"
                        py="16px"
                        px="20px"
                        borderRadius={"4px"}
                        fontFamily={"Nunito Sans"}
                        mb="16px"
                        borderWidth="0.2"
                        borderColor={"gray.200"}
                    >
                        <VStack>
                            <TouchableOpacity onPress={() => handleToggleQuestion(item.id)}>
                                <HStack
                                    justifyContent={"space-between"}
                                    alignItems="center"
                                    pb={selectedQuestion == item.id ? "24px" : 0}
                                    space={2}
                                >
                                    <Text
                                        style={
                                            selectedQuestion == item.id
                                                ? { fontWeight: "bold" }
                                                : { fontWeight: "normal" }
                                        }
                                        fontSize="14px"
                                        color="#17181A"
                                        fontFamily={"Nunito Sans"}
                                        textAlign={"left"}
                                    >
                                        {item.question}
                                    </Text>
                                    {/* <MaterialIcons
                                        name={
                                            selectedQuestion === item.id
                                                ? "keyboard-arrow-up"
                                                : "keyboard-arrow-down"
                                        }
                                        size={21}
                                        color="#17181A"
                                    /> */}
                                    {selectedQuestion === item.id ?
                                        <Image  alt={"image"}
                                            width={{ md: 6, base: 6 }}
                                            height={{ md: 6, base: 6 }}
                                            resizeMode="contain"
                                            source={{ uri: 'https://images.netskill.com/frontend/upArrow.webp' }} /> :
                                        <Image alt={"image"}
                                            width={{ md: 6, base: 6 }}
                                            height={{ md: 6, base: 6 }}
                                            resizeMode="contain"
                                            source={{ uri: 'https://images.netskill.com/frontend/downArrow.webp' }} />
                                    }
                                </HStack>
                            </TouchableOpacity>

                            {selectedQuestion == item.id ? (
                                <>
                                    <Text
                                        pb={"15px"}
                                        fontSize="14px"
                                        color={"#17181a"}
                                        textAlign={"left"}
                                        fontFamily={"Nunito Sans"}
                                        lineHeight={"28px"}
                                    >
                                        {parse(item.answer)}
                                    </Text>
                                    {item?.bullets?.map((item) => {
                                        return (
                                            <HStack alignContent={"center"}>
                                                {/* <Entypo mt={10} mr={"10px"} name="dot-single" size={"24px"} color="#17181a" /> */}
                                                <Text
                                                    fontSize="14px"
                                                    color={"#17181a"}
                                                    textAlign={"left"}
                                                    fontFamily={"Nunito Sans"}
                                                    lineHeight={"28px"}>{item}</Text>
                                            </HStack>
                                        )
                                    }
                                    )}
                                </>
                            ) : null}
                        </VStack>
                    </Box>
                ))}
                <HStack>
                    <Button mt={"44px"}
                        variant={"raised"}
                        px={2}
                        w={"170px"}
                        bg="#ff5e63"
                        _hover={{
                            bg: "error.500",
                        }}
                        onPress={() => {
                            window.open("https://www.netskill.com/faq")
                        }}
                    >
                        <Text color={"#fff"} fontSize="15px">
                            View more FAQs
                        </Text>
                    </Button>
                </HStack>
            </VStack>
        );
    }
    const renderContentSkill = (item) => {
        return (
            <Box borderRadius={'4px'} h={"107px"} mr={"30px"} py={"24px"} w={"290px"} bg={"white"} mb={[3, 0]}>
                <HStack borderRadius={'4px'}>
                    <Box alignItems={"center"} flex={0.2} >
                        <Image   source={{uri:item?.media?.data[0]?.attributes?.url}} width={'30px'} height={'32px'} alt="CourseImage" />
                    </Box>
                    <VStack flex={0.8} flexWrap={"wrap"}>
                        <Text fontWeight={'600'} color={'#17181a'} fontSize={"20px"}>{item?.title}</Text>
                        <Text pt={"10px"} fontWeight={'normal'} color={'#17181a'} fontSize={"14px"}>{item?.description}</Text>
                    </VStack>
                </HStack>
            </Box>
        )
    }

    const SecondRoute = () => {
        return (
            <VStack bg={"#f4f4f4"} pt={"80px"} pb={"80px"} px={["10px", "39px"]} w={"100%"}>
                <Text
                    fontSize="24px"
                    fontWeight={"600"}
                    pb={"28px"}
                    color="#1f2937"
                >
                    Skills Required
                </Text>
                <Hidden till="md">
                    <ScrollView
                        ref={scrollRef}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        pagingEnabled={true}
                    >
                        {content?.skills_required?.[0]?.course_content?.map((item) => {
                            return (
                                renderContentSkill(item)
                            )
                        })}
                    </ScrollView>
                </Hidden>
                <Hidden from="md">
                    <Box w={"100%"}>
                        {content?.skills_required?.[0]?.course_content?.map((item) => {
                            return (renderContentSkill(item)
                            )
                        })}
                    </Box>
                </Hidden>
            </VStack>
        );
    }
    const callCourse = (data) => {
        let newSlug = "";
        if(data?.slug === 'frontend-development-test') newSlug = "frontend-development-course"
        if(data?.slug === 'fullstack-development-test') newSlug = "fullstack-development-course"
        if(data?.slug === 'backend-development-test') newSlug = "backend-development-course"
        let data_ = data?.banner?.linkUrl + "courses/" + newSlug;
        // let data_ =  "/courses/" + newSlug;
        return data_;
        // push(data_)
    }
    const stickyHeaderView = (data) => {
        return (
            <VStack bg={"#1D1E21"} w={'100%'}>
                <HStack
                    alignItems={["flex-start", "center"]}
                    direction={{ base: "column", md: "row" }}
                    // h={"115px"}
                    color={"black"}
                    pl={["5px", "39px"]}
                >
                    <Text
                        textTransform={"capitalize"}
                        w={{ base: "100%", md: "69%" }}
                        lineHeight={{ base: "30", md: "39" }}
                        fontWeight={"bold"}
                        color="#fff"
                        textAlign={{ base: "start", md: "start" }}
                        fontSize={{
                            base: "24px",
                            md: "32px",
                        }}
                        pt={{ base: "20px", md: "25px" }}
                    >
                        {get(data, 'title', '')}
                    </Text>
                </HStack>
                <ScrollView
                    showsHorizontalScrollIndicator={false}
                    horizontal={{
                        base: true,
                        md: true,
                    }}
                    contentContainerStyle={{ flexGrow: 1 }}
                >
                    <Box w={{ base: "100%", md: "69%" }} px={["5px", "39px"]} pt={"30px"} pb={"6px"}>
                        <TabListHeader
                            tabList={tabList}
                            handleChange={handleChange}
                            selectedTab={selectedTab}
                        />
                    </Box>
                </ScrollView>
            </VStack>

        )
    }

    const showTest = (data) => {
        return (
            <VStack
                justifyContent={"flex-start"}
                w={{ base: "100%", md: "100%" }}
            >
                <>
                    <HStack h={"353px"}>
                        <HStack pl={["15px", "39px"]} pt={["20px", "54px"]} width={"50%"}>
                            <VStack>
                                <HStack flexWrap={"wrap"} alignItems={"center"}>
                                    <Text
                                        fontSize={["24px", "32px"]}
                                        color="#17181A"
                                        fontWeight="BOLD"
                                        textAlign={"left"}
                                        bold
                                        mt={{ base: "12px", md: 0 }}
                                        w={["65%", "100%"]}
                                        fontFamily={"Nunito Sans"}
                                    >
                                        {data?.title}
                                    </Text>
                                </HStack>
                                <HStack pt={"24px"}>
                                    <Image source={"https://images.netskill.com/frontend/netskill/courses/wallet.webp"} size={"20px"} alt="WalletImage" />
                                    <Text
                                        fontSize="14px"
                                        color="#262627"
                                        fontWeight="500"
                                        fontFamily={"Nunito Sans"}
                                        textAlign={"left"}
                                        pl={2}
                                    >
                                        {data?.banner?.course_content[0]?.title}
                                        {console.log("44444444444444444444",data?.banner)}
                                    </Text>
                                </HStack>
                                <HStack pt={"17px"}>
                                    <Image source={"https://images.netskill.com/frontend/netskill/courses/job.webp"} size={"20px"} alt="CoursejobImage" />
                                    <Text
                                        fontSize="14px"
                                        color="#262627"
                                        fontWeight="500"
                                        fontFamily={"Nunito Sans"}
                                        textAlign={"left"}
                                        pt={0.5}
                                        pl={2}
                                    >
                                        {data?.banner?.course_content[1]?.title}
                                    </Text>
                                </HStack>
                                <HStack pt={"17px"}>
                                    <Image source={"https://images.netskill.com/frontend/netskill/courses/hiring.webp"} size={"20px"} alt="CourseImage" />
                                    <Text
                                        fontSize="14px"
                                        color="#262627"
                                        fontWeight="500"
                                        fontFamily={"Nunito Sans"}
                                        textAlign={"left"}
                                        pt={0.5}
                                        pl={2}
                                    >
                                        {data?.banner?.course_content[2]?.title}
                                    </Text>
                                </HStack>
                                <Button mt={"44px"}
                                    variant={"raised"}
                                    w={"217px"}
                                    px={4}
                                    bg="#ff5e63"
                                    _hover={{
                                        bg: "error.500",
                                    }}
                                    onPress={openModal}
                                >
                                    <Text color={"#fff"} fontSize="15px">{data?.banner?.buttons[0]?.text}</Text>
                                </Button>
                              
                                <HStack alignItems={"center"} pt={{base:"15px",md:"25px"}}>
                                    <Image
                                        source={{ uri: 'https://images.netskill.com/frontend/netskill/home/hire.webp' }}
                                        size={'24px'}
                                        alt="HireImage"
                                    />
                                    {/* <Text color={"amber.700"}>{data?.banner?.linkUrl}</Text> */}
                                    <Link isExternal href={callCourse(data)}> 
                                   
                                    <Text w={['100%', "100%"]} pl={"10px"} color={'#17181a'} fontSize={"12px"}>{data?.banner?.linkText} 
                                    {/* <Pressable w={"150px"} py={"20px"}  style={{backgroundColor:"red"}} onPress={()=>callCourse(data)}> */}
                                        <Text textDecorationLine={'underline'} color={'#17181a'} fontSize={"14px"} fontWeight={"600"}> Netskill course</Text>
                                     {/* </Button> */}
                                        {/* </Pressable> */}
                                    </Text> 
                              
                                    </Link>
                                    </HStack>
                                   
                            </VStack>
                        </HStack>
                        <Box width={"50%"}>
                            <Box position={"absolute"} top={["30%", "20%"]} left={["25%", "38%"]}>
                                {courseImg ? 
                                <Image alt="CourseImage" w={["100px", "180px"]} h={["100px", "180px"]} source={courseImg} /> 
                                : null}
                            </Box>
                            <Hidden till="md">
                                <Box position={"absolute"} right={inwdith < 1300 ? "-15%" : "0"} w={"520px"} h={["100%"]} alignSelf={"flex-end"}>
                                    <Image w={"100%"} h={["", "100%"]} resizeMethod={"contain"} source={"https://images.netskill.com/frontend/netskill/testpage/testpage/banner.webp"} alt="BannerImage" />
                                </Box>
                            </Hidden>
                        </Box>
                        <Hidden from="md">
                            <Pressable zIndex={10} position={"absolute"} right={"15px"} top={"20px"} onPress={() => setShowTestdata(0)}>
                            <Image
                                            width={{
                                                md: "25px",
                                                base: 25,
                                            }}
                                            height={{
                                                md: "25px",
                                                base: 20,
                                            }}
                                            source={{ uri: 'https://images.netskill.com/frontend/close.svg' }}
                                            alt="close"
                                            resizeMode="contain"
                                            textAlign={"end"}
                                        />
                            </Pressable>
                        </Hidden>
                    </HStack>
                </>
                <VStack pt={"63px"}>
                    <HStack pl={["12px", "39px"]} pr={["12px", undefined]}>
                        <ScrollView
                            showsHorizontalScrollIndicator={false}
                            horizontal={{
                                base: true,
                                md: true,
                            }}
                            contentContainerStyle={{ flexGrow: 1 }}
                        >
                            {tabList?.map((item) => {
                                return (
                                    <Pressable mr={["25px", "25px"]} onPress={() => handleChange(item?.id)} borderBottomColor={"#ff5e63"} borderBottomWidth={selectedTab === item?.id ? 2 : 0} h={"36px"}>
                                        <Text color={selectedTab === item?.id ? "#ff5e63" : "#17181a"} fontWeight={selectedTab === item?.id ? "bold" : "normal"} fontSize={"16px"}>{item?.tabName}</Text>
                                    </Pressable>
                                )
                            })}
                        </ScrollView>
                    </HStack>
                    <Divider backgroundColor={"#dddddd"} w={'100%'} mt={-0.4} />
                    <TabView setTabCoord={setTab1Coord}>
                        <>
                            <VStack pt={"64px"} width={"100%"} pl={["14px", '39px']}>
                                <Text pb={"24px"} fontSize={["20px", '24px']} style={{ fontWeight: "bold", color: "#17181a" }} color="#747476" fontFamily={"Nunito Sans"}>
                                    Test Structure</Text>
                            </VStack>
                            <HStack pl={["10px", "39px"]}>
                                <VStack width={"100%"} pr={SectionSpacingX}>
                                    {content?.test_structure?.[0]?.header?.map((item, index) => {
                                        return (
                                            <Box
                                                borderRadius={"4px"}
                                                fontFamily={"Nunito Sans"}
                                                mb="16px"
                                            >

                                                <VStack>
                                                    <TouchableOpacity
                                                        onPress={
                                                            () => handleToggleQuestionOne(index)
                                                        }
                                                    >
                                                        <HStack px="20px" borderWidth={1} borderRadius={"4px"}
                                                            borderColor="coolGray.200" py="20px"
                                                            justifyContent={"space-between"}
                                                            alignItems="center"
                                                            // mb={selectedQuestion == index ? "18px" : 0}
                                                            space={2}
                                                        >
                                                            <HStack w={["50%", "65%"]}>
                                                                <Text w={["10%", "5%"]} fontSize="16px" color={'#747476'} fontFamily={"Nunito Sans"}>{index + 1}</Text>
                                                                <Text w={["90%", "95%"]}
                                                                    fontSize="16px"
                                                                    color={"#17181a"}
                                                                    fontFamily={"Nunito Sans"}
                                                                    fontWeight="bold"
                                                                    textAlign={"left"}
                                                                >
                                                                    {item?.title}
                                                                </Text>
                                                            </HStack>
                                                            <Text
                                                                fontSize="16px"
                                                                color={"#17181a"}
                                                                fontFamily={"Nunito Sans"}
                                                                fontWeight="600"
                                                                textAlign={"left"}

                                                            >
                                                                <HStack
                                                                    flexDirection={["column", "row"]}
                                                                    justifyContent={"flex-start"} alignItems={["flex-start", "center"]}
                                                                >
                                                                    <HStack bgalignItems={"center"} mr={"12px"}>
                                                                        <Image
                                                                            source={"https://images.netskill.com/frontend/netskill/courses/board.webp"}
                                                                            size={["12px", "16px"]}
                                                                            pt={5}
                                                                            alt="BoardImage" />

                                                                        <Text color={'#595fff'} fontSize={["12px", "14px"]} semibold pr={[2, 0]} pl={1} fontFamily={"Nunito Sans"}>
                                                                            {item?.project}
                                                                        </Text>
                                                                    </HStack>
                                                                    <HStack ml={"12px"} alignItems={"center"}>
                                                                        <Image pt={5} source={"https://images.netskill.com/frontend/netskill/home/timeIcon.webp"} size={["12px", "16px"]} alt="TimeiconImage" />
                                                                        <Text color={'#595fff'} fontSize={["12px", "14px"]} semibold pr={[2, 4]} pl={1} fontFamily={"Nunito Sans"}>
                                                                            {item?.duration}
                                                                        </Text>
                                                                    </HStack>
                                                                    {index === selectedQuestionOne ?
                                                                        <Image
                                                                            width={{ md: 6, base: 6 }}
                                                                            height={{ md: 6, base: 6 }}
                                                                            resizeMode="contain"
                                                                            source={{ uri: 'https://images.netskill.com/frontend/upArrow.webp' }} alt={"image"} /> 
                                                                            :
                                                                        <Image
                                                                            width={{ md: 6, base: 6 }}
                                                                            height={{ md: 6, base: 6 }}
                                                                            resizeMode="contain"
                                                                            source={{ uri: 'https://images.netskill.com/frontend/downArrow.webp' }} alt={"image"} />
                                                                    }
                                                                </HStack>
                                                            </Text>
                                                        </HStack>
                                                    </TouchableOpacity>

                                                    {selectedQuestionOne == index ? (
                                                        <Box borderWidth={1}
                                                            borderColor="coolGray.200" py="20px">
                                                            <Text pl={'5%'}
                                                                fontSize="14px"
                                                                color="#17181a"
                                                                textAlign={"left"}
                                                                pb="12px"
                                                                fontFamily={"Nunito Sans"}
                                                                lineHeight={"28px"}
                                                            >
                                                                {item?.description}
                                                            </Text>
                                                        </Box>
                                                    ) : null}
                                                </VStack>
                                            </Box>

                                        )
                                    })}
                                </VStack>
                            </HStack>
                            <Button mt={"40px"}
                                variant={"raised"}
                                mb={"80px"}
                                w={"200px"}
                                ml={["5px", "39px"]}
                                bg="#ff5e63"
                                _hover={{
                                    bg: "error.500",
                                }}
                                onPress={openModal}
                            >
                                <Text color={"#fff"} fontSize="15px">Take test to get hired</Text>
                            </Button>
                        </>
                    </TabView>
                    <TabView setTabCoord={setTab2Coord}>
                        {SecondRoute()}
                    </TabView>
                    <TabView setTabCoord={setTab3Coord}>
                        {ThirdRoute()}
                    </TabView>
                    <TabView setTabCoord={setTab4Coord}>
                        {FourthRoute()}
                    </TabView>
                    <TabView setTabCoord={setTab5Coord}>
                        {FifthRoute()}
                    </TabView>
                    <></>
                </VStack>
            </VStack>
        )
    }

    const mainContent = (data) => {
        return (
            <VStack alignSelf={"flex-end"}
                //  w={["100%", "100%", "70%"]}
                w={inwdith < 700 ? "100%" :
                    inwdith > 700 && inwdith < 1290 ? "68%" :
                        "70%"
                }
            >
                <VStack
                    bg="#FFFFFF"
                    zIndex={2}
                    w="100%"
                    pl={["3px", SectionSpacingX]}
                >
                    <HStack
                        justifyContent={"space-between"}
                        flexDirection={{ base: "column", md: "row" }}
                    >
                        {showTest(data)}
                    </HStack>
                </VStack>

            </VStack >
        )
    }

    return (
        <>
            {/* {showStickyHeader ? ( */}
            <>
                {showStickyHeader ?
                    <Animated.View
                        style={{
                            alignSelf: "center",
                            maxWidth: "1640px",
                            position: "fixed",
                            top: inwdith < 700 ? "0px" : "10px",
                            right: 0,
                            width: inwdith < 700 ? "100%" :
                                inwdith > 700 && inwdith < 1290 ? "68%" :
                                    "70%",
                            zIndex: 100,
                            backgroundColor: "black",
                            // opacity: showStickyHeader ? 1 : 0,
                        }}
                    >{stickyHeaderView(content)}
                    </Animated.View>
                    : null}
                {/* <StickyTabs color={"#1D1E21"} marginTop={inwdith < 700 ? "0px" : "10px"}
                    width={inwdith < 700 ? "100%" :
                        inwdith > 700 && inwdith < 1290 ? "68%" :
                            "70%"
                    } showStickyHeader={showStickyHeader} align={"flex-end"}>
                    {stickyHeaderView(content)}
                </StickyTabs> */}

            </>
            {/* ) : null} */}
            <Animated.ScrollView
                onScroll={(e) => handleScroll(e)}
                ref={scrollViewRef}
                style={{ width: "100%", alignSelf: "flex-end", marginTop: '10px', height: height1 }}
            >
               
                {mainContent(content)}
            </Animated.ScrollView>

        </>
    );
}