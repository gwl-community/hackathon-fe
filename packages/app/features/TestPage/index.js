import React, { useEffect, useState } from "react";
import {
    Box,
    HStack,
    Image,
    Text,
    VStack,
    Pressable,
    Hidden,
    ScrollView,
    Button, useToast
} from "native-base";
import api from "../../lib/api";
import DashboardLayout from "../../layouts/DashboardLayout";
import TestContent from "./TestContent";
import { useRouter } from 'solito/router'
import Footer from "../../layouts/Footer";
import { validateEmail } from "../../lib/utils";
import { TouchableOpacity, View, StyleSheet, Modal } from "react-native";
import PhoneInput from 'react-phone-input-2'
import 'react-phone-input-2/lib/style.css'
// import CustomDropdown from "../CoursePreview/components/CustomDropdown";
// import CustomTextField from "../CoursePreview/components/CustomTextField";
import page_generator_api from "../../lib/page_api";
import get from "lodash.get";
import { createParam } from 'solito'
const { useParam } = createParam()


export default function TestPage({ props, cmsBaseUrl, pageGeneratorBaseUrl }) {
    const [height1, setHeight1] = useState(0);
    const [inwdith, setInwidth] = useState(0);
    const { push } = useRouter()


    useEffect(() => {
        const newHeight = window?.innerHeight;
        const newWidth = window?.innerWidth;
        setHeight1(newHeight);
        setInwidth(newWidth)
    }, []);
    let [slugName] = useParam('slugName')
    const [content2, setcontent2] = useState([]);
    const url = '';
    const tabList = [
        {
            id: 0,
            tabName: 'Test Structure'
        }, {
            id: 1,
            tabName: 'Skills Required'
        }, {
            id: 2,
            tabName: 'Jobs'
        }, {
            id: 3,
            tabName: 'Hiring Partners'
        }, {
            id: 4,
            tabName: 'FAQ'
        },

    ];
    const testStructre = [
        {
            id: 0,
            question: "Screening Test",
            answer: "An MCQ round of 30-50 questions to test your Aptitude, English and basic coding skills.\nGet 75 out of 150 marks to qualify for Coding Challenge round ",
            marks: 150,
            time: 45,
        },
        {
            id: 1,
            question: "Coding Challenge ",
            answer: "You are given two coding problems to solve to test your Data Structures & Algorithm skills.\nGet 75 out of 150 marks to qualify for Interview round",
            marks: 150,
            time: 60,
        },
        {
            id: 2,
            question: "Interview",
            answer: "A face-to-face or Zoom interview with a senior software engineer to test your abilities in frontend and backend tech stacks.\nGet 100 out of 200 marks to qualify for Customer Interviews ",
            marks: 200,
            time: 60,
        },
    ];

    const [selectedQuestion, setSelectedQuestion] = useState(1);
    const [selectedQuestionOne, setSelectedQuestionOne] = useState(
        testStructre?.[0]?.id
    );
    const [showToast, setShowToast] = useState({
        msg: "",
        status: null
    });
    useEffect(() => {
        fetchData();
    }, [slugName]);
    const fetchData = async () => {
        try {
            let { data } = await page_generator_api.get(`${pageGeneratorBaseUrl}api/tests-pages?filters[slug]=${slugName}`);
            setcontent2(data?.data?.[0]?.attributes);
        } catch (error) {
            console.log("error", error);
        }
    };


    const handleToggleQuestion = (id) => {
        if (id === selectedQuestion) setSelectedQuestion(-1);
        else setSelectedQuestion(id);
    };

    const handleToggleQuestionOne = (id) => {
        if (id === selectedQuestionOne) setSelectedQuestionOne(-1);
        else setSelectedQuestionOne(id);
    };
    const getPageUrl = (id) => {
        if (url) {
            if (id === 1) {
                if (url.includes("dev")) {
                    return 'https://dev-v2.netskill.com/courses/fullstack-development-course';
                } else if (url.includes("staging")) {
                    return 'https://staging-v2.netskill.com/courses/fullstack-development-course';
                } else {
                    return 'https://www.netskill.com/courses/fullstack-development-course';
                }
            }
            if (id === 2) {
                if (url.includes("dev")) {
                    return 'https://dev-v2.netskill.com/courses/frontend-development-course';
                } else if (url.includes("staging")) {
                    return 'https://staging-v2.netskill.com/courses/frontend-development-course';
                } else {
                    return 'https://www.netskill.com/courses/frontend-development-course';
                }
            }
            if (id === 3) {
                if (url.includes("dev")) {
                    return 'https://dev-v2.netskill.com/courses/backend-development-course';
                } else if (url.includes("staging")) {
                    return 'https://staging-v2.netskill.com/courses/backend-development-course';
                } else {
                    return 'https://www.netskill.com/courses/backend-development-course';
                }
            }
        }
    }

    const skillsData = [
        {
            slug: "fullstack-development-test",
            name: "Fullstack Development Test",
            id: 1,
            type: "Fullstack Development",
            url: "https://images.netskill.com/frontend/netskill/testpage/testpage/fullstack.webp",
            content: "CTC Upto 30 lacs",
            job: "200+ Jobs",
            pageUrl: getPageUrl(1),
            jobList: [
                {
                    id: 1,
                    title: "Fullstack Java Developer",
                    company: "Goodworks Alpha",
                    description: "GoodWorks Alpha is creating a team of remote Back End Developers with core skills in Java, React/Angular, HTML, CSS.",
                    duration: "Full time",
                    package: "8-12 LPA",
                    img: "https://images.netskill.com/frontend/netskill/courses/ga.webp"
                },
                {
                    id: 2,
                    title: "MERN Stack Developer",
                    company: "GoodWorks Alpha",
                    description: "GoodWorks Alpha is a platform for remote developers for international jobs. Fullstack (MERN) skills needed are Node, React, HTML, CSS.",
                    duration: "Full time",
                    package: "8-16 LPA",
                    img: "https://images.netskill.com/frontend/netskill/courses/ga.webp"
                },
                {
                    id: 3,
                    title: "NodeJS Developer",
                    company: "GoodWorks Alpha",
                    description: "GoodWorks Alpha is looking for entry-level developers with core skills in Node, MongoDb, AWS, DSA. Industry experience is a plus. ",
                    duration: "Full time",
                    package: "6-8 LPA",
                    img: "https://images.netskill.com/frontend/netskill/courses/ga.webp",
                    opacity: 0.4

                }
            ],
            skillsRequired: [
                {
                    id: 0, name: "Frontend tech stacks", subText: "React JS, HTML, CSS", img: "https://images.netskill.com/frontend/netskill/testpage/testpage/react.svg",
                },
                {
                    id: 1, name: "Backend tech stacks", subText: "NodeJS or Java", img: "https://images.netskill.com/frontend/netskill/testpage/testpage/node.svg",
                },
                {
                    id: 2, name: "Data Structures and Algorithms", img: "https://images.netskill.com/frontend/netskill/testpage/testpage/node-tree.webp",
                },
            ]
        },
        {
            slug: "frontend-development-test",
            name: "Frontend Development Test",
            id: 2,
            type: "Frontend Development",
            url: "https://images.netskill.com/frontend/netskill/testpage/testpage/react.svg",
            content: "CTC Upto 26 lacs",
            job: "150+ Jobs",
            pageUrl: getPageUrl(2),
            jobList: [
                {
                    id: 1,
                    title: "ReactJS Developer",
                    company: "GoodWorks Alpha",
                    description: "GoodWorks Alpha is looking for entry-level developers with core skills in React, HTML, CSS. Industry experience is a plus.",
                    img: "https://images.netskill.com/frontend/netskill/courses/ga.webp",
                    duration: "Full time",
                    package: "6-8 LPA",
                },
                {
                    id: 2,
                    title: "Front End Developer",
                    company: "GoodWorks Alpha",
                    description: "GoodWorks Alpha is creating a team of remote Front End Developers with core skills in React / Angular, HTML, CSS.",
                    duration: "Full time",
                    package: "8-12 LPA",
                    img: "https://images.netskill.com/frontend/netskill/courses/ga.webp"
                },
                {
                    id: 3,
                    title: "MERN Stack Developer",
                    company: "Facebooks",
                    description: "GoodWorks Alpha is a platform for remote developers for international jobs. Fullstack (MERN) skills needed are Node, React, HTML, CSS.",
                    duration: "Full time",
                    package: "8-16 LPA",
                    img: "https://images.netskill.com/frontend/netskill/courses/ga.webp",
                    opacity: 0.4

                }
            ],
            skillsRequired: [
                {
                    id: 0, name: "Frontend tech stacks", subText: "React JS, HTML, CSS", img: "https://images.netskill.com/frontend/netskill/testpage/testpage/react.svg",
                },
                {
                    id: 1, name: "Data Structures and Algorithms", img: "https://images.netskill.com/frontend/netskill/testpage/testpage/node-tree.webp",
                },
            ]
        },
        {
            slug: "backend-development-test",
            name: "Backend Development Test",
            id: 3,
            type: "Backend Development",
            url: "https://images.netskill.com/frontend/netskill/testpage/testpage/node.svg",
            content: "CTC Upto 30 lacs",
            job: "250+ Jobs",
            pageUrl: getPageUrl(3),
            jobList: [
                {
                    id: 1,
                    title: "NodeJS Developer",
                    company: "GoodWorks Alpha",
                    description: "GoodWorks Alpha is looking for entry-level developers with core skills in Node, MongoDb, AWS, DSA. Industry experience is a plus.",
                    duration: "Full time",
                    package: "6-8 LPA",
                    img: "https://images.netskill.com/frontend/netskill/courses/ga.webp",
                },
                {
                    id: 2,
                    title: "Back End Developer",
                    company: "GoodWorks Alpha",
                    description: "GoodWorks Alpha is creating a team of remote Back End Developers with core skills in Node / Java, HTML, CSS. ",
                    duration: "Full time",
                    package: "8-12 LPA",
                    img: "https://images.netskill.com/frontend/netskill/courses/ga.webp"
                },
                {
                    id: 3,
                    title: "MERN Stack Developer",
                    company: "GoodWorks Alpha",
                    description: "GoodWorks Alpha is a platform for remote developers for international jobs. Fullstack (MERN) skills needed are Node, React, HTML, CSS.",
                    duration: "Full time",
                    package: "8-16 LPA",
                    img: "https://images.netskill.com/frontend/netskill/courses/ga.webp",
                }
            ],
            skillsRequired: [
                {
                    id: 0, name: "Backend tech stacks", subText: "NodeJS or Java", img: "https://images.netskill.com/frontend/netskill/testpage/testpage/node.svg",
                },
                {
                    id: 1, name: "Data Structures and Algorithms", img: "https://images.netskill.com/frontend/netskill/testpage/testpage/node-tree.webp",
                },
            ]
        },
    ];
    const yearList = [
        { text: "2000", value: "2000" },
        { text: "2001", value: "2001" },
        { text: "2002", value: "2002" },
        { text: "2003", value: "2003" },
        { text: "2004", value: "2004" },
        { text: "2005", value: "2005" },
        { text: "2006", value: "2006" },
        { text: "2007", value: "2007" },
        { text: "2008", value: "2008" },
        { text: "2009", value: "2009" },
        { text: "2010", value: "2010" },
        { text: "2011", value: "2011" },
        { text: "2012", value: "2012" },
        { text: "2013", value: "2013" },
        { text: "2014", value: "2014" },
        { text: "2015", value: "2015" },
        { text: "2016", value: "2016" },
        { text: "2017", value: "2017" },
        { text: "2018", value: "2018" },
        { text: "2019", value: "2019" },
        { text: "2020", value: "2020" },
        { text: "2021", value: "2021" },
        { text: "2022", value: "2022" }
    ];
    const courseList = [
        { text: "Fullstack Development Course", value: "Fullstack Development Course" },
        { text: "Frontend Development Course", value: "Frontend Development Course" },
        { text: "Backend Development Course", value: "Backend Development Course" },
        { text: "iOS Development with Swift", value: "iOS Development with Swift" },
        { text: "Android Development with Kotlin", value: "Android Development with Kotlin" },
        { text: "Manual Testing Course", value: "Manual Testing Course" },
        
      ]
    const [showTestdata, setShowTestdata] = useState(0);
    const [courseImg, setCourseImg] = useState(inwdith > 700 ? get(content2, 'banner.media.data[0].attributes.url', '') : null);
    const index = props?.route?.params?.slug ? skillsData.findIndex(object => {
        return object.slug === props?.route?.params?.slug;
    }) : 0;
    const [selectedTest, setSelectedTest] = useState(skillsData[index])
    const [showBookSlot, setShowBookSlot] = useState(false);
    const [fullName, setFullName] = useState("");
    const [slug, setSlug] = useState(props);
    useEffect(() => {
        setSelectedTest(skillsData[index])
    }, [index])
    const [email, setEmail] = useState("");
    const [selectedYear, setSelectedYear] = useState("");
    const [selectedCourse, setSelectedCourse] = useState('')
    const [phone, setPhone] = useState("");
    const [err, setErr] = useState(null);
    const [errEmail, setErrEmail] = useState(null);
    const [phoneCode, setPhoneCode] = useState("");
    const toast = useToast();
    const handleSubmit = async () => {
        if (phone) {
            const onlyNum = phone.slice(phoneCode.length);
            const firstNum = onlyNum.charAt(0);
            if (phoneCode == 91 && (onlyNum.length < 10 || firstNum == 0)) {
                setErr("Please enter a valid phone number");
            }
            else {
                const checkEmail = validateEmail(email);
                if (!checkEmail) {
                    try {
                        let { data } = await api.post(`${cmsBaseUrl}api/contact-uses`, {
                            data: {
                                name: fullName,
                                email: email,
                                phone: onlyNum,
                                country_code: "+" + phoneCode,
                                // course: selectedTest?.name,
                                course: selectedCourse,
                                graduation_year: selectedYear,
                                is_book_or_contact: "Book_Slot",
                            },
                        });

                        setFullName("");
                        setEmail("");
                        setPhone("");
                        setSelectedYear("")
                        setSelectedCourse("")
                        setShowBookSlot(false);
                        setShowToast({ msg: 'Submitted Successfully !!', status: "error" });
                        setTimeout(() => setShowToast({
                            msg: "",
                            status: null
                        }), 2200);
                    } catch (error) {
                        console.log(error);
                    }
                }
                else setErrEmail("Please check your email id");
            }
        } else setErr("Please enter phone number");
    };
    const [meta, setMeta] = useState(selectedTest?.type);
    const questions = [
        {
            id: 1,
            question: `Which tech stacks are required to pass this ${selectedTest?.name}?`,
            answer: 'You need to focus on the following',
            bullets: selectedTest?.id == 2 ? [
                'Frontend tech stacks - ReactJS, HTML, CSS,',
                'Data Structures and Algorithms'
            ] : selectedTest?.id == 3 ? [
                'Backend tech stacks - NodeJS or Java',
                'Data Structures and Algorithms'
            ] : [
                'Frontend tech stacks - ReactJS, HTML, CSS,',
                'Backend tech stacks - NodeJS or Java',
                'Data Structures and Algorithms'
            ]
        },
        {
            id: 2,
            question: "How much do I pay for the Netskill Placement Tests?",
            answer: "Netskill Placement Tests are absolutely free. You don't have to pay anything. Rather you will end up landing a top paying job!"
        },
        {
            id: 3,
            question: "What happens if I don’t clear the tests?",
            answer: 'We have a minimum score that you need to achieve to clear the tests. In case you don’t pass the tests, you can always reapply after 60 days. \nFurther, you can take up one of our Netskill courses to prepare for the tests. Our courses are driven by industry-best curriculum and live mentor support on top of an AI-powered platform that guides you based on your strengths and areas of weakness.'
        },
        {
            id: 4,
            question: "Who can take the Netskill Placement Tests?",
            answer: 'Anyone who has passed 12th grade can take up the tests. All you need to do is a good internet connection and laptop to take up the tests at your convenience.'
        },
        {
            id: 5,
            question: "Can experienced professionals take up Netskill Placement Tests?",
            answer: 'Netskill Placement Tests are more suited to students, freshers and professionals upto 3 years of experience. However, that doesn’t mean you cannot take up the tests if you have more experience. There are professionals who would want to change their careers (for e.g. from QA or Teaching to becoming a Software Engineer). We definitely encourage you to try our tests and gain access to a path for career change. \nHowever, if you have more than 3+ years of experience, we encourage you to take up tests at GoodWorks Alpha (which is our exclusive placement partner) and land top paying international jobs, with possibility to have onsite travel and immigration opportunities.'
        },
    ];

    const closeModal = () => {
        setShowBookSlot(false)
        setFullName("");
        setEmail("");
        setSelectedYear("");
        setSelectedCourse("");
        setPhone("");
        setPhoneCode("");
    };

    const setTest = (data) => {
        setCourseImg(null);
        setMeta(null);
        setShowTestdata(data?.id);
        setSelectedTest(data);
        
        push(`/test/${data?.slug}`)
    }

    useEffect(() => {
        setTimeout(() => {
            setMeta(selectedTest?.name)
            setCourseImg(selectedTest?.url || selectedTest?.media?.data?.[0]?.attributes?.url)
            // setCourseImg(selectedTest?.url)
        }, 30)
    }, [selectedTest])


    const showSideBar = () => {
        return (
            <VStack
                zIndex={4}
                bg={"white"}
                h={height1}
                position={["relative", "absolute"]}
                top={[0, 2.5]}
                justifyContent={"flex-start"}
                w={inwdith < 700 ? "100%" :
                    inwdith > 700 && inwdith < 1290 ? "32%" :
                        "30%"
                }
                style={{
                    borderColor: "coolGray.700",
                    borderRight: "1px solid #2222",
                }}
                pr={inwdith < 1290 ? "10px" : "19px"}
                pl={["4%", "8%"]}
                mb={"5%"}
                flexWrap={"wrap"}
            >
                <ScrollView
                    contentContainerStyle={{
                        position: "absolute", top: 50, width: "100%", height: height1 < 730 ? "120%" : "110%",
                    }}
                    showsVerticalScrollIndicator={false}
                >
                    <Text
                        fontSize="22px"
                        color="#17181A"
                        fontWeight="500"
                        textAlign={"left"}
                        fontFamily={"Nunito Sans"}
                        pb="24px"
                        bold
                    >
                        Select tests
                    </Text>
                    <Box
                        fontSize="22px"
                        color="#17181A"
                        fontWeight="500"
                        textAlign={"left"}
                        fontFamily={"Nunito Sans"}
                        mb="24px"
                    >
                        <Pressable >
                            {content2?.select_test?.course_content?.map((item, i) => {
                               
                                return (
                                    
                                    <Box py={2} >

                                        <Pressable
                                            onPress={() => setTest(item)}
                                            borderWidth={1.5}
                                            borderColor={item?.slug === slugName ? "#595fff" : "coolGray.200"}
                                            py={2}
                                            pl={"12px"}
                                            borderRadius={"4px"}
                                            backgroundColor={item?.slug === slugName ? "rgba(89, 95, 255, 0.03)" : "white"}
                                            cursor={"pointer"}
                                        >
                                            <HStack >
                                                <VStack pt={4}>
                                                    <Image size={"28px"} source={item?.media?.data[0]?.attributes?.url} alt="TestImage" />
                                                </VStack>
                                                <HStack>
                                                    <VStack py={2}>
                                                        <Text
                                                            color="#262627"
                                                            fontSize={{
                                                                md: "16",
                                                            }}
                                                            fontFamily={"Nunito Sans"}
                                                            bold
                                                            paddingLeft="14px"
                                                        >
                                                            {item?.title}
                                                        </Text>
                                                        <HStack alignItems={"center"}>
                                                            <Text
                                                                // pr={2}
                                                                pt={1}
                                                                color="#262627"
                                                                fontSize={"12px"}
                                                                paddingLeft="14px"
                                                                fontFamily={"Nunito Sans"}
                                                            >
                                                                {item?.description}
                                                            </Text>
                                                            <Text mx={2} mt={-4} fontSize={"30px"} color="#262627" >.</Text>
                                                            <Text
                                                                pt={1}
                                                                color="#262627"
                                                                fontSize={"12px"}
                                                                fontFamily={"Nunito Sans"}
                                                            >
                                                                {item?.jobsTitle}
                                                            </Text>
                                                        </HStack>
                                                    </VStack>
                                                </HStack>
                                            </HStack>
                                        </Pressable>


                                    </Box>
                                );
                            })}
                        </Pressable>

                    </Box>
                </ScrollView>
            </VStack>
        )
    }
    return (
        <>
            <DashboardLayout
                title="Test"
                displayLogo={true}
                displayMenuButton={false}
                displayScreenTitle={false}
                displaySidebar={false}
                displayAlternateMobileHeader={false}
                navigation={props?.navigation}
                displayIcons={true}
                //hideFooter={true}
                position={"absolute"}
            >
                {showToast?.msg && (
                    <HStack zIndex={10000} bg={showToast?.status == "success" ? "#b8f1d2" : "#f6ced3"} position={"absolute"} top={70} left={{base:8,md:550}} w={"300px"} h={"50px"} justifyContent={"space-evenly"} alignItems={"center"} borderRadius={"4px"} >
                        <Text fontSize={"18px"} color={"black"}>{showToast?.msg}</Text>
                    </HStack>
                 )} 
                <Hidden till="md">
                    {
                        <TestContent
                            props={props}
                            tabList={tabList}
                            // jobList={jobList}
                            questions={questions}
                            testStructre={testStructre}
                            selectedQuestion={selectedQuestion}
                            selectedQuestionOne={selectedQuestionOne}
                            handleToggleQuestion={handleToggleQuestion}
                            handleToggleQuestionOne={handleToggleQuestionOne}
                            courseImg={courseImg}
                            selectedTest={selectedTest}
                            content={content2}
                            setShowTestdata={setShowTestdata}
                            showTestdata={showTestdata}
                            // skillsData={skillsData}
                            setShowBookSlot={setShowBookSlot}
                            slugName={slugName}
                        />
                    }
                </Hidden>
                <Hidden till="md">
                    {showSideBar()}
                </Hidden>
                <Hidden from="md">
                    {showTestdata === 0 ? showSideBar() :
                        <TestContent
                           showToast={showToast}
                            props={props}
                            tabList={tabList}
                            questions={questions}
                            testStructre={testStructre}
                            selectedQuestion={selectedQuestion}
                            selectedQuestionOne={selectedQuestionOne}
                            handleToggleQuestion={handleToggleQuestion}
                            handleToggleQuestionOne={handleToggleQuestionOne}
                            courseImg={courseImg}
                            selectedTest={selectedTest}
                            setShowTestdata={setShowTestdata}
                            content={content2}
                            showTestdata={showTestdata}
                            skillsData={skillsData}
                            setShowBookSlot={setShowBookSlot}
                            slugName={slugName}
                        />
                    }
                </Hidden>

                {/* <Modal
                    animationType={"fade"}
                    transparent={true}
                    backdropOpacity={0.1}
                    visible={showBookSlot}
                    onBackdropPress={closeModal}
                >
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                            <VStack height={{ md: 430 }} width={{ md: 580 }}>
                                <HStack justifyContent={"flex-end"}>
                                    <TouchableOpacity onPress={closeModal}>
                                        <Image
                                            width={{
                                                md: "25px",
                                                base: 25,
                                            }}
                                            height={{
                                                md: "25px",
                                                base: 20,
                                            }}
                                            source={{ uri: 'https://images.netskill.com/frontend/close.svg' }}
                                            alt="close"
                                            resizeMode="contain"
                                            textAlign={"end"}
                                        />
                                    </TouchableOpacity>
                                </HStack>
                                <Text
                                    color="#17181A"
                                    fontSize={"22px"}
                                    fontWeight="500"
                                    mb="32px"
                                    fontFamily={'Nunito Sans'}
                                >
                                    {"Book your slot today!"}
                                </Text>
                                <VStack>
                                    <CustomTextField
                                        value={fullName}
                                        setValue={setFullName}
                                        label="Full Name"
                                    />
                                    <CustomTextField
                                        value={email}
                                        setValue={setEmail}
                                        label="Email Address"
                                        setErrEmail={setErrEmail}
                                    />
                                    {errEmail ? (
                                        <Text
                                            mt={-3}
                                            textAlign={"end"}
                                            fontSize={"13px"}
                                            color={"error.500"}
                                            fontFamily={'Nunito Sans'}
                                        >
                                            {errEmail}
                                        </Text>
                                    ) : null}
  <Box
                // maxW={['100%', '%']}
                // width={['100%', '45%']}
                // height={70}
                borderColor={'#17181A'}
                mt={-2}
              >
                <CustomDropdown
                  value={selectedCourse}
                  setValue={setSelectedCourse}
                  options={courseList}
                  label="Select course"
                />
              </Box>
                                    <HStack mt={3}
                                        flexDirection={["column", "row"]}
                                        justifyContent={["flex-start", "space-between"]}
                                        // space="2"
                                        alignItems="center"
                                    >
                                        <Box
                                            maxW={["100%", "47%"]}
                                            width={["100%", "47%"]}
                                            borderColor={"#17181a"}
                                        >
                                            <CustomDropdown
                                                value={selectedYear}
                                                setValue={setSelectedYear}
                                                options={yearList}
                                                label="Graduation Year"
                                            />
                                        </Box>
                                        <Box w={{ base: '100%', md: '47%' }} >
                                            <PhoneInput
                                                inputStyle={{
                                                    width: '100%',
                                                    height: 45,
                                                    fontSize: '13px',
                                                    paddingLeft: '48px',
                                                    borderRadius: '5px'
                                                }}
                                                buttonStyle={{ backgroundColor: 'white' }}
                                                dropdownStyle={{ width: 250, position: "absolute", bottom: 35 }}
                                                country={'in'}
                                                value={phone}
                                                onChange={(phone, code) => { setPhone(phone), setPhoneCode(code?.dialCode), setErr(null); }}
                                            />
                                        </Box>
                                    </HStack>
                                    {err ? (
                                        <Text
                                            textAlign={"end"}
                                            mt={-3}
                                            fontSize={"13px"}
                                            color={"error.500"}
                                        >
                                            {err}
                                        </Text>
                                    ) : null}
                                    <Button
                                        variant="raised"
                                        mt={{ md: 10, base: 5 }}
                                        size="md"
                                        borderRadius="4"
                                        _text={{
                                            fontWeight: "medium",
                                        }}
                                        _light={{
                                            bg: "primary.900",
                                        }}
                                        _dark={{
                                            bg: "primary.700",
                                        }}
                                        onPress={() => {
                                            handleSubmit();
                                        }}
                                        isDisabled={
                                            !fullName ||
                                            !email ||
                                            !selectedYear ||
                                            !selectedCourse
                                        }
                                        _hover={{
                                            bg: "error.500",
                                        }}
                                        py="3"
                                    >
                                        Submit
                                    </Button>
                                </VStack>
                            </VStack>
                        </View>
                    </View>
                </Modal> */}
                {/* <Footer /> */}
            </DashboardLayout>
        </>
    );
}

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: 'rgba(0,0, 0, 0.5)',

    },
    modalView: {
        // margin: 10,
        backgroundColor: "white",
        borderRadius: 10,
        padding: 30,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
    },

});
