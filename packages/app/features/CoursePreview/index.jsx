import {
  Box,
  Button,
  HStack,
  Hidden,
  Image,
  Pressable,
  Text,
  VStack,
  useToast,
  useColorModeValue
} from "native-base";
import React, { useEffect, useState, useRef } from "react";
import { useRouter } from 'next/router'
import moment from "moment";
import dynamic from 'next/dynamic'
import { ScrollView, Animated, Modal, View, StyleSheet } from "react-native";
import api from "../../lib/api";
import { TouchableOpacity } from "react-native";
import { validateEmail } from "../../lib/utils";
import PhoneInput from 'react-phone-input-2'
import 'react-phone-input-2/lib/style.css'
import page_generator_api from "../../lib/page_api";
import get from "lodash.get";
import { LinearGradient } from 'expo-linear-gradient';
import { getData, storeData } from "../../lib/utils";
import { createParam } from 'solito';
import {getCalclulatedDate} from "../../lib/utils";

const { useParam } = createParam()


const DashboardLayout = dynamic(() => import('../../layouts/DashboardLayout'));
const Overview = dynamic(() => import('./components/Overview'));
const TutorsMentor = dynamic(() => import('./components/TutorsMentor'))
const ProgramStructure = dynamic(() => import('../../components/section/ProgramStructure'))
const CourseCurriculum = dynamic(() => import('./components/CourseCurriculum'))
const Faq = dynamic(() => import('./components/Faq'))
const Hiring = dynamic(() => import('./components/Hiring'))
const Fee = dynamic(() => import('./components/Fee'))
const Footer = dynamic(() => import('../../layouts/Footer'));
const CustomTextField = dynamic(() => import('./components/CustomTextField'));
const FloatingLabelInput = dynamic(() => import('../../components/FloatingLabelInput'));
const CustomDropdown = dynamic(() => import('./components/CustomDropdown'));


export default function CoursePreview({ props, content2, cmsBaseUrl, pageGeneratorBaseUrl }) {
  const [slug] = useParam('slug')
  const [userDetails, setUserDetails] = useState();
  const router = useRouter()
  const [showSlotPopup, setShowSlotPopup] = useState(false);
  const [showDetailPopup, setShowDetailPopup] = useState(false);
  const [fullName, setFullName] = useState("");
  const [email, setEmail] = useState("");
  const [selectedYear, setSelectedYear] = useState("");
  const [phone, setPhone] = useState("");
  const [pdf, setPdf] = useState();
  const [err, setErr] = useState(null);
  const [errEmail, setErrEmail] = useState(null);
  const [phoneCode, setPhoneCode] = useState("");
  const [content, setContent] = useState(content2);
  let { attributes = {} } = content;
  const overviewData = content2?.contentSections?.find((d) => d.__component === 'sections.overview');
  const BannerData = content2?.contentSections?.find((d) => d.__component === 'sections.course-banner');
  const mentoreListData = content2?.contentSections?.find((d) => d.__component === 'sections.mentor-list');
  const programStructureData = content2?.contentSections?.find((d) => d.__component === 'sections.program-structure');
  const bottomActionData = content2?.contentSections?.find((d) => d.__component === 'sections.bottom-actions');
  const mediaListData = content2?.contentSections?.find((d) => d.__component === 'sections.media-list');
  const featurecoloumnData = content2?.contentSections?.find((d) => d.__component === 'sections.feature-columns');
  const offerdetailsData = content2?.contentSections?.find((d) => d.__component === 'sections.offer-details');
  const testimonilaData = content2?.contentSections?.find((d) => d.__component === 'sections.testimonials-group');
  const certificateData = content2?.contentSections?.find((d) => d.__component === 'sections.banner');
  const stepsData = content2?.contentSections?.find((d) => d.__component === 'sections.steps');
  const faqData = content2?.contentSections?.find((d) => d.__component === 'sections.faqs');
  const curriculamData = content2?.contentSections?.find((d) => d.__component === 'sections.course-curriculum');
  const featuresData = content2?.contentSections?.find((d) => d.__component === 'sections.feature-sequence');
  const planFeeData = content2?.contentSections?.find((d) => d.__component === 'sections.plan-structure');
  const planStructureData = planFeeData?.plan_fees?.data[0]?.attributes?.plan_structure
  const priceArray = planStructureData?.slice(2, 3)
  const [selectedPlan1, setSelectedPlan1] = useState(false)
  const [bookSlotArray, setBookSlotArray] = useState();

  useEffect(() => {
    const getPlan = async () => {
      let plan = await getData("plan", true);
      if (plan) setSelectedPlan1(plan)
      return plan;
    }
    getPlan();
  }, [])
  const basicPlan = () => {
    setSelectedPlan1(true);
  }
  const premiumPlan = () => {
    setSelectedPlan1(false)
  }
  useEffect(() => {
    storeData("plan", JSON.stringify(selectedPlan1));
  }, [selectedPlan1])
  const bookSlot = async () => {
    const userData = await getData("userData", true);
    if (userData) {
      router.push({
        pathname: `/plan-fee/${slug}`,
        query: {
          priceplan: true,
        },
      })
    } else {
      router.push({
        pathname: `/signup`,
        query: {
          slug: slug,
        },
      })
    }
  }
  const fetchData = async () => {
    try {
      let { data } = await page_generator_api.get(`${pageGeneratorBaseUrl}api/pages?filters[slugName]=${slug}`);
      setContent(data?.data[0]);
      setPdf(data?.data[0]?.attributes?.contentSections[0]?.marketing_course?.data?.attributes?.download_link);
      bookSlotData(data?.data[0]?.attributes?.contentSections[14]?.plan_fees?.data[0]?.attributes.course_id);
    } catch (error) {
      console.log("error", error);
    }
  };

  const bookSlotData = async (cid) => {
    const userData = await getData("userData", true);
    try {
      let { data } = await api.get(`${cmsBaseUrl}api/course-enrollments?filters[user]=${userData?.user?.id}&filters[courses]=${cid}`);
      setBookSlotArray(data?.data)
    } catch (error) {
      console.log("error", error);
    }
  };
  const getUserData = async () => {
    const userData = await getData("userData", true);
    setUserDetails(userData?.user);
    if (userData?.user) {
      setFullName(userData?.user?.username);
      setEmail(userData?.user?.email);
      setPhone(userData?.user?.mobile);
    }
  }

  useEffect(() => {
    getUserData();
  }, [])
  useEffect(() => {
    fetchData();
  }, [slug]);

  const tabList = [
    {
      title: "Overview",
      id: 1,
    },
    {
      title: "Tutors and Mentors",
      id: 2,
    },
    {
      title: "Course Curriculum",
      id: 3,
    },
    {
      title: "Hiring Companies",
      id: 4,
    },
    {
      title: "Plans & Fees",
      id: 5,
    },
    {
      title: "FAQs",
      id: 6,
    },
  ];
  const staticList = [
    {
      icon: "https://images.netskill.com/frontend/netskill/courseDetail/live.png",
      title: "Live Classes",
    },
    {
      icon: "https://images.netskill.com/frontend/netskill/courseDetail/AI.png",
      title: "AI Driven Education Platform",
    },
    {
      icon: "https://images.netskill.com/frontend/netskill/courseDetail/project.png",
      title: "Industry Projects",
    },
    {
      icon: "https://images.netskill.com/frontend/netskill/courseDetail/doubt.png",
      title: "Resolve doubts with Mentors",
    },
    {
      icon: "https://images.netskill.com/frontend/netskill/courseDetail/placement.png",
      title: "100% Placement Assistance",
    },
  ];
  const tutorData = [
    {
      name: "Murali M",
      position: "Tech Architect",
      image:
        "https://images.netskill.com/frontend/netskill/courseDetail/murali.webp",
      company: [
        {
          name: "https://images.netskill.com/frontend/netskill/courseDetail/NetSkill.svg",
          aspectRatio: 7 / 2,
        },
      ],
    },
    {
      name: "Swayangjit Parida",
      position: "Tech Architect",
      image:
        "https://images.netskill.com/frontend/netskill/courseDetail/swayangjit.webp",
      company: [
        {
          name: "https://images.netskill.com/frontend/netskill/courseDetail/mahindra.webp",
          aspectRatio: 2 / 1,
        },
        {
          name: "https://images.netskill.com/frontend/netskill/courseDetail/ekstep.webp",
          aspectRatio: 2 / 1,
        },
      ],
    },
    {
      name: "Prasang Misra",
      position: "Tech Lead",
      image:
        "https://images.netskill.com/frontend/netskill/courseDetail/prasang.webp",
      company: [
        {
          name: "https://images.netskill.com/frontend/netskill/courseDetail/mercedes.webp",
          aspectRatio: 4 / 1,
        },
      ],
    },
  ];

  const partners = [
    "https://images.netskill.com/frontend/netskill/courseDetail/C1.png",
    "https://images.netskill.com/frontend/netskill/courseDetail/C2.png",
    "https://images.netskill.com/frontend/netskill/courseDetail/C3.png",
    "https://images.netskill.com/frontend/netskill/courseDetail/C4.png",
    "https://images.netskill.com/frontend/netskill/courseDetail/C5.png",
    "https://images.netskill.com/frontend/netskill/courseDetail/C6.png",
    "https://images.netskill.com/frontend/netskill/courseDetail/C7.png",
    "https://images.netskill.com/frontend/netskill/courseDetail/C8.png",
    "https://images.netskill.com/frontend/netskill/courseDetail/C9.png",
    "https://images.netskill.com/frontend/netskill/courseDetail/C10.png",
  ]
  const yearList = [
    { text: "2000", value: "2000" },
    { text: "2001", value: "2001" },
    { text: "2002", value: "2002" },
    { text: "2003", value: "2003" },
    { text: "2004", value: "2004" },
    { text: "2005", value: "2005" },
    { text: "2006", value: "2006" },
    { text: "2007", value: "2007" },
    { text: "2008", value: "2008" },
    { text: "2009", value: "2009" },
    { text: "2010", value: "2010" },
    { text: "2011", value: "2011" },
    { text: "2012", value: "2012" },
    { text: "2013", value: "2013" },
    { text: "2014", value: "2014" },
    { text: "2015", value: "2015" },
    { text: "2016", value: "2016" },
    { text: "2017", value: "2017" },
    { text: "2018", value: "2018" },
    { text: "2019", value: "2019" },
    { text: "2020", value: "2020" },
    { text: "2021", value: "2021" },
    { text: "2022", value: "2022" },
  ];
  const [selectedTab, setSelectedTab] = useState(1);
  const scrollViewRef = useRef();
  const [overview, setOverview] = useState(0);
  const [tutors, setTutor] = useState(0);
  const [course, setCourse] = useState(0);
  const [hiring, setHiring] = useState(0);
  const [fee, setFee] = useState(0);
  const toast = useToast();
  const [faq, setFaq] = useState(0);

  const [showStickyHeader, setShowStickyHeader] = useState(false);
  const [changeHeader, setChangeHeader] = useState(false);
  const [headerCoords, setHeaderCoords] = useState(0);
  const [count, setCount] = useState(0);
  const [countForClick, setCountForClick] = useState(0);
  const [modalHeader, setModalHeader] = useState(null);
  const [manuallyClicked, setManuallyClicked] = useState(false);
  const [finalTitle, setFinalTitle] = useState(null);
  const [height, setHeight] = useState();
  const [inwdith, setInwidth] = useState();
  const [window_, setWindow] = useState();

  useEffect(() => {
    const newHeight = window?.innerHeight;
    const newWidth = window?.innerWidth;
    setInwidth(newWidth)
    setHeight(newHeight);
    setWindow(window);
  }
    , [])

  var lastScroll = 0;
  const scrollMargin = 670;
  let courseName = finalTitle?.replace("Development Course", "").toLowerCase();
  const courseListHire = [
    {
      id: 1,
      title: "Students",
      subTitle: `With this course, you can start your career as a ${courseName}developer. Ideal to start in 2nd or 3rd year of your Engineering course.`,
      img: "https://images.netskill.com/frontend/netskill/courseDetail/1.png",
    },
    {
      id: 2,
      title: "Freshers",
      subTitle: `Fresh graduates with zero industry experience can take this course to become a ${courseName}web developer.`,
      img: "https://images.netskill.com/frontend/netskill/courseDetail/2.png"
    },
    {
      id: 3,
      title: "Working Professionals",
      subTitle: `Early-career professionals (ideally from 1-3 years of experience) can take this course to become a ${courseName}web developer.`,
      img: "https://images.netskill.com/frontend/netskill/courseDetail/3.png"
    },
    {
      id: 4,
      title: "Freelancers/Consultants",
      subTitle: `You are currently a freelancer, but want to be a pro in ${courseName}development! This is the right course for you.`,
      img: "https://images.netskill.com/frontend/netskill/courseDetail/4.png"
    },
    {
      id: 4,
      title: "Career changers",
      subTitle: `Upskilling & reskilling are critical. This course will help you turn around your career & make you a successful ${courseName}developer.`,
      img: "https://images.netskill.com/frontend/netskill/courseDetail/5.png"
    },
  ]
  useEffect(() => {
    window.addEventListener('scroll', handleScroll)
    return () => window.removeEventListener('scroll', handleScroll)
  })
  const handleScroll = () => {
    let currentScroll = window.scrollY; // Get Current Scroll Value
    if (currentScroll > 0
      // && lastScroll <= currentScroll
    ) {
      lastScroll = currentScroll;
      if (window.scrollY > headerCoords + 100)
        setChangeHeader(true);
      if (showStickyHeader && !manuallyClicked) {
        if (
          window.scrollY > overview - scrollMargin -100 && window.scrollY < tutors - scrollMargin
        ) {
          setSelectedTab(1);
        }
        if (
          window.scrollY > tutors - 700 &&
          window.scrollY < course - scrollMargin
        ) {
          setSelectedTab(2);
        }
        if (
          window.scrollY > course - 700 &&
          window.scrollY < hiring - scrollMargin
        ) {
          setSelectedTab(3);
        }
        if (
          window.scrollY > hiring - 700 &&
          window.scrollY < fee - scrollMargin
        ) {
          setSelectedTab(4);
        }
        if (
          window.scrollY > fee - 700 &&
          window.scrollY < faq - scrollMargin
        ) {
          setSelectedTab(5);
        }
        if (window.scrollY > faq - scrollMargin) {
          setSelectedTab(6);
        }
      }
    } else {
      lastScroll = currentScroll;
      // if (window.scrollY < headerCoords - 200)
      if (window.scrollY == 0)
        setChangeHeader(false);
      setCountForClick(0);
    }
  };

  const handleChange = (newValue) => {
    setSelectedTab(newValue);
    setManuallyClicked(true);
    try {
      if (newValue == 1) {
        window.scrollTo({
          top: showStickyHeader ? overview - scrollMargin : overview,
          left: 0,
          behavior: "smooth",
        });
      }
      else if (newValue == 2) {
        window.scrollTo({
          top: showStickyHeader ? tutors - scrollMargin : tutors,
          left: 0,
          behavior: "smooth",
        });
      }
      else if (newValue == 3) {
        window.scrollTo({
          top: showStickyHeader ? course - scrollMargin : course,
          left: 0,
          behavior: "smooth",
        });
      }
      else if (newValue == 4) {
        window.scrollTo({
          top: showStickyHeader ? hiring - scrollMargin : hiring,
          left: 0,
          behavior: "smooth",
        });
      }
      else if (newValue == 5) {
        window.scrollTo({
          top: showStickyHeader ? fee - scrollMargin : fee,
          left: 0,
          behavior: "smooth",
        });
      }
      else {
        window.scrollTo({
          top: showStickyHeader ? faq - scrollMargin : faq,
          left: 0,
          behavior: "smooth",
        });
      }
      setCountForClick(1);
    } catch (err) {
      console.log(err);
    }
    setTimeout(() => {
      setManuallyClicked(false);
    }, 1000);
  };

  const handleDownload = async () => {
    console.log("phone", phone)
    if (phone) {
      const onlyNum = phone.slice(phoneCode?.length);
      const firstNum = onlyNum.charAt(0);
      if (phoneCode == 91 && (onlyNum.length < 10 || firstNum == 0)) {
        setErr("Please enter a valid phone number");
      } else {
        const checkEmail = validateEmail(email);
        if (!checkEmail) {
          try {
            let { data } = await api.post(`${cmsBaseUrl}api/contact-uses`, {
              data: {
                name: fullName,
                email: email,
                phone: onlyNum,
                country_code: "+" + phoneCode,
                is_book_or_contact: "download_link",
                course: title,
              },
            });
            // console.log("data--------------", data)
            toast.show({
              status: "emerald.500",
              title: "Submitted Successfully",
              placement: "bottom",
            });
            setShowDetailPopup(false);
            setFullName("");
            setEmail("");
            setSelectedYear("");
            setPhone("");
            setPhoneCode("");
            window_?.open(pdf)
            router?.push('/thank-you')
            // Linking.openURL(pdf, "_blank");
            setModalHeader(null);
          } catch (error) {
            console.log(error);
            setErr("Please enter valid  credentials !!");
          }
        }
        else setErrEmail("Please check your email id");
      }
    }
  }
  const handleSubmit = async () => {
    if (phone) {
      const onlyNum = phone.slice(phoneCode.length);
      const firstNum = onlyNum.charAt(0);
      if (phoneCode == 91 && (onlyNum.length < 10 || firstNum == 0)) {
        setErr("Please enter a valid phone number");
      }
      else {
        const checkEmail = validateEmail(email);
        if (!checkEmail) {
          try {
            let { data } = await api.post(`${cmsBaseUrl}api/contact-uses`, {
              data: {
                name: fullName,
                email: email,
                phone: onlyNum,
                country_code: "+" + phoneCode,
                course: title,
                graduation_year: selectedYear,
                is_book_or_contact: modalHeader == "Talk to career counselor" ? "career_counselor" : "Book_Slot",
              },
            });
            toast.show({
              render: () => {
                return <Box bg="emerald.500" px="2" py="1" rounded="sm" mb={5}>
                        You are successfully registered for webinar !!
                      </Box>;
              },
              placement: "top",
            });
            setShowSlotPopup(false);
            setFullName("");
            setEmail("");
            setSelectedYear("");
            setPhone("");
            setPhoneCode("");
             router?.push('/thank-you')
            // props.navigation.navigate("ThankYou", { slug: "book-slot" });
          } catch (error) {
            console.log(error);
          }
        }
        else setErrEmail("Please check your email id");
      }
    } else setErr("Please enter phone number");
  };
  const goToUnitOverView = () => {
    router.push({
      pathname: '/unit-overview/[slug]',
      query: {
        slug: slug,
      }
    })
  }
  const showButtons = (text) => {
    return (
      <HStack
        // ml={2}
        pt={{ base: "0px", md: "10px" }}
        flex={0.15}
        space="6"
        mt={{ base: "18px" }}
        alignItems={{ md: "center" }}
        justifyContent={{ base: "space-between", md: "flex-start" }}
        flexDirection={text === "header" ? ["row", "column", "row"] : ["row"]}
      >
        {bookSlotArray?.length ?
          <Box alignItems="left" size="s" height="50px">
            <Button
              width={["110px", "135px"]}
              size="md"
              bg="#ff5e63"
              variant="raised"
              _hover={{
                bg: "error.500",
              }}
              onPress={() => goToUnitOverView()
                // props.navigation.navigate("UnitOverview", { slug: props?.route?.params?.slug }) }
              }
            >
              <Text fontWeight={"600"} fontSize={["12px", "14px"]}>
                {'Continue course'}
              </Text>
            </Button>
          </Box>
          :
          <Box alignItems="left" size="s" height="50px">
            <Button
              width={["110px", "135px"]}
              size="md"
              bg="#ff5e63"
              variant="raised"
              _hover={{
                bg: "error.500",
              }}
              onPress={bookSlot}
            >
              <Text fontWeight={"600"} fontSize={["12px", "14px"]}>
                {BannerData?.buttons[0]?.text}
              </Text>
            </Button>
          </Box>
        }
        <Box alignItems="left" size="s" height="50px">
          <Button
            width={["170px", "190px"]}
            borderWidth={1}
            borderColor={"white"}
            size="md"
            bg="transparent"
            variant="raised"
            onPress={() => {
              setShowSlotPopup(true);
              setModalHeader("Talk to career counselor");
              if (userDetails) {
                setFullName(userDetails?.username),
                  setEmail(userDetails?.email),
                  setPhone(userDetails?.mobile)
              }
            }}
          >
            <Text fontWeight={"600"} fontSize={["12px", "14px"]} fontFamily={'Nunito Sans'}>
              {'Talk to career counselor'}
            </Text>
          </Button>
        </Box>
      </HStack>
    );
  };
  const value = new Animated.Value(0);
  useEffect(() => {
    if (changeHeader !== showStickyHeader) {
      setShowStickyHeader(changeHeader);
      if (!changeHeader) {
        setSelectedTab(1);
      }
    }
  }, [changeHeader]);

  useEffect(() => {
    if (showStickyHeader) {
      Animated.timing(value, {
        toValue: 1,
        duration: 0,
        useNativeDriver: false,
      }).start();
    }
    if (!showStickyHeader) {
      Animated.timing(value, {
        toValue: 0,
        duration: 20,
        useNativeDriver: true,
      }).start();
    }
  }, [showStickyHeader]);

  const closeModal = () => {
    setShowSlotPopup(false);
    setModalHeader(null);
    setFullName("");
    setEmail("");
    setSelectedYear("");
    setPhone("");
    setPhoneCode("");
    setErr("")
  };
  const closeDetailModal = () => {
    setShowDetailPopup(false);
    setModalHeader(null);
    setFullName("");
    setEmail("");
    setSelectedYear("");
    setPhone("");
    setPhoneCode("");
  }
  const title = content2?.shortName;

  useEffect(() => {
    const titleCase = (str) => {
      var newTitle = str?.toLowerCase().split(' ');
      if (newTitle) {
        for (var i = 0; i < newTitle.length; i++) {
          newTitle[i] = newTitle[i].charAt(0).toUpperCase() + newTitle[i].substring(1);
        }
        return newTitle.join(' ');
      }
    }
    const finalTitle = titleCase(title);
    setFinalTitle(finalTitle);
  }, [title]);

  const openDetailPopUp = () => {
    setShowDetailPopup(true)
    if (userDetails) {
      setFullName(userDetails?.username),
        setEmail(userDetails?.email),
        setPhone(userDetails?.mobile)
    }
  }

  return (
    <>
      {showStickyHeader ? (
        <Animated.View
          style={{
            alignSelf: "center",
            maxWidth: "1640px",
            position: "fixed",
            top: 0,
            width: "100%",
            zIndex: 100,
            backgroundColor: "black",
            opacity: showStickyHeader ? 1 : 0,
          }}
        >
          <VStack px={{ base: "20px", md: "113" }}>
            <HStack
              alignItems={"center"}
              direction={{ base: "column", md: "row" }}
              h={"115px"}
              color={"black"}
            >
              <Text
                // textTransform={"capitalize"}
                w={{ base: "100%", md: "69%" }}
                lineHeight={{ base: "30", md: "39" }}
                fontWeight={"bold"}
                color="#fff"
                textAlign={{ base: "center", md: "start" }}
                fontSize={{
                  base: "21px",
                  md: "40px",
                }}
                pt={{ base: "16px", sm: "16px" }}
                fontFamily={'Nunito Sans'}
              >
                {content2?.shortName}
              </Text>
              <Box w={{ base: "100%", md: "26%" }}>{showButtons("header")}</Box>
            </HStack>
            <ScrollView
              showsHorizontalScrollIndicator={false}
              horizontal={{
                base: true,
                md: true,
              }}
              contentContainerStyle={{ flexGrow: 1 }}
            >
              <HStack
                pt={5}
                alignItems={"center"}
                h={["10%", "10%", undefined]}
                w={"100%"}
              >
                {tabList?.map((item, id) => {
                  return (
                    <Pressable
                      mr={"40px"}
                      justifyContent="flex-start"
                      onPress={() => handleChange(item?.id)}
                    >
                      <Box
                        justifyContent={"space-around"}
                        alignItems={"center"}
                        borderBottomWidth={3}
                        borderColor={
                          selectedTab === item?.id ? "#FF5E63" : "transparent"
                        }
                        h={50}
                      >
                        <Text
                          fontFamily={'Nunito Sans'}
                          fontSize={{ base: "16px", md: "20px" }}
                          fontWeight={"medium"}
                          color={
                            selectedTab === item?.id ? "#FF5E63" : "#ffffff"
                          }
                        >
                          {item?.title}
                        </Text>
                      </Box>
                    </Pressable>
                  );
                })}
              </HStack>
            </ScrollView>
          </VStack>
        </Animated.View>
      ) : null}

      <Animated.ScrollView
        onScroll={(e) => handleScroll()}
        ref={scrollViewRef}
      >
        <DashboardLayout
          title="Course Preview"
          displayMenuButton
          displayScreenTitle={false}
          displaySidebar={false}
          displayAlternateMobileHeader={false}
          navigation={props?.navigation}
          background={"white"}
        >
          {!showStickyHeader ? (
            <VStack bg="#17181a" zIndex={2} h={{ base: "640px", md: "500px" }}>
              <Hidden till="md">
                {BannerData?.marketing_course?.data?.attributes?.header?.media?.data?.attributes?.url ?
                  <Image
                    source={BannerData?.marketing_course?.data?.attributes?.header?.media?.data?.attributes?.url}
                    flex={1}
                    height={"100%"}
                    width={"56%"}
                    position={"absolute"}
                    right={-60}
                    justifyContent={"center"}
                    resizeMode={"cover"}
                    alt="BannerHead"
                  /> : null}
              </Hidden>
              <Hidden from="md">
                {BannerData?.marketing_course?.data?.attributes?.header?.media?.data?.attributes?.url ?
                  <Image
                    source={BannerData?.marketing_course?.data?.attributes?.header?.media?.data?.attributes?.url}
                    style={{
                      height: "35%",
                      width: "100%",
                      justifyContent: "center",
                      resizeMode: "cover",
                    }}
                    resizeMode={"cover"}
                    alt="BannerImage"
                  /> : null}
              </Hidden>
              <HStack alignItems="center" justifyContent="space-between" >
                <VStack
                  space={4}
                  height="500px"
                  px={{ lg: "110px", base: "13px", md: "40px" }}
                  w="100%"
                  justifyContent={"center"}

                >
                  <VStack h="400" mt={"30px"}
                    space={3}
                  >
                    <VStack flex={1}
                      space={4}
                    >
                      <VStack flex={0.3} justifyContent={"center"}>
                        <Text
                          width={{ base: "100%", md: "55%" }}
                          fontSize={{
                            base: "10px",
                            md: "20px",
                          }}
                          color="#ffffff"
                          lineHeight={{ base: "30", md: "39" }}
                          fontWeight={700}
                          bold
                          mb={{ md: "30px" }}
                        >
                          <h1 style={{ fontFamily: "Nunito Sans" }}>
                            {content2?.shortName}

                          </h1>
                        </Text>
                      </VStack>
                      <HStack
                        pt={{ base: 10, md: "16px" }}
                        flex={0.3}
                        alignItems={"center"}
                      >
                        <Image
                          resizeMode="contain"
                          width={'24px'}
                          height={'24px'}
                          source={{ uri: 'https://images.netskill.com/frontend/jobPlacement.webp' }}
                        />
                        <Text
                          width="xl"
                          fontFamily="Nunito Sans"
                          lineHeight={"48"}
                          fontSize={{
                            base: "13px",
                            md: "16px",
                          }}
                          color="#ffffff"
                          fontWeight="600"
                          letterSpacing="0.2"
                        >
                          {`   ${get(BannerData, 'marketing_course.data.attributes.course_content[0].title', '')}`}
                        </Text>
                      </HStack>
                      {/* <VStack pt={{ base: 4, md: 0 }} flex={0.1} /> */}
                      <Text
                        pt={"5px"}
                        flex={0.2}
                        fontSize={{
                          base: "13px",
                          md: "16px",
                        }}
                        fontFamily="Nunito Sans"
                        color="#B0B5C0"
                        fontWeight="normal"
                        letterSpacing="0.2"
                        width={{
                          base: "100%",
                          lg: "54%",
                          md: "100%",
                        }}
                        maxW={["100%", "100%", "80%"]}
                      >
                        {BannerData?.marketing_course?.data?.attributes?.header?.subDescription}
                      </Text>
                    </VStack>
                    <HStack flexWrap={"wrap"} pt={{ base: "35px", md: "15px" }} space={5}>
                      <HStack
                        justifyContent={"space-between"}
                        bg="#1D1E21"
                        h="30"
                        px={4}
                        alignItems={'center'}
                      >
                        <Text
                          fontFamily={'Nunito Sans'}
                          fontSize={"14px"}
                          fontWeight={"600"}
                          color={"#B0B5C0"}
                        >
                          {'Duration : '}
                        </Text>
                        <Text
                          fontSize={"16px"}
                          fontWeight={"600"}
                          color={"white"}
                          fontFamily={'Nunito Sans'}
                        >
                          {priceArray?.[0]?.duration[0]?.duration}{' '}{priceArray?.[0]?.duration[0]?.unit}
                        </Text>
                      </HStack>
                      <HStack
                        justifyContent={"space-between"}
                        bg="#1D1E21"
                        h="30"
                        px={4}
                        alignItems={'center'}
                      >
                        <Text
                          fontFamily="Nunito Sans"
                          fontSize={"14px"}
                          fontWeight={"600"}
                          color={"#B0B5C0"}
                        >
                          {'Next Batch : '}
                        </Text>
                        <Text
                          fontSize={"16px"}
                          fontWeight={"600"}
                          color={"white"}
                          fontFamily="Nunito Sans"
                        >
                          {moment(getCalclulatedDate(priceArray?.[0]?.batch?.startDateTime)).format('DD MMM, YYYY')}
                          {/* {moment(priceArray?.[0]?.batch?.startDateTime).format('DD MMM, YYYY')} */}
                        </Text>
                      </HStack>
                    </HStack>


                    <VStack
                      // pt={"19px"}
                      // justifyContent={"center"}
                      bg="#1D1E21"
                      // h="203"
                      // w="491"
                      width={{ base: "100%", md: "491px" }}
                      px={4}
                      flexWrap={"wrap"}
                      borderRadius={'4px'}
                    >
                      <Text
                        fontSize={"14px"}
                        fontWeight={"bold"}
                        color={"#B0B5C0"}
                        lineHeight={'19px'}
                        py={3}
                        mb={2}
                        fontFamily="Nunito Sans"
                      >
                        Program Fee
                      </Text>
                      <TouchableOpacity disabled={priceArray?.[0]?.program_fees_structure?.[0]?.coming_soon || bookSlotArray?.length} onPress={basicPlan}>
                        <HStack justifyContent={'space-between'}>
                          <HStack alignItems={'center'}  >
                            {selectedPlan1 ?
                              <Image
                                marginRight={3}
                                width={{
                                  md: "24px",
                                  base: 5,
                                }}
                                height={{
                                  md: "24px",
                                  base: 5,
                                }}
                                resizeMode="contain"
                                source={{ uri: 'https://images.netskill.com/frontend/Redselected.svg' }}
                                // alignSelf={"self-start"}
                                alt="details_image"
                                pl={0}
                              /> :
                              <>
                                <Image
                                  opacity={priceArray?.[0]?.program_fees_structure?.[0]?.coming_soon || bookSlotArray?.length ? 0.3 : 1}
                                  marginRight={3}
                                  width={{
                                    md: "24px",
                                    base: 5,
                                  }}
                                  height={{
                                    md: "24px",
                                    base: 5,
                                  }}
                                  resizeMode="contain"
                                  source={{ uri: "https://images.netskill.com/frontend/netskill/home/circle.svg" }}
                                  textAlign="start"
                                  // alignSelf={"self-start"}
                                  alt="details_image"
                                  pl={0}
                                />
                              </>
                            }

                            <Text
                              color={priceArray?.[0]?.program_fees_structure?.[0]?.coming_soon || bookSlotArray?.length ? "coolGray.500" : '#fff'}
                              fontSize="16px"
                              fontWeight={'bold'}
                              fontFamily="Nunito Sans"
                            >
                              {priceArray?.[0]?.program_fees_structure[0]?.title}
                            </Text>
                          </HStack>
                          {priceArray?.[0]?.program_fees_structure?.[0]?.coming_soon ?
                            // <VStack width="45%" alignSelf={'flex-end'} mt={-2}>
                            <LinearGradient
                              colors={['#424345', '#424345', 'rgba(196, 196, 196, 0.006)']}
                              style={{ width: 150 }}
                              location={[1, 55.4, 1]}
                              start={[0, 0]} end={[1, 0]}
                            >
                              <Text
                                fontFamily="Nunito Sans"
                                py={"5px"}
                                px={"15px"}
                                color={'#fff'}
                                fontSize="14px"
                                fontWeight={'600'}
                                style={{ textDecorationColor: "#ff5e63" }}
                              >
                                Coming Soon
                              </Text>
                            </LinearGradient>
                            // </VStack> 
                            :
                            <HStack opacity={priceArray?.[0]?.program_fees_structure?.[0]?.coming_soon || bookSlotArray?.length ? 0.3 : 1} justifyContent={'center'} alignItems={'center'}>
                              <Text
                                fontFamily="Nunito Sans"
                                fontSize={"16px"}
                                pt={1}
                                fontWeight={'800px'}
                                color={"#fff"}
                              >
                                {priceArray?.[0]?.program_fees_structure[0]?.currency}{' '}{priceArray?.[0]?.program_fees_structure[0]?.discountPrice}{"  "}
                                <Text
                                  fontSize={"12px"}
                                  color={"#B0B5C0"}
                                  textDecorationLine="line-through"
                                  style={{ textDecorationColor: "#ff5e63" }}
                                  fontFamily="Nunito Sans"
                                >
                                  {priceArray?.[0]?.program_fees_structure[0]?.currency}{' '}{priceArray?.[0]?.program_fees_structure[0]?.actualPrice}
                                </Text>
                              </Text>
                            </HStack>}
                        </HStack>
                      </TouchableOpacity>

                      <Image
                        borderTopRadius="lg"
                        width={{
                          md: "100%",
                          base: '100%',
                        }}
                        height={{
                          md: "20px",
                          base: 4,
                        }}
                        resizeMode="contain"
                        source={{ uri: 'https://images.netskill.com/frontend/planfeeLine.webp' }}
                        alt="divider"
                        mt={1}
                      />

                      <TouchableOpacity onPress={premiumPlan}>
                        <HStack pb={2} justifyContent={'space-between'}>
                          <HStack alignItems={'center'}>
                            {!selectedPlan1 ?
                              <Image
                                marginRight={3}
                                width={{
                                  md: "24px",
                                  base: 5,
                                }}
                                height={{
                                  md: "24px",
                                  base: 5,
                                }}
                                resizeMode="contain"
                                source={{ uri: 'https://images.netskill.com/frontend/Redselected.svg' }}
                                alt="details_image"
                                pl={0}
                              />
                              :
                              <>
                                <Image
                                  marginRight={3}
                                  width={{
                                    md: "24px",
                                    base: 5,
                                  }}
                                  height={{
                                    md: "24px",
                                    base: 5,
                                  }}
                                  resizeMode="contain"
                                  source={{ uri: "https://images.netskill.com/frontend/netskill/home/circle.svg" }}
                                  alt="details_image"
                                  pl={0}
                                />
                              </>
                            }

                            <Text
                              color={'#FFFFFF'}
                              fontSize="16px"
                              fontWeight={'bold'}
                              lineHeight={'21.82px'}
                              fontFamily="Nunito Sans"
                            //fontFamily={'body'}
                            >
                              {priceArray?.[0]?.program_fees_structure[1]?.title}
                            </Text>
                          </HStack>
                          <HStack justifyContent={'center'} alignItems={'center'}>
                            <Text
                              fontFamily="Nunito Sans"
                              fontSize={"16px"}
                              pt={1}
                              fontWeight={"800"}
                              color={"white"}
                            >
                              {priceArray?.[0]?.program_fees_structure[0]?.currency}{' '}{priceArray?.[0]?.program_fees_structure[1]?.discountPrice}{"  "}
                              <Text
                                fontFamily="Nunito Sans"
                                fontSize={"12px"}
                                color={"#B0B5C0"}
                                textDecorationLine="line-through"
                                style={{ textDecorationColor: "#ff5e63" }}
                              >
                                {priceArray?.[0]?.program_fees_structure[0]?.currency}{' '}{priceArray?.[0]?.program_fees_structure[1]?.actualPrice}
                              </Text>
                            </Text>
                          </HStack>
                        </HStack>
                      </TouchableOpacity>
                      {showButtons("banner")}s
                    </VStack>
                    {/* </VStack> */}

                  </VStack>
                </VStack>
              </HStack>
            </VStack>
          ) : null}
          {!showStickyHeader ? (
            <HStack px={{ base: "20px", md: "100" }} h={inwdith < 700 ? "100" : "78"} bg="#1D1E21">
              <ScrollView
                showsHorizontalScrollIndicator={false}
                contentContainerStyle={{
                  flexGrow: 1,
                  justifyContent: "space-around",
                }}
                horizontal
              >
                {staticList.map((item, idx) => {
                  return (
                    <VStack
                      px={[10, 10, undefined]}
                      justifyContent={"center"}
                      alignItems="center"
                    >
                      <Image source={{ uri: item?.icon }} w={7} h={7} alt="iconImage" />
                      <Text
                        mt={2}
                        fontWeight="600"
                        fontSize={"14px"}
                        color={"white"}
                        fontFamily="Nunito Sans"
                      >
                        {item?.title}
                      </Text>
                    </VStack>
                  );
                })}
              </ScrollView>
            </HStack>
          ) : null}
          {!showStickyHeader ? (
            <VStack
              onLayout={(e) => {
                if (count == 0) {
                  setHeaderCoords(e.nativeEvent.layout.y);
                  setCount(1);
                }
              }}
              px={{ base: "20px", md: "110" }}
            >
              <ScrollView
                showsHorizontalScrollIndicator={false}
                horizontal={{
                  base: true,
                  md: true,
                }}
                contentContainerStyle={{
                  flexGrow: 1,
                  justifyContent: "flex-start",
                }}
              >
                <HStack
                  // justifyContent={"flex-start"}
                  alignItems={"center"}
                  h={["14%", "14%", 40]}
                  mt={{ base: 6, md: 0 }}
                  w={"100%"}
                >
                  {tabList?.map((item, id) => {
                    return (
                      <Pressable
                        justifyContent="flex-start"
                        mr={"40px"}
                        onPress={() => handleChange(item?.id)}
                      >
                        <Box
                          pt={{ base: 5, md: 0 }}
                          justifyContent={"space-around"}
                          alignItems={"center"}
                          borderBottomWidth={3}
                          borderColor={
                            selectedTab === item?.id ? "#FF5E63" : "transparent"
                          }
                          h={62}
                        >
                          <Text
                            fontFamily="Nunito Sans"
                            fontSize={{ base: "17px", md: "20px" }}
                            fontWeight={
                              selectedTab === item?.id ? "700" : "600"
                            }
                            color={
                              selectedTab === item?.id ? "#FF5E63" : "#17181A"
                            }
                          >
                            {item?.title}
                          </Text>
                        </Box>
                      </Pressable>
                    );
                  })}
                </HStack>
              </ScrollView>
            </VStack>
          ) : null}
          {showStickyHeader ? (
            <Box w={"100%"} h={{ base: 160, md: 200 }}></Box>
          ) : null}
          <VStack
            onLayout={(e) => {
              setOverview(e.nativeEvent.layout.y);
            }}
            px={[0, 0, "110px"]}
          >
            <Overview
              overData={overviewData}
            />
          </VStack>
          <VStack
            onLayout={(e) => {
              setTutor(e.nativeEvent.layout.y);
            }}
            px={[0, 0, "110px"]}
            py={10}
          >
            <TutorsMentor
              title={get(mentoreListData, 'header.title', '')}
              subtitle={get(mentoreListData, 'header.description', '')}
              data={tutorData}
              isCoursepreview={true}
              data1={mentoreListData}
              bottomSpace={true}
            />

            <ProgramStructure
              topmargin={"80px"}
              dataId={programStructureData?.accordins[0]?.id}
              data={programStructureData?.accordins}

              shapeImage={require("../../assets/images/oval.svg")}
              mainImage={"https://images.netskill.com/frontend/netskill/courseDetail/laptop.svg"}
              title={get(programStructureData, 'header.title', '')}
              subtitle={get(programStructureData, 'header.description', '')}
              bottomActionData={bottomActionData}

            />
            <Box w={{ base: "100%", md: "35%" }} flexDirection={'row'} alignItems="center" justifyContent={"center"} height="50px" >

              <Button
                width={["280px", "280px"]}
                size="md"
                bg="#ff5e63"
                variant="raised"
                _hover={{
                  bg: "error.500",
                }}
                onPress={() => setShowDetailPopup(true)}
              >
                <Image width={{
                  md: "20px",
                  base: "20px",
                }}
                  height={{
                    md: "20px",
                    base: "20px",
                  }}
                  source={{ uri: 'https://images.netskill.com/frontend/downIcon.png' }}
                  alt="download"
                  resizeMode="contain"
                  textAlign={"start"}
                  position={'absolute'}
                  zIndex={'5000'} left={['-15px', '-15px']}
                />
                <Text ml={5} fontFamily="Nunito Sans" fontWeight={"600"} fontSize={["14px", "14px"]}>
                  {'Download Detailed Curriculum'}
                </Text>

              </Button>
            </Box>
          </VStack>
          <VStack
            onLayout={(e) => {
              setCourse(e.nativeEvent.layout.y);
            }}
            px={0}
          >
            <CourseCurriculum
              props={props}
              clmamData={curriculamData}
              featuresData={featuresData}
              bottomActionData={bottomActionData}
              donwloadBtn={setShowDetailPopup}
              router={router}
            />
          </VStack>
          <VStack
            onLayout={(e) => {
              setHiring(e.nativeEvent.layout.y);
            }}
            pl={[2, 2, "110px"]}
          >
            <Hiring
              title={props ? 'Companies hiring for this course' : null}
              subHeading={'100+ Top Companies hire ‘job ready’ engineers with cutting-edge skills from Netskill'}
              companyImgSize={"220px"}
              showMembers={true}
              partners={curriculamData ? partners : null}
              features={curriculamData ? featurecoloumnData : null}
              courseListHire={courseListHire}
            />
          </VStack>
          <VStack
            onLayout={(e) => {
              setFee(e.nativeEvent.layout.y);
            }}
            px={0}
          >
            <Fee
              donwloadBtn={setShowDetailPopup}
              data={planFeeData?.plan_fees?.data[0]?.attributes?.plan_structure}
              title={'Plans & Fee  Structure'}
              bookSlotArray={bookSlotArray}
              selectedPlan={selectedPlan1}
              setSelectedPlan={setSelectedPlan1}
              isHomeScreen={true}
              bookSlot={bookSlot}
              props={props}
              setShowSlot={setShowSlotPopup}
              slug={slug}
              finalTitle={content?.attributes?.shortName} offerdetailsData={offerdetailsData} testimonilaData={testimonilaData} certificateData={certificateData} />
          </VStack>
          <VStack
            onLayout={(e) => {
              setFaq(e.nativeEvent.layout.y);
            }}
            px={[0, 0, "110px"]}
          >
            <Faq
              title={faqData?.title}
              Isfaq={faqData?.faq}
              stepsData={stepsData}
              setShowSlot={setShowSlotPopup}
              setModalHeader={setModalHeader}
              bookSlot={bookSlot}
              bookSlotArray={bookSlotArray}
              slug={props?.route?.params?.slug}
              nav={props?.navigation}
              goToUnitOverView={goToUnitOverView}
            />
          </VStack>

          <Modal
            // animationType={"fade"}
            useNativeDriver={true}
            transparent={true}
            backdropOpacity={0.1}
            visible={showSlotPopup}
            onBackdropPress={closeModal}
          >
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
                <VStack height={{ md: 400 }} width={{ md: 580 }}>
                  <HStack justifyContent={"flex-end"}>
                    <TouchableOpacity onPress={() => closeModal()}>
                      <Image
                        width={{
                          md: "25px",
                          base: 25,
                        }}
                        height={{
                          md: "25px",
                          base: 20,
                        }}
                        source={{ uri: 'https://images.netskill.com/frontend/close.svg' }}
                        alt="close"
                        resizeMode="contain"
                        textAlign={"end"}
                      />
                    </TouchableOpacity>
                  </HStack>
                  <Text
                    color="#17181A"
                    fontSize={"22px"}
                    fontWeight="500"
                    mb="32px"
                    fontFamily={'Nunito Sans'}
                  >
                    {modalHeader ? modalHeader : "Book your slot today!"}
                  </Text>
                  <VStack space={{ md: 2 }}>
                    <VStack mb={"18px"}>
                      <FloatingLabelInput
                        value={fullName}
                        label={"Full Name"}
                        borderRadius={4}
                        color="#17181a"
                        labelColor="#17181a"
                        labelBGColor={useColorModeValue("#fff", "#fff")}
                        defaultValue={value}
                        onChangeText={(txt) => {
                          setFullName(txt);
                          //  if(errEmail) setErrEmail(null);
                        }}
                        hintColor="#17181A"
                        hintTextColor="#17181A"
                        style={{
                          fontFamily: "Nunito Sans",
                          fontWeight: value ? "600" : "400",
                          color: "#17181a",
                          fontSize: value ? "14px" : "12px",
                          paddingLeft: 17,
                          paddingVertical: 12,
                          border: "0px solid #DDDDDD",
                        }}
                        InputRightElement={null}
                      />
                    </VStack>
                    <VStack mb={"18px"}>
                      <FloatingLabelInput
                        value={email}
                        label={"Email Address"}
                        borderRadius={4}
                        color="#17181a"
                        labelColor="#17181a"
                        labelBGColor={useColorModeValue("#fff", "#fff")}
                        defaultValue={value}
                        onChangeText={(txt) => {
                          setEmail(txt);
                          if (errEmail) setErrEmail(null);
                        }}
                        hintColor="#17181A"
                        hintTextColor="#17181A"
                        style={{
                          fontFamily: "Nunito Sans",
                          fontWeight: value ? "600" : "400",
                          color: "#17181a",
                          fontSize: value ? "14px" : "12px",
                          paddingLeft: 17,
                          paddingVertical: 12,
                          border: "0px solid #DDDDDD",
                        }}
                        InputRightElement={null}
                      />
                    </VStack>
                    {/* <CustomTextField
                      value={fullName}
                      setValue={setFullName}
                      label="Full Name"
                    />
                    <CustomTextField
                      value={email}
                      setValue={setEmail}
                      label="Email Address"
                      setErrEmail={setErrEmail}
                    /> */}
                    {errEmail ? (
                      <Text
                        mt={-3}
                        textAlign={"end"}
                        fontSize={"13px"}
                        color={"error.500"}
                        fontFamily={'Nunito Sans'}
                      >
                        {errEmail}
                      </Text>
                    ) : null}

                    <HStack
                      flexDirection={["column", "row"]}
                      justifyContent={["flex-start", "space-between"]}
                      // space="2"
                      alignItems="center"
                    >
                      <Box
                        maxW={["100%", "47%"]}
                        width={["100%", "47%"]}
                        borderColor={"#17181a"}
                      >
                        <CustomDropdown
                          value={selectedYear}
                          setValue={setSelectedYear}
                          options={yearList}
                          label="Graduation Year"
                        />
                      </Box>
                      <Box w={{ base: '100%', md: '47%' }} >
                        <PhoneInput
                          inputStyle={{
                            width: '100%',
                            height: 45,
                            fontSize: '13px',
                            paddingLeft: '48px',
                            borderRadius: '5px'
                          }}
                          buttonStyle={{ backgroundColor: 'white' }}
                          dropdownStyle={{ width: 250, position: "absolute", bottom: 35 }}
                          country={'in'}
                          value={phone}
                          onChange={(e, code) => { setPhone(e), setPhoneCode(code?.dialCode), setErr(null); }}
                        />
                      </Box>
                    </HStack>
                    {err ? (
                      <Text
                        textAlign={"end"}
                        mt={-3}
                        fontSize={"13px"}
                        color={"error.500"}
                      >
                        {err}
                      </Text>
                    ) : null}
                    <Button
                      variant="raised"
                      mt={{ md: 10, base: 5 }}
                      size="md"
                      borderRadius="4"
                      _text={{
                        fontWeight: "medium",
                      }}
                      _light={{
                        bg: "primary.900",
                      }}
                      _dark={{
                        bg: "primary.700",
                      }}
                      onPress={() => {
                        handleSubmit();
                      }}
                      isDisabled={
                        !fullName ||
                        !email ||
                        !selectedYear
                      }
                      _hover={{
                        bg: "error.500",
                      }}
                      py="3"
                    >
                      Submit
                    </Button>
                  </VStack>
                </VStack>
              </View>
            </View>
          </Modal>
          <Modal
            // animationType={"fade"}
            useNativeDriver={true}
            transparent={true}
            backdropOpacity={0.1}
            visible={showDetailPopup}
          >
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
                <VStack height={{ md: 380 }} width={{ md: 580 }}>
                  <HStack justifyContent={"flex-end"}>
                    <TouchableOpacity onPress={closeDetailModal}>
                      <Image
                        width={{
                          md: "25px",
                          base: 25,
                        }}
                        height={{
                          md: "25px",
                          base: 20,
                        }}
                        source={{ uri: 'https://images.netskill.com/frontend/close.svg' }}
                        alt="close"
                        resizeMode="contain"
                        textAlign={"end"}
                      />
                    </TouchableOpacity>
                  </HStack>
                  <Text
                    color="#17181A"
                    fontSize={"22px"}
                    fontWeight="500"
                    mb="32px"
                    fontFamily={'Nunito Sans'}
                  >
                    {'Download Detailed Curriculum'}
                  </Text>
                  <VStack space={{ md: 2 }}>
                    <CustomTextField
                      value={fullName}
                      setValue={setFullName}
                      label="Full Name"
                    />
                    <CustomTextField
                      value={email}
                      setValue={setEmail}
                      label="Email Address"
                      setErrEmail={setErrEmail}
                    />
                    {errEmail ? (
                      <Text
                        mt={-3}
                        textAlign={"end"}
                        fontSize={"13px"}
                        color={"error.500"}
                      >
                        {errEmail}
                      </Text>
                    ) : null}


                    <View w={["100%", "100%"]}>
                      <PhoneInput
                        inputStyle={{
                          width: '100%',
                          height: 45,
                          fontSize: '13px',
                          paddingLeft: '48px',
                          borderRadius: '5px',
                        }}
                        buttonStyle={{ backgroundColor: 'white' }}
                        dropdownStyle={{ width: 250, position: "absolute", bottom: 35 }}
                        country={'in'}
                        value={phone}
                        onChange={(e, code) => { setPhone(e), setPhoneCode(code?.dialCode), setErr(null); }}
                      />
                    </View>

                    {err ? (
                      <Text
                        textAlign={"end"}
                        mt={-3}
                        fontSize={"13px"}
                        color={"error.500"}
                      >
                        {err}
                      </Text>
                    ) : null}
                    <Box py={{ md: 16, base: 10 }} flexDirection={'row'} alignItems="center" justifyContent={['center', 'center']} size="s" height="50px">
                      <Image width={{
                        md: "25px",
                        base: 25,
                      }}
                        height={{
                          md: "25px",
                          base: 20,
                        }}
                        source={require("../../assets/images/downIcon.png")}
                        alt="download"
                        resizeMode="contain"
                        textAlign={"start"}
                        position={'absolute'}
                        zIndex={'5000'} left={['90px', '200px']}
                      />
                      <Button
                        width={["250px", "100%"]}
                        size="md"
                        variant="raised"
                        _hover={{
                          bg: "error.500",
                        }}
                        borderRadius="4"
                        _text={{
                          fontWeight: "medium",
                        }}
                        _light={{
                          bg: "primary.900",
                        }}
                        _dark={{
                          bg: "primary.700",
                        }}
                        onPress={() => {
                          handleDownload();
                        }}
                        isDisabled={
                          !fullName ||
                          !email ||
                          !phone
                        }
                        country={'in'}
                        countryCodeEditable={false}
                      >
                        Download
                      </Button>
                    </Box>
                  </VStack>
                </VStack>
              </View>
            </View>
          </Modal>

          <Footer />
        </DashboardLayout>
      </Animated.ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: 'rgba(0,0, 0, 0.5)',
  },
  modalView: {
    // margin: 10,
    backgroundColor: "white",
    borderRadius: 10,
    padding: 30,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },

});
