import * as React from 'react';
import { HStack, Pressable, Text, Box, ScrollView} from 'native-base';
const tabList = [
    {
        title: "Overview",
        id: 1,
    },
    {
        title: "Tutors and Mentors",
        id: 2,
    },
    {
        title: "Course Curriculum",
        id: 3,
    },
    {
        title: "Hiring Companies",
        id: 4,
    },
    {
        title: "Fee Structure",
        id: 5,
    },
    {
        title: "FAQs",
        id: 6,
    },
];
export default function TabComponent(props) {
  return (
    <ScrollView
    horizontal={{
      base: true,
      md: true,
    }}
    contentContainerStyle={{ flexGrow: 1}}
    >
    <HStack justifyContent={"space-around"} alignItems={"center"} h={["10%","10%",40]} mt={5} w={{base: "100%", md: "83%"}}>
    {tabList?.map((item, id) => {
        return (
            <Pressable mx={6} onPress={() => props?.setSelectedTab(item?.id)}>
                <Box justifyContent={"space-around"} alignItems={"center"} borderBottomWidth={3} borderColor={props?.selectedTab === item?.id ? "#FF5E63" : "transparent"} h={62}>
                    <Text fontSize={{base: "16px", md: "17px"}} fontWeight={"medium"} color={props?.selectedTab === item?.id ? "#FF5E63" : "#17181A"}>{item?.title}</Text>
                </Box>
            </Pressable>
        )
    })}
</HStack>
</ScrollView>
  )}
