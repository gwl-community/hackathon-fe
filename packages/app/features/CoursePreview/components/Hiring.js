import * as React from 'react';
import { FlatList, Image, Text, Box, ScrollView } from 'native-base';

export default function Hiring({subHeading, title, companyImgSize,showMembers, partners, courseListHire}) {

    const renderItem = ({ item }) => {
        return (
            <Box justifyContent={"flex-start"} pt={3}>
                <Image
                    width={{
                        md: companyImgSize,
                        base: "150px",
                    }}
                    height={{
                        md: "70px",
                        base: "50px",
                    }}
                    source={item}
                    alt="company"
                    resizeMode="contain"
                />
            </Box>
        );
    };

    return (
        <Box pb={5} justifyContent={"space-around"} >
            <Box>
            <Text fontFamily="Nunito Sans" pb={4} fontSize={{
                base: "23px",
                md: "24px",
            }} fontWeight={"bold"} color={"#000000"}>{"Companies hiring for this course"}</Text>
            <Text fontFamily="Nunito Sans" fontSize={{
                base: "15px",
                md: "16px",
            }} mt={2} fontWeight={"normal"} opacity={0.6} color={"#17181A"}>{subHeading}</Text>
            <Box pt={7} pb={10} w={"100%"}>
                <ScrollView contentContainerStyle={{ justifyContent: "space-around" }} showsHorizontalScrollIndicator={false} horizontal={{
                    base: true,
                    md: true,
                }}>
                    <FlatList
                        style={{ width: "100%" }}
                        scrollEnabled
                        data={partners}
                        numColumns={5}
                        renderItem={renderItem}
                        keyExtractor={(item) => item.id}
                    />
                </ScrollView>
            </Box>
            </Box>
            {showMembers ? 
            <>
            <Box pr={[2, 2, "108px"]}>
            <Text fontFamily="Nunito Sans" py={5} fontSize={{
                base: "22px",
                md: "24px",
            }} fontWeight={"bold"} color={"#000000"}>Who should take this course?</Text>
              </Box>
            <Box pr={[2, 2, "108px"]}>
            <ScrollView showsHorizontalScrollIndicator={false}
                contentContainerStyle={{ flexGrow: 1, justifyContent: "flex-start" }}
                horizontal={true}>
                {/* <HStack mx={3} w={{base: undefined ,md:"80%"}} justifyContent="center" pb={10}> */}
                {courseListHire?.map((item) => {
                    return (
                        <Box borderRadius={"4px"} w={'210px'} mr={"10px"} py={4} pb={"76px"} px={3}  bg={"#f4f4f4"}>
                            <Image source={item?.img} w={"48px"} h={"48px"} alt="HireImage"/>
                            <Text fontFamily="Nunito Sans" pt={5} w={"100%"} fontSize={"16px"} color={"#000000"} fontWeight={"bold"}>{item?.title}</Text>
                            <Text fontFamily="Nunito Sans" py={5} fontSize={"14px"} color={"#000000"}>{item?.subTitle}</Text>
                        </Box>
                    )
                })}
            </ScrollView>
            </Box>
            </> :null}
        </Box>
    )
}
