import * as React from 'react';
import { Pressable, Image, Text, Box, HStack, Button, ScrollView, VStack } from 'native-base';
import { Link, TextLink } from 'solito/link'
//import { MaterialCommunityIcons, MaterialIcons } from "@expo/vector-icons";
import get from "lodash.get";
// import { Link } from '@react-navigation/native';

export default function CourseCurriculum({ props, clmamData, featuresData, bottomActionData, donwloadBtn, router }) {

    const CuricullamData = get(clmamData, 'header.marketing_course_detail.data.attributes', '')
    const [selected, setSelected] = React.useState(0);

    return (
        <Box py={10} justifyContent={"space-around"} >
            <Text fontFamily="Nunito Sans" px={{ base: 3, md: "110px" }} pb={3} fontSize={{ base: "20px", md: "24px" }} fontWeight={"bold"} color={"#000000"}>Course curriculum</Text>
            <ScrollView
                horizontal={{
                    base: true,
                    md: true,
                }}>
                <HStack py={4} px={{ base: 3, md: "110px" }} >
                    {CuricullamData?.skills?.map((item, i) => {
                        return (
                            <Box mr={"40px"} alignItems={"center"}>
                                <Image source={item?.media?.data?.attributes?.url} w={12} h={12} alt="CourseImage" />
                                <Text fontFamily="Nunito Sans" fontSize={{ base: "13px", md: "14px" }} pt={3} color={'#17181a'}>{item?.title}</Text>
                            </Box>
                        )
                    })}
                </HStack>
            </ScrollView>
            <HStack flexDirection={{ base: "column", md: "row" }} pb={{ base: 3, md: 10 }} px={{ base: 3, md: "110px" }} mt={9}>
                <Box bg={"white"} flex={[undefined, undefined, 0.65]}>
                    {CuricullamData?.units?.map((item, i) => {
                        return (
                            <>
                                {selected === i ?
                                    <Pressable onPress={() => setSelected(-1)}>
                                        <Box h={"72px"} justifyContent={"center"} px={4} bg={"white"} borderColor={"rgba(176, 181, 192, 0.5)"} borderWidth={1}>
                                            <HStack alignItems={"center"}>
                                                <Text fontFamily="Nunito Sans" w={{ base: "12%", md: "7%" }} fontSize={{
                                                    base: "12px",
                                                    md: "16px",
                                                }} color={'#17181a'}>Unit {i + 1}</Text>
                                                <Text fontFamily="Nunito Sans" ml={["2px", "24px"]} w={{ base: "65%", md: "70%" }} fontSize={{
                                                    base: "9px",
                                                    md: "13.5px",
                                                }} color={'#17181a'} fontWeight={"600"}><h2 style={{ marginBottom: 'auto', marginTop: 'auto' }}>{item?.header?.title}</h2></Text>
                                                <HStack flexDirection={["column", "row"]} justifyContent={["center", "flex-start"]} alignItems={"center"} w={{ base: "15%", md: "15%" }} >
                                                    <Image source={"https://images.netskill.com/frontend/netskill/home/timeIcon.webp"} h={["12px", "20px"]} w={["12px", "20px"]} alt="timeIconImage" />
                                                    <Text fontFamily="Nunito Sans" ml={[0, 2]} fontSize={["10px", "16px"]} fontWeight={"700"} color={'#595fff'}>
                                                        {item?.header?.duration}</Text>
                                                </HStack>
                                                <Image
                                                 width={{ md: 5, base: 5 }}
                                                 height={{ md: 5, base: 5 }}
                                                 resizeMode="contain"
                                                 source={{ uri: 'https://images.netskill.com/frontend/darkdonw.webp' }} alt={"image"}/>
                                            </HStack>
                                        </Box>

                                        <Box p={4} mb={4} bg={"white"} borderColor={"rgba(176, 181, 192, 0.5)"} borderWidth={1} borderTopWidth={0}>
                                            {item?.unitDetails?.map((item, i) => {
                                                return (
                                                    <HStack py={2}>
                                                        <Box w={"7%"} alignItems={"center"} pt={1}>
                                                            <Box w={"32px"} h={"32px"} justifyContent="center" alignItems={"center"} bg={"rgba(255, 94, 99, 0.1)"} borderRadius={"100"}>
                                                                <Text fontFamily="Nunito Sans" fontSize={["14px", "20px"]} fontWeight={"700"} color={"#ff5e63"}>{i + 1}</Text>
                                                            </Box>
                                                        </Box>
                                                        <Box ml={"24px"} w={"91%"} >
                                                            <Text
                                                                mb={{ md: '2px', base: '2px' }}
                                                                fontFamily="Nunito Sans"
                                                                fontSize={{
                                                                    base: "11px",
                                                                    md: "16px",
                                                                }} fontWeight={"700"}
                                                                pt={{ base: '4px', md: '4px' }}

                                                                color={'#17181a'}
                                                            ><h2 style={{ marginBottom: 'auto', marginTop: 'auto' }}>{item?.title}</h2></Text>
                                                            {/* <Text pt={1} fontWeight={"bold"} fontSize={"12px"} color={'#19bf79'}>{item?.extraText}</Text> */}
                                                            <HStack >
                                                                <Text fontFamily="Nunito Sans" pt={1} fontWeight={"bold"} fontSize={"12px"} color={'#19bf79'}>{item?.duration}</Text>
                                                                <Text mx={"4px"} mt={-3} fontSize={24} color={'#19bf79'} fontWeight={"bold"}>{"."}</Text>
                                                                <Text fontFamily="Nunito Sans" pt={1} fontWeight={"bold"} fontSize={"12px"} color={'#19bf79'}>{item?.project}</Text>
                                                                {item?.study ? <Text mx={"4px"} mt={-3} fontSize={24} color={'#19bf79'} fontWeight={"bold"}>{"."}</Text>
                                                                    : null}<Text fontFamily="Nunito Sans" pt={1} fontWeight={"bold"} fontSize={"12px"} color={'#19bf79'}>{item?.study}</Text>
                                                            </HStack>
                                                            <Text fontFamily="Nunito Sans" pt={1} fontSize={{
                                                                base: "12px",
                                                                md: "14px",
                                                            }} color={'rgba(23, 24, 26, 0.8)'}>{item?.subTitle}</Text>
                                                        </Box>
                                                    </HStack>
                                                )
                                            })}
                                        </Box>
                                    </Pressable>
                                    :
                                    <Pressable onPress={() => setSelected(i)}>
                                        <Box h={"72px"} borderRadius={"4px"} justifyContent={"center"} px={4} mb={4} bg={"white"} borderColor={"rgba(176, 181, 192, 0.5)"} borderWidth={1}>
                                            <HStack alignItems={"center"}>
                                                <Text fontFamily="Nunito Sans" w={{ base: "12%", md: "7%" }} fontSize={{
                                                    base: "12px",
                                                    md: "16px",
                                                }} color={'#17181a'}>Unit {i + 1}</Text>
                                                <Text fontFamily="Nunito Sans" w={{ base: "65%", md: "70%" }} fontSize={{
                                                    base: "9px",
                                                    md: "13.5px",
                                                }} color={'#17181a'} ml={["2px", "24px"]} fontWeight={"600"}><h2 style={{ marginBottom: 'auto', marginTop: 'auto' }}>{item?.header?.title}</h2></Text>
                                                <HStack flexDirection={["column", "row"]} justifyContent={["center", "flex-start"]} alignItems={"center"} w={{ base: "15%", md: "15%" }}>
                                                    <Image source={"https://images.netskill.com/frontend/netskill/home/timeIcon.webp"} h={["12px", "20px"]} w={["12px", "20px"]} alt="timeIconImage" />
                                                    <Text fontFamily="Nunito Sans" ml={[0, 2]} fontSize={["10px", "16px"]} fontWeight={"700"} color={'#595fff'}>
                                                        {item?.header?.duration}</Text>
                                                </HStack>
                                                <Image
                                                  width={{ md: 5, base: 5 }}
                                                  height={{ md: 5, base: 5 }}
                                                  resizeMode="contain" source={{ uri: 'https://images.netskill.com/frontend/darkup.webp' }} alt={"image"} />
                                            </HStack>
                                        </Box>
                                    </Pressable>
                                }
                            </>
                        )
                    })}
                </Box>
                <Box bg={'white'} ml={{ base: 1, md: 5 }} flex={0.3}>
                    <Box borderRadius={"4px"} p="4" borderWidth={1} borderColor={'rgba(176, 181, 192, 0.5)'}>
                        {CuricullamData?.highlights?.map((item, i) => {

                            return (
                                <HStack py="2" pl={2} key={i} >
                                    <Image source={item?.media?.data?.attributes?.url ?? ''} w={5} h={5} alt="ImageIcon" />
                                    <Box w={"100%"}>
                                        <Text fontFamily="Nunito Sans" color="#17181a" ml={2} fontWeight={"bold"} fontSize={"16px"}>{item?.title}</Text>
                                        <Text fontFamily="Nunito Sans" w={"80%"} color="#17181a" ml={2} fontSize={"12px"}>{item?.description}</Text>
                                    </Box>
                                </HStack>
                            )
                        })}
                    </Box>
                    <Box borderRadius={"4px"} p="4" mt={5} borderWidth={1} borderColor={'rgba(176, 181, 192, 0.5)'}>
                        <Text fontFamily="Nunito Sans" pl={2} py={2} color="#17181a" fontSize={{ base: "17px", md: "20px" }} fontWeight={"medium"}>Job Opportunities Available</Text>
    
                        {CuricullamData?.jobOpportunities?.map((item, i) => {

                            return (
                                <HStack py="2" pl={2} >
                                    <Image size={18} source={{uri:'https://images.netskill.com/frontend/checkbox-circle-fill.png'}} alt={"image"}/>
                                    <Text fontFamily="Nunito Sans" ml={3} color="#17181a" fontSize={"14px"} >{item?.title}</Text>
                                </HStack>
                            )
                        })}

                    </Box>
                    <Box flexDirection={'row'} alignItems="center" justifyContent={['center', 'start']} size="s" height="50px" alignSelf={'center'}>
                        <Image width={{
                            md: "20px",
                            base: "20px",
                        }}
                            height={{
                                md: "20px",
                                base: "20px",
                            }}
                            source={{uri:'https://images.netskill.com/frontend/downIcon.png'}}
                            alt="download"
                            resizeMode="contain"
                            textAlign={"start"}
                            position={'absolute'}
                            zIndex={'5000'} left={['13px', '20px']}
                        />
                        <Button
                            alignSelf={'center'}
                            width={["250px", "280px"]}
                            size="md"
                            bg="#ff5e63"
                            variant="raised"
                            _hover={{
                                bg: "error.500",
                            }}
                            onPress={() => donwloadBtn(true)}
                            alt={"image"}
                        >
                            <Text fontWeight={"600"} fontSize={["12px", "14px"]} ml={7}>
                                {'Download Detailed Curriculum'}
                            </Text>
                        </Button>
                    </Box>

                </Box>
            </HStack >
            <Box>
                <Image w={"100%"} h={"150"} source={"https://images.netskill.com/frontend/netskill/about/banner.svg"} alt="bannerImage" />
                <Box
                    alignItems="left"
                    size="s"
                    height="50px"
                    position={"absolute"}
                    left={[0, 0, "270px"]}
                    h={"150"}
                >
                    <HStack flexDirection={{ base: "column", md: "row" }} alignItems={"center"} h={"150"} >
                        <Box justifyContent={"center"}>
                            <Text fontFamily="Nunito Sans" p={{ base: 2, md: 0 }} pt={{ base: 2, md: 0 }} fontSize={{ base: "14px", md: "16px" }} color={"white"} opacity={0.6} fontWeight={"800"}>{bottomActionData?.header?.title}</Text>
                            <Text fontFamily="Nunito Sans" p={{ base: 2, md: 0 }} pt={2} fontSize={{ base: "16px", md: "20px" }} color={"white"} fontWeight={"600"}>{bottomActionData?.header?.description}</Text>
                        </Box>
                            <Button
                                onPress={() => router?.push('/contact-us')}
                                
                                ml={"130px"}
                                h={"40px"}
                                bg={"white"}
                                size="md"
                                variant="raised"
                                px={4}
                            >
                                <Text fontFamily={'Nunito Sans'} fontWeight={"semibold"} color={'#17181a'} fontSize={"13px"} >Talk to our team</Text>
                            </Button>
                        {/* </Link> */}

                    </HStack>
                </Box>
            </Box>
            <Box pl={[3, 3, "110px"]} pr={[3, 3, "110px"]} pb={5} pt={10} w={"100%"} bg={"#f4f4f4"}>
                <Text fontFamily={'Nunito Sans'} pb={2} fontSize={{ base: "23px", md: "24px" }} fontWeight={"bold"} color={"#000000"}>{featuresData?.header?.title}</Text>
                <Text  fontFamily={'Nunito Sans'} color="#17181a" pb={5} opacity={0.6} fontSize={16}>{featuresData?.header?.description}</Text>
                <Box>
                    <ScrollView
                        contentContainerStyle={{ width: "100%", justifyContent: "flex-start" }}
                        showsHorizontalScrollIndicator={false}
                        horizontal={{
                            base: true,
                            md: true,
                        }}
                    >
                        <HStack justifyContent={"flex-start"} w={{ base: undefined, md: "100%" }} pb={10}>
                            {console.log("featuresData", featuresData)}
                            {featuresData?.features?.map((item, i) => {
                                return (
                                    <Box borderRadius={"4px"} w={{ base: 250, md: 350 }} pb={"70px"} py={4} px={5} mr={"20px"} bg={"white"}>
                                        <Text fontFamily="Nunito Sans" pt={5} w={"80%"} fontSize={"16px"} color={"#ff5e63"} fontWeight={"bold"}>{item?.caption}</Text>
                                        <Text fontFamily="Nunito Sans" pt={5} w={"80%"} fontSize={{ base: "17px", md: "20px" }} color={"#000000"} fontWeight={"bold"}>{item?.title}</Text>
                                        <Text fontFamily="Nunito Sans" mt={5} fontSize={"16px"} color={"#000000"}>{item?.description}</Text>
                                    </Box>
                                )
                            })}
                        </HStack>
                    </ScrollView>
                </Box>
            </Box>
        </Box >
    )
}
