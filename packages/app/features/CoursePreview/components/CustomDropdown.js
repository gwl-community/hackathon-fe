 
 import {
    CheckIcon,
    Select,
  } from "native-base";
  function CustomDropdown({ label, options, value, setValue }) {
    return (
      <Select
        selectedValue={value}
        minWidth="200"
        accessibilityLabel={label}
        placeholder={label}
        placeholderTextColor={"#17181a"}
        fontWeight={value ? "600" : "500"}
        fontSize={"14px"}
        // fontFamily={"Nunito Sans"}
        color="#17181a"
        borderColor={"#DDDDDD"}
        height={"12"}
        p={2}
        px={4}
        _selectedItem={{
          bg: "teal.600",
          endIcon: <CheckIcon size="5" />,
        }}
        mt={1}
        onValueChange={(itemValue) => setValue(itemValue)}
      >
        {options.map((option) => (
          <Select.Item
            label={option.text}
            value={option.value}
            px="8px"
            py="2px"
          />
        ))}
      </Select>
    );
  };

  export default CustomDropdown;