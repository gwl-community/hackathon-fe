import React, { Suspense } from 'react';
import get from "lodash.get";
import { Box, Button, HStack, Image, VStack, Text } from 'native-base';
import { SectionSpacingY, SectionSpacingX } from "../../../lib/constants";
import PlanFeeComponent from "./PlanFeeComponent";
import StudentTestimonial from '../../../components/section/StudentTestimonial';


export default function Fee({ bookSlotArray, props, testimonilaData, certificateData, title, isHomeScreen, bookSlot, data, selectedPlan, setSelectedPlan, donwloadBtn , slug}) {
    const imageUrl = get(certificateData, 'media.data[0].attributes.url', '');
    return (
        // <Suspense fallback={<></>}>
            <Box py={7} justifyContent={"space-around"} >
                <Box mb={3} bg={"#F4F4F4"}>
                    <VStack
                        bg="#17181a"
                        zIndex={2}
                        w="100%"
                        py={SectionSpacingY}
                        px={SectionSpacingX}
                    >
                        <PlanFeeComponent
                            bookSlotArray={bookSlotArray}
                            data={data}
                            title={title}
                            props={props}
                            isHomeScreen={isHomeScreen}
                            bookSlot={bookSlot}
                            selectedPlan={selectedPlan}
                            setSelectedPlan={setSelectedPlan}
                            donwloadBtn={donwloadBtn}
                            slug={slug}
                        />
                    </VStack>
                </Box>
                <Box mx={[3, 3, '113px']}>
                    <Text fontFamily="Nunito Sans" pt={8} textAlign={["center", "flex-start"]} mb={-5} fontSize={{ base: "21px", md: "24px" }} fontWeight={"bold"} color={"#000000"}>Student testimonials</Text>
                    <StudentTestimonial
                        theme={"light"}
                        data={testimonilaData?.testimonials}
                    />
                </Box>
                <Box my={12} w={'100%'} mb={3} bg={"#F4F4F4"}>
                    <HStack w={"100%"} py={10} flexDirection={["column", "column", "row"]}>
                        <Box pb={[4, 4, 0]} alignItems={["center", "center", undefined]} w={["100%", "100%", "40%"]} justifyContent={"center"} pl={[0, 0, 135]}>
                            <Image ml={[0, 0, 8]} source={"https://images.netskill.com/frontend/netskill/courseDetail/staticCertificate.png"} h={210} w={350} alt="certificateImage" />
                        </Box>
                        <Box w={["100%", "100%", "50%"]} alignItems={["center", "center", "flex-start"]} justifyContent={"center"} pl={[3, 3, 100]} >
                            <Text fontFamily="Nunito Sans" color={"#17181a"} fontWeight={"bold"} fontSize={["26px", "26px", "32px"]}>{certificateData?.header?.title}</Text>
                            <Text fontFamily="Nunito Sans" pt={5} color={"#17181a"} fontWeight={"normal"} fontSize={["16px", "20px"]}>{certificateData?.header?.description}</Text>
                        </Box>
                    </HStack>
                </Box>
            </Box>
    )
}
