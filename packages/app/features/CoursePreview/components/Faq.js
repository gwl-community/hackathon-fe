import React from "react";
import { Button, Pressable, Text, Box, HStack, Link, Image, VStack } from "native-base";
import parse from 'html-react-parser';

export default function Faq({
  data,
  Isfaq,
  setShowSlot,
  stepsData,
  setModalHeader,
  hideSteps = false,
  buttonText,
  onButtonClick,
  title,
  bookSlot,
  slug,
  nav,
  bookSlotArray,
  goToUnitOverView
}) {
  const [selected, setSelected] = React.useState(0);
  const faq = [
    {
      id: 1,
      title: "What is Netskill ?",
      subTitle: "NetSkill is a new-age AI-Driven skilling platform that provides high-quality training and guaranteed placements in top companies globally. The companies hiring from our programs are Fortune 500 companies, unicorns and fast growing product startups. We also specialise in placing students for international remote jobs.\n\n NetSkill intends to create 1 million graduates in India who are 'job ready' for high-quality software engineering and R&D roles globally.",
    },

    {
      id: 2,
      title: "Who can take these courses?",
      subTitle: "Anyone interested in becoming software developers with cutting-edge skills and starting a career in the tech industry can take up the courses. Please check the criteria for the course you are enrolled in to understand whether it's for early career professionals (1-3 years of experience) or mid level professionals (4-7 years) or students/freshers.",

    },
    {
      id: 3,
      title: "How will my doubts be resolved during the course?",
      subTitle: "Every student or a batch will be assigned Mentors/Tutors/Instructional Assistants. These mentors will guide you to understand the concepts, resolve doubts and help crack tests with Weekly guided exercises & industry projects.",

    },
    {
      id: 4,
      title: "Will I get a job after completing the course?",
      subTitle:
        "Once you crack all the unit level assessments & complete industry projects, we will assist you with jobs. Interview prep is a part of our course. We also focus on soft skills, agile project management processes, data structure & algorithms, and coding best practices apart from just hardcore tech skills. We have a 100% track record for placing students on job once they successfully complete the unit assessments and interviews!",
    },
    {
      id: 5,
      title: "Will I get placement support during the course?",
      subTitle:
        "Absolutely. Our primary goal is to get you HIRED! We focus on soft skills, corporate english, agile project management processes, data structure & algorithms, and coding best practices apart from just hardcore tech skills. Our mentors will constantly work with you to ensure you get that dream job.",
    },
  ];
  return (
    // <Suspense fallback={<></>}>
    <Box
      w={"100%"}
      alignSelf={"center"}
      pt={6}
      px={3}
      pb={hideSteps ? 0 : 20}
      justifyContent={"space-around"}
    >
      <Text
        fontFamily={"Nunito Sans"}
        pb={4}
        textAlign={["center", "flex-start"]}
        fontSize={{ base: "21px", md: "24px" }}
        fontWeight={"bold"}
        color={"#000000"}
      >
        {title ?? `Frequently Asked Questions`}
      </Text>
      {(Isfaq || data || faq).map((item, index) => {
        const source = {
          html: item?.answer
        };
        return (
          <>
            {selected === index ? (
              <Pressable onPress={() => setSelected(-1)}>
                <Box
                  h={"72px"}
                  justifyContent={"center"}
                  borderRadius={"4px"}
                  pt={4}
                  px={4}
                  mt={2}
                  bg={"#f4f4f4"}
                >
                  <HStack justifyContent={"space-between"}>
                    <Text
                      // fontFamily={"Nunito Sans"}
                      fontSize={{ base: "15px", md: "16px" }}
                      fontWeight={"600"}
                      color={"#17181a"}
                    >
                      {item?.title || item?.question}
                    </Text>
                    <Image
                      width={{ md: 8, base: 5 }}
                      height={{ md: 8, base: 5 }}
                      resizeMode="contain"
                      source={{ uri: 'https://images.netskill.com/frontend/upArrow.webp' }}  alt={"image"} />
                  </HStack>
                </Box>
                <Box borderRadius={"4px"} pb={9} px={4} mb={2} bg={"#f4f4f4"}>
                  {item?.subTitle ?
                    <Text
                      // fontFamily={"Nunito Sans"}
                      mt={2}
                      opacity={0.8}
                      fontSize={["15px", "16px"]}
                      color={"#17181a"}
                    >
                      {item?.subTitle}
                    </Text>
                    : (item?.answer ?
                      < Text
                        // fontFamily={"Nunito Sans"}
                        mt={2}
                        opacity={0.8}
                        fontSize={["15px", "16px"]}
                        color={"#17181a"}
                      >
                        {/* <RenderHtml
                            source={source}
                          /> */}
                        {item?.answer ?
                          <Box>
                            {parse(item?.answer)}
                          </Box>
                          : null}
                      </Text> : null)
                  }
                  {/* {item?.answer ?
                    <Text
                      fontFamily={"Nunito Sans"}
                      mt={2}
                      opacity={0.8}
                      fontSize={["15px", "16px"]}
                      color={"#17181a"}
                    >
                      {item?.subTitle || item?.answer}
                    </Text>
                   } */}

                </Box>
              </Pressable>
            ) : (
              <Pressable onPress={() => setSelected(index)}>
                <Box
                  h={"72px"}
                  justifyContent={"center"}
                  borderRadius={"4px"}
                  p={4}
                  my={2}
                  bg={"#f4f4f4"}
                >
                  <HStack justifyContent={"space-between"}>
                    <Text
                      // fontFamily={"Nunito Sans"}
                      fontSize={{ base: "15px", md: "16px" }}
                      fontWeight={"medium"}
                      color={"#17181a"}
                    >
                      {item?.title || item?.question}
                    </Text>
                    <Image
                      width={{ md: 8, base: 5 }}
                      height={{ md: 8, base: 5 }}
                      resizeMode="contain"
                      source={{ uri: 'https://images.netskill.com/frontend/downArrow.webp' }}  alt={"image"} />
                  </HStack>
                </Box>
              </Pressable>
            )}
          </>
        );
      })}
      {hideSteps ? null : (
        <VStack pt={10}>
          <Text 
            // fontFamily={"Nunito Sans"}
            my={5}
            textAlign={["center", "flex-start"]}
            fontSize={{ base: "21px", md: "24px" }}
            fontWeight={"bold"}
            color={"#000000"}
          >
            {stepsData?.header?.title}
          </Text>
          <HStack
            flexWrap={["wrap", "nowrap"]}
            justifyContent={["center", "flex-start"]}
          >
            {stepsData?.steps?.map((item, i) => {
              return (
                <Box
                  borderRadius={"4px"}
                  my={1}
                  w={{
                    base: "250px",
                    md: "250px",
                  }}
                  h={{
                    base: "90px",
                    md:"108px"
                  }}
                  mx={"8px"}
                  bg={"#f4f4f4"}
                  justifyContent="center"
                >

                  <Box
                    w={"28px"}
                    borderTopLeftRadius={"4px"}
                    borderBottomRightRadius={"4px"}
                    h={"28px"}
                    bg={"#595fff"}
                    position={"absolute"}
                    top={0}
                    justifyContent={"center"}
                    alignItems={"center"}
                  >
                    <Text
                      // fontFamily={"Nunito Sans"}
                      fontSize={"16px"}
                      p={2}
                      color={"white"}
                    >
                      {i + 1}
                    </Text>
                  </Box>
                  <Text
                    // fontFamily={"Nunito Sans"}
                    textAlign={"center"}
                    py={4}
                    mx={"40px"}
                    fontSize={"16px"}
                    fontWeight={"600"}
                    color={"#17181a"}
                  >
                    {item?.title}
                  </Text>
                </Box>
              );
            })}
          </HStack>
        </VStack>
      )
      }
      {
        hideSteps ? (
          <Link href={onButtonClick}>
            <Button
              onPress={onButtonClick}
              width={["150px", "150px"]}
              size="md"
              mt={{ base: "8px", md: "14px" }}
              bg="#ff5e63"
              variant="raised"
              borderWidth={1}
              borderColor={"#ff5e63"}
              _hover={{
                bg: "error.500",
              }}
            >
              <Text
              //  fontFamily={"Nunito Sans"} 
               fontSize={'15px'}>
                {buttonText || 'Contact Us'}
              </Text>
            </Button>
          </Link>

        ) : (
          <HStack
            px={2}
            py={8}
            flexDirection={{ base: "column", md: "row" }}
            justifyContent={["center", "flex-start"]}
          >
            <Box
              alignItems={["center", "flex-start"]}
              size="s"
              height="50px"
              mr={"16px"}
            >
              {bookSlotArray?.length ? null :
              <Button
                onPress={() => bookSlot()}
                width={["214px", "220px"]}
                size="md"
                bg="#ff5e63"
                variant="raised"
                borderWidth={1}
                borderColor={"#ff5e63"}
                _hover={{
                  bg: "error.500",
                }}
              >
                <Text
                //  fontFamily={"Nunito Sans"} 
                 fontSize={'15px'}>
                  Get started with this course
                </Text>
              </Button>}
            </Box>
            <Box alignItems={["center", "flex-start"]} size="s" height="50px">
              {bookSlotArray?.length ?
                <Button
                onPress={() => goToUnitOverView()}
                  // onPress={() => nav?.navigate("UnitOverview", slug)
                  // }
                  width={["124px", "180px"]}
                  borderWidth={1}
                  size="md"
                  bg="#ff5e63"
                  variant="raised"
                  borderColor={"#ff5e63"}
                  _hover={{
                    bg: "error.500",
                  }}
                  ml={-4}
                >
                 <Text
                //  fontFamily={"Nunito Sans"} 
                 fontSize={'15px'}>
                    Continue course
                  </Text>
                </Button>
                :
                <Button
                  // onPress={() => bookSlot()
                  // }
                  onPress={() => { 
                     setShowSlot(true),
                     setModalHeader("Talk to career counselor");
                    }}
                  width={["124px", "150px"]}
                  borderWidth={1}
                  borderColor={"#17181a"}
                  size="md"
                  bg="transparent"
                  variant="raised"
                >
                  <Text
                    // fontFamily={"Nunito Sans"}
                    color={"#17181a"}
                    fontSize={"15px"}
                  >
                    Take a free trial
                  </Text>
                </Button>
              }
            </Box>
          </HStack>
        )
      }
    </Box >
    // </Suspense>
  );
}
