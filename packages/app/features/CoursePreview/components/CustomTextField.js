import {
    useColorModeValue,
    View,
    Spinner
} from "native-base";
import React, { Suspense } from 'react';

import FloatingLabelInput from '../../../components/FloatingLabelInput';
function CustomTextField({
    value,
    setValue,
    label,
    multiline,
    setErrEmail,
    setErr,
    height,
}) {
    return (
        // <Suspense fallback={<></>}>
            <View mb="18px">
                <FloatingLabelInput
                    value={value}
                    label={label}
                    borderRadius={4}
                    color="#17181a"
                    labelColor="#17181a"
                    labelBGColor={useColorModeValue("#fff", "#fff")}
                    // defaultValue={value}
                    onChangeText={(txt) => {
                        setValue(txt);
                        if (label === "Email Address") setErrEmail(null);
                    }}
                    multiline={multiline}
                    hintColor="#17181A"
                    hintTextColor="#17181A"
                    style={{
                        fontFamily: "Nunito Sans",
                        fontWeight: value ? "600" : "400",
                        color: "#17181a",
                        fontSize: value ? "14px" : "12px",
                        paddingLeft: 17,
                        paddingVertical: 12,
                        border: "1px solid #DDDDDD",
                        height: height || null
                    }}
                    InputRightElement={null}
                />
            </View>
        // </Suspense>
    );
}
export default CustomTextField;