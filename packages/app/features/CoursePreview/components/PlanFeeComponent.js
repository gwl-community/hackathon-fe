import {
  VStack,
  Text,
  Image,
  HStack,
  Box,
  View,
  Button,
  ScrollView,
  FlatList,
  Icon,
} from "native-base";
import React, { useEffect, useState } from "react";
import { Pressable, TouchableOpacity } from "react-native";
import parse from 'html-react-parser';
import moment from "moment";
import { LinearGradient } from 'expo-linear-gradient';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useRouter } from 'next/router';
import {getCalclulatedDate} from "../../../lib/utils";

function PlanFeeComponent({ bookSlotArray, data, title, props, isHomeScreen, bookSlot, cId, selectedPlan, setSelectedPlan, donwloadBtn, slug }) {
  var plansArray = data?.slice(0, 2)
  var priceArray = data?.slice(2, 3)
  const [plan, setPlan] = useState(null)
  const [planValue, setPlanValue] = useState();
  const router = useRouter()
  useEffect(() => {
    setPlan(selectedPlan ? "Basic" : "Standard")
  }, [selectedPlan])
  const basicPlan = () => {
    setSelectedPlan(true);
    // setPlan('Basic');
  }
  const premiumPlan = () => {
    setSelectedPlan(false)
    // setPlan('Standard');
  }
  const continuePress = async () => {
    // await AsyncStorage.removeItem("plan");
    router.push({
      pathname: `/bill/${slug}`,
      query: {
        id: cId,
        fee: selectedPlan ?
          parseFloat(data?.[2]?.program_fees_structure?.[0]?.discountPrice.replace(/,/g, ''))
          : parseFloat(data?.[2]?.program_fees_structure?.[1]?.discountPrice.replace(/,/g, '')),
        priceplan: router?.query?.priceplan ? true : false
      },
    })
    // props.navigation.navigate("BillingPage", {
    //   slug: props?.route?.params?.slug,
    //   id: cId,
    //   fee: selectedPlan ?
    //     parseFloat(data?.[2]?.program_fees_structure?.[0]?.discountPrice.replace(/,/g, ''))
    //     : parseFloat(data?.[2]?.program_fees_structure?.[1]?.discountPrice.replace(/,/g, '')), priceplan: props?.route?.params?.priceplan ? true : false
    // })
  }
  const renderItem = (val) => {
    const data = val?.item;
    return (
      <Box mb={10} ml={4} borderRadius="8px" w={{ base: 330, md: "97%" }} shadow={plan === data?.header?.title ? '1' : '0'}>
        <Box
          borderColor={plan == data?.header?.title ? 'error.500' : '#b0b5c0'}
          borderWidth={plan == data?.header?.title ? 1 : 0}
          shadow={plan == data?.header?.title ? 1 : 0}
          key={data.id}
          borderRadius="8px"
          w={"100%"}
          maxW={"100%"}
        >
          <VStack w="100%"
            // borderColor={plan === data?.header?.title ? 'error.500' : null}
            // borderWidth={plan === data?.header?.title ? 1 : 0}
            borderWidth={0}
            borderRadius='8px'
          >
            <Box
              bg="#1D1E21"
              key={data?.header?.title}
              borderRadius='8px'
              borderWidth={0}
            >
              <HStack>
                <View style={{
                  width: data?.header?.title === "Basic" ? 90 : 132,
                  height: 40,
                  borderRadius: '32px',
                  backgroundColor: data?.header?.title === 'Basic' ? '#fff' : '#595fff',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginTop: 20,
                  marginLeft: 20,
                  alignSelf: 'flex-end'
                }}>
                  <Text
                    color={data?.header?.title === 'Basic' ? '#17181a' : '#fff'}
                    fontSize="24px"
                    fontWeight={800}
                  >
                    {data?.header?.title}
                  </Text>
                </View>

                {data?.header?.title === 'Standard' ?
                  <Image
                    width={{
                      md: "80px",
                      base: '100px',
                    }}
                    height={{
                      md: "80px",
                      base: '100px',
                    }}
                    position={'absolute'}
                    top={'-7px'}
                    right={'0px'}
                    resizeMode="contain"
                    source={{ uri: "https://images.netskill.com/frontend/netskill/home/Bestseller.svg" }}
                    alt="divider"
                  /> : null}

              </HStack>
              <Box p={{ base: "15px", md: "18px" }}>
                {data?.program_fees_structure?.[0]?.coming_soon ?
                  <View
                    // colors={['rgba(201, 201, 201, 0.048)', 'rgba(198, 198, 198, 0.0250312)', 'rgba(196, 196, 196, 0.006)']}
                    style={{ height: "97%", width: "99%", position: "absolute", left: 0, backgroundColor: '#1D1E21', zIndex: '100' }}
                  // location={[1.57, 55.4, 1]}
                  // start={[0, 0]} end={[1, 0]}
                  >
                    <Box
                      h={"100%"} w={"100%"}
                      justifyContent={"center"} alignItems={"center"}>
                      <Text
                        fontSize={{ base: "24px", md: "28px" }}
                        fontWeight="700"
                        fontFamily="Nunito Sans"
                        lineHeight={"normal"}
                        color={'#fff'}
                        opacity={0.8}>
                        Coming Soon!
                      </Text>

                    </Box>
                  </View> : null}
                <Box opacity={data?.program_fees_structure?.[0]?.coming_soon ? 0.02 : 1}>
                  <Text
                    fontSize={{ base: "16px", md: "16px" }}
                    fontWeight="normal"
                    fontFamily="Nunito Sans"
                    lineHeight={"normal"}
                    color={'#fff'}
                    opacity={0.8}
                  >
                    {data?.header?.description}
                  </Text>
                  <View style={{ alignItems: 'center', flexDirection: 'row' }}>

                    <Image
                      width={{
                        md: "27px",
                        base: "22px",
                      }}
                      height={{
                        md: "27px",
                        base: "22px",
                      }}
                      resizeMode="contain"
                      source={{ uri: "https://images.netskill.com/frontend/netskill/home/rupeeImg.webp" }}
                      alt="details_image"
                    />

                    <Text
                      fontSize={{ base: "22px", md: "32px" }}
                      fontWeight="600"
                      fontFamily="Nunito Sans"
                      lineHeight={"27px"}
                    >
                      {parse(data?.offers[0]?.details)}
                    </Text>
                    <Text
                      fontSize={{ base: "12px", md: "18px" }}
                      fontWeight="200"
                      fontFamily="Nunito Sans"
                      lineHeight={"20px"}
                      pt={2}
                    >
                      {'/year'}
                    </Text>
                  </View>

                  <Image
                    // borderTopRadius="lg"
                    width={{
                      md: "100%",
                      base: '100%',
                    }}
                    height={{
                      md: "20px",
                      base: 4,
                    }}
                    resizeMode="stretch"
                    source={{ uri: data?.header?.subTitleMedia?.data?.attributes?.url }}
                    alt="divider"
                  />
                  {(data?.course_content)?.map((details, index) => (
                    <HStack
                      pb={"9px"}
                      pt='8px'
                      justifyContent={"flex-start"}
                      space={0}
                    >
                      <Image
                        width={{
                          md: "18px",
                          base: 5,
                        }}
                        height={{
                          md: "18px",
                          base: 5,
                        }}
                        resizeMode="contain"
                        source={{ uri: details?.media?.data?.[0]?.attributes?.url }}
                        textAlign="start"
                        alignSelf={"self-start"}
                        alt="details_image"
                        pl={0}
                      />
                      <Text
                        fontSize={{
                          base: "16px",
                          md: "16px",
                        }}
                        fontFamily="Nunito Sans"
                        pl={"8px"}
                      >
                        {details?.title}
                      </Text>
                    </HStack>
                  ))}
                </Box>
              </Box>
            </Box>
          </VStack>
        </Box >
      </Box >
    );
  };

  return (
    <VStack justifyContent="center">
      <Text
        fontSize={{
          base: 26,
          md: "24px",
        }}
        fontFamily="Nunito Sans"
        fontWeight="700"
      >
        {title}
      </Text>
      <ScrollView
        contentContainerStyle={{ width: "100%" }}
        showsHorizontalScrollIndicator={false}
        horizontal={{
          base: true,
          md: false,
        }}
      >
        <HStack w={{ md: "100%", base: '100%' }} pt={{ base: 4, md: 5 }} direction={{ base: 'column', md: 'row' }}>
          <Box w={{ md: '67%', base: '100%' }}>
            <FlatList
              style={{ width: "100%" }}
              // contentContainerStyle={{
              //   justifyContent: "space-around",
              //   alignContent: "center",
              // }}
              data={plansArray}
              horizontal
              showsHorizontalScrollIndicator={false}
              renderItem={renderItem}
              keyExtractor={(item) => item.id}
            />
          </Box>

          <Box mx={2} borderRadius="8px" w={{ base: 330, md: "32%" }}>
            <Box
              shadow={1}
              borderRadius="8px"
              w={"100%"}
              maxW={"100%"}
            >
              <VStack w="100%" borderRadius="8px">
                <Box
                  bg="#1D1E21"
                  borderRadius="8px"
                  py={{ base: "15px", md: "18px" }}
                >
                  <Text
                    color={'#fff'}
                    fontSize={{ base: "20px", md: "20px" }}
                    fontWeight={800}
                    marginLeft={5}
                    fontFamily="Nunito Sans"
                  >
                    {priceArray?.[0]?.header?.title}
                  </Text>
                  <Image
                    borderTopRadius="lg"
                    width={{
                      md: "100%",
                      base: '100%',
                    }}
                    height={{
                      md: "20px",
                      base: 4,
                    }}
                    resizeMode="contain"
                    source={{ uri: 'https://images.netskill.com/frontend/ractLine.webp.webp' }}
                    alt="divider"
                  />
                  <VStack py={{ base: "15px", md: "18px" }}>
                    <HStack justifyContent={'space-around'}>
                      <HStack justifyContent={'center'} alignItems={'center'} bg={'#17181a'} w='120px' h='30px'>
                        <Text
                          color={'#b0b5c0'}
                          fontSize="12px"
                          fontWeight={'normal'}
                          fontFamily="Nunito Sans"
                        >
                          {'Duration : '}
                        </Text>
                        <Text
                          color={'#fff'}
                          fontSize="12px"
                          fontWeight={'normal'}
                          fontFamily="Nunito Sans"
                        >
                          {priceArray?.[0]?.duration[0]?.duration}{' '}{priceArray?.[0]?.duration[0]?.unit}
                        </Text>
                      </HStack>
                      <HStack justifyContent={'center'} alignItems={'center'} bg={'#17181a'} w='167px' h='30px'>
                        <Text
                          fontFamily="Nunito Sans"
                          color={'#b0b5c0'}
                          fontSize="12px"
                          fontWeight={'normal'}
                        >
                          {'Next Batch : '}
                        </Text>
                        <Text
                          color={'#fff'}
                          fontSize="12px"
                          fontWeight={'normal'}
                        >
                          {moment(getCalclulatedDate(priceArray?.[0]?.batch?.startDateTime)).format('DD MMM, YYYY')}
                          {/* {moment(priceArray?.[0]?.batch?.startDateTime).format('DD MMM, YYYY')} */}
                        </Text>
                      </HStack>
                    </HStack>
                    <TouchableOpacity disabled={priceArray?.[0]?.program_fees_structure?.[0]?.coming_soon || bookSlotArray?.length} onPress={basicPlan}>
                      <HStack mt={5} justifyContent={'space-between'} px={3}>
                        <HStack alignItems={'center'}>
                          {selectedPlan ?
                            <Image
                              marginRight={2}
                              width={{
                                md: "24px",
                                base: 5,
                              }}
                              height={{
                                md: "24px",
                                base: 5,
                              }}
                              resizeMode="contain"
                              source={{ uri: 'https://images.netskill.com/frontend/Redselected.svg' }}
                              alt="details_image"
                              pl={0}
                            /> :
                            <>
                              <Image
                                opacity={priceArray?.[0]?.program_fees_structure?.[0]?.coming_soon || bookSlotArray?.length ? 0.3 : 1}
                                marginRight={2}
                                width={{
                                  md: "24px",
                                  base: 5,
                                }}
                                height={{
                                  md: "24px",
                                  base: 5,
                                }}
                                resizeMode="contain"
                                source={{ uri: "https://images.netskill.com/frontend/netskill/home/circle.svg" }}
                                alt="details_image"
                                pl={0}
                              />
                            </>
                          }

                          <Text
                            color={'#FFFFFF'}
                            fontSize="16px"
                            fontWeight={'700'}
                            lineHeight={'21.82px'}
                            fontFamily={'Nunito Sans'}
                            opacity={priceArray?.[0]?.program_fees_structure?.[0]?.coming_soon || bookSlotArray?.length ? 0.3 : 1}
                          >
                            {/* {'Basic'} */}
                            {priceArray?.[0]?.program_fees_structure?.[0]?.title}
                          </Text>
                        </HStack>
                        {priceArray?.[0]?.program_fees_structure?.[0]?.coming_soon ?
                          <VStack width="45%" alignSelf={'flex-end'} mt={-2}>
                            <LinearGradient
                              colors={['#424345', '#424345', 'rgba(196, 196, 196, 0.006)']}
                              style={{}}
                              location={[1.57, 55.4, 1]}
                              start={[0, 0]} end={[1, 0]}
                            >
                              <Text
                                fontFamily="Nunito Sans"
                                py={"5px"}
                                px={"15px"}
                                color={'#fff'}
                                fontSize="14px"
                                fontWeight={'600'}
                                style={{ textDecorationColor: "#ff5e63" }}
                              >
                                Coming Soon
                              </Text>
                            </LinearGradient>
                          </VStack>
                          : <HStack opacity={priceArray?.[0]?.program_fees_structure?.[0]?.coming_soon || bookSlotArray?.length ? 0.3 : 1} justifyContent={'center'} alignItems={'center'}>
                            <Text
                              fontFamily="Nunito Sans"
                              color={'#FFFFFF'}
                              fontSize="14px"
                              fontWeight={'800'}
                              lineHeight={'100%'}
                            >
                              {priceArray?.[0]?.program_fees_structure?.[0]?.currency}{' '}{priceArray?.[0]?.program_fees_structure?.[0]?.discountPrice} {'  '}
                              <Text
                                fontFamily="Nunito Sans"
                                color={'#B0B5C0'}
                                fontSize="14px"
                                lineHeight={'100%'}
                                fontWeight={'normal'}
                                textDecorationLine="line-through"
                                style={{ textDecorationColor: "#ff5e63" }}
                              >
                                {priceArray?.[0]?.program_fees_structure?.[0]?.currency}{' '}{priceArray?.[0]?.program_fees_structure?.[0]?.actualPrice}
                              </Text>
                            </Text>
                          </HStack>
                        }
                      </HStack>
                    </TouchableOpacity>

                    <Image
                      borderTopRadius="lg"
                      alignSelf={'center'}
                      width={{
                        md: "93%",
                        base: '93%',
                      }}
                      height={{
                        md: "20px",
                        base: 4,
                      }}
                      resizeMode="contain"
                      source={{ uri: 'https://images.netskill.com/frontend/ractLine.webp.webp' }}
                      alt="divider"
                      mt={2}
                    />
                    <TouchableOpacity onPress={premiumPlan}>
                      <HStack mt={3} justifyContent={'space-between'} px={3}>
                        <HStack alignItems={'center'}>
                          {!selectedPlan ?
                            <Image
                              marginRight={2}
                              width={{
                                md: "24px",
                                base: 5,
                              }}
                              height={{
                                md: "24px",
                                base: 5,
                              }}
                              resizeMode="contain"
                              source={{ uri: 'https://images.netskill.com/frontend/Redselected.svg' }}
                              alt="details_image"
                              pl={0}
                            />
                            :
                            <>
                              <Image
                                marginRight={2}
                                width={{
                                  md: "24px",
                                  base: 5,
                                }}
                                height={{
                                  md: "24px",
                                  base: 5,
                                }}
                                resizeMode="contain"
                                source={{ uri: "https://images.netskill.com/frontend/netskill/home/circle.svg" }}
                                // textAlign="start"
                                // alignSelf={"self-start"}
                                alt="details_image"
                                pl={0}
                              />
                            </>
                          }

                          <Text
                            color={'#FFFFFF'}
                            fontSize="16px"
                            fontWeight={'700'}
                            lineHeight={'21.82px'}
                            fontFamily={'Nunito Sans'}
                          >
                            {/* {'Standard'} */}
                            {priceArray?.[0]?.program_fees_structure?.[1]?.title}
                          </Text>
                        </HStack>
                        <HStack justifyContent={'center'} alignItems={'center'}>
                          <Text
                            fontFamily="Nunito Sans"
                            color={'#FFFFFF'}
                            fontSize="14px"
                            fontWeight={'800'}
                            lineHeight={'100%'}
                          >
                            {priceArray?.[0]?.program_fees_structure?.[1]?.currency}{' '}{priceArray?.[0]?.program_fees_structure?.[1]?.discountPrice}{'  '}
                          </Text>
                          <Text
                            fontFamily="Nunito Sans"
                            color={'#B0B5C0'}
                            fontSize="14px"
                            fontWeight={'normal'}
                            lineHeight={'100%'}
                            textDecorationLine="line-through"
                            style={{ textDecorationColor: "#ff5e63" }}
                          >
                            {priceArray?.[0]?.program_fees_structure?.[1]?.currency}{' '}{priceArray?.[0]?.program_fees_structure?.[1]?.actualPrice}
                          </Text>
                        </HStack>
                      </HStack>
                    </TouchableOpacity>

                    {isHomeScreen ?
                      <>
                        {bookSlotArray?.length ?
                          <Button
                            width={["250px", "90%"]}
                            size="lg"
                            bg="#ff5e63"
                            variant="raised"
                            _hover={{
                              bg: "error.500",
                            }}
                            height={'48px'}
                            marginTop={5}
                            alignSelf={'center'}
                            onPress={() => {
                              router.push({
                                pathname: '/unit-overview/[slug]',
                                query: {
                                  slug: slug,
                                },
                              })
                            }}
                          // onPress={() => { props.navigation.navigate("UnitOverview", { slug: props?.route?.params?.slug }) }}
                          >
                            <Text
                              fontFamily="Nunito Sans"
                              fontWeight={"600"} fontSize={["12px", "14px"]}>
                              {'Continue course'}
                            </Text>
                          </Button>
                          :
                          <Button
                            width={["250px", "90%"]}
                            size="lg"
                            bg="#ff5e63"
                            variant="raised"
                            _hover={{
                              bg: "error.500",
                            }}
                            height={'48px'}
                            marginTop={5}
                            alignSelf={'center'}
                            // onPress={() => props.navigation.navigate("PlanFee", { routeData: data, isHomeScreen: true })}
                            onPress={bookSlot}
                          >
                            <Text
                              fontFamily="Nunito Sans"
                              fontWeight={"600"}
                              fontSize={["12px", "14px"]}
                            >
                              {'Book your slot'}
                            </Text>
                          </Button>
                        }

                        <Button
                          width={["250px", "90%"]}
                          borderWidth={1}
                          borderColor={"white"}
                          size="lg"
                          bg="transparent"
                          variant="raised"
                          alignSelf={'center'}
                          marginTop={5}
                          height={'48px'}
                          onPress={() => router?.push('/contact-us')}
                        >
                          <Text
                            fontFamily="Nunito Sans"
                            fontWeight={"600"}
                            fontSize={["12px", "14px"]}>
                            {'Contact Us'}
                          </Text>
                        </Button>
                      </>
                      :
                      <Button
                        width={["250px", "90%"]}
                        size="lg"
                        bg="#ff5e63"
                        variant="raised"
                        _hover={{
                          bg: "error.500",
                        }}
                        marginTop={5}
                        alignSelf={'center'}
                        onPress={continuePress}
                      >
                        <Text
                          fontFamily="Nunito Sans"
                          fontWeight={"600"}
                          fontSize={["12px", "14px"]}>
                          {'Continue'}
                        </Text>
                      </Button>

                    }

                    {/* <HStack p={{ base: "15px", md: "18px" }} alignItems={'center'}>
                      <Text
                        color={'#f4f4f4'}
                        fontSize="20px"
                        fontWeight={'600'}
                      >
                        {'Starting at '}
                      </Text>
                      <Text
                        color={'#ff5e63'}
                        fontSize="20px"
                        fontWeight={'normal'}
                      >
                        {'6,000'}
                      </Text>
                      <Text
                        color={'#ff5e63'}
                        fontSize="14px"
                        fontWeight={'normal'}
                      >
                        {' '}{'per month'}
                      </Text>
                    </HStack> */}

                    {/* <Text
                      pt={"18px"}
                      px={{ base: "15px", md: "18px" }}
                      color={'#b0b5c0'}
                      fontSize="12px"
                      fontWeight={'normal'}
                    >
                      {'EMI Options / Students Loans Available'}
                    </Text> */}
                    {isHomeScreen ?
                      <Button
                        width={["250px", "90%"]}
                        borderWidth={1}
                        borderColor={"white"}
                        size="lg"
                        bg="transparent"
                        variant="raised"
                        alignSelf={'center'}
                        marginTop={5}
                        height={'48px'}
                        flexDirection={'row'}
                        onPress={() => donwloadBtn(true)}
                      >
                        
                        <Image width={{
                          md: "20px",
                          base: "20px",
                        }}
                          height={{
                            md: "20px",
                            base: "20px",
                          }}
                          source={{ uri: 'https://images.netskill.com/frontend/downIcon.png' }}
                          alt="download"
                          resizeMode="contain"
                          textAlign={"start"}
                          position={'absolute'}
                          zIndex={'5000'} left={['-20px', '-25px']}
                        />
                        <Text
                          fontFamily="Nunito Sans"
                          fontWeight={"600"}
                          fontSize={["12px", "14px"]}
                          ml={5}
                        >
                          {'Download brochure'}
                        </Text>
                      </Button> : null}

                  </VStack>
                </Box>

              </VStack>
            </Box >
          </Box >
        </HStack>

      </ScrollView>
    </VStack>
  );
}

export default PlanFeeComponent;
