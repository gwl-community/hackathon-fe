import * as React from 'react';
import get from "lodash.get";
import { VStack, Image, Text, Box, HStack, ScrollView } from 'native-base';

export default function TutorsMentor({ data, subtitle, title, bottomSpace, width, right, data1, isCoursepreview = false }) {
  return (
    <Box px={{ base: 0, md: 0 }} pb={"20"} justifyContent={"space-around"} >
      <Text fontFamily="Nunito Sans" px={{ base: 3, md: 0 }} fontSize={{ base: "22px", md: "24px" }} fontWeight={"bold"} color={"#000000"}>{title}</Text>
      {subtitle ?
        <Text fontFamily="Nunito Sans" px={{ base: 3, md: 0 }} fontSize={{ base: "15px", md: "16px" }} opacity={0.6} mt={3} fontWeight={"normal"} color={"#17181A"}>{subtitle}</Text> : null}
      <Box pb={bottomSpace || "0px"} pt={{ base: 0, md: "5" }} w={{ base: "100%", md: "100%" }}>
        <ScrollView pt={3} showsHorizontalScrollIndicator={false} horizontal contentContainerStyle={{ width: "100%" }}>
          {(isCoursepreview ? data1?.mentors : data)?.map((item, index) => (
            <Box mr={right || "18px"} key={index} borderRadius="4px" w={{ base: 350, md: "100%" }} minW={{ base: 350, md: "31%" }} maxWidth={{ base: 350, md: "32%" }}>
              <HStack>
                {item?.media?.data?.[0]?.attributes?.url || item?.image ?
                  <Image
                    borderLeftRadius={"4px"}
                    width={{
                      md: "100px",
                      base: "100px",
                    }}
                    height={{
                      md: "120px",
                      base: "120px",
                    }}
                    resizeMode="cover"
                    source={isCoursepreview ? item?.media?.data[0]?.attributes?.url : item?.image}
                    alt="Alternate Text"
                  />
                  : null}
                <Box
                  bg="#F4F4F4"
                  p={2}
                  width={"70%"}
                  borderTopRightRadius="6px"
                  borderBottomRightRadius="6px"
                >
                  <VStack pl={{ base: 2, md: 5 }} h={"100%"} py={2} justifyContent={'space-around'}>
                    <Box>
                      <Text fontFamily="Nunito Sans" fontSize="16px" color="#000000" fontWeight="700" pb="2px">
                        {isCoursepreview ? item?.title : item.name}
                      </Text>
                      <Text fontFamily="Nunito Sans" fontSize={"14px"} fontWeight="600" color="#17181A" opacity="0.6">
                        {isCoursepreview ? item?.subTitle : item?.position}
                      </Text>
                    </Box>
                    <HStack justifyContent={"flex-start"}>
                      {(isCoursepreview ? item?.clients?.data : item?.company)?.map((logo) => (
                        <Image mr={"24px"}
                          //style={{aspectRatio: logo?.aspectRatio}}
                          width={{
                            md: "80px",
                            base: 20,
                          }}
                          height={{
                            md: "24px",
                            base: 7,
                          }}
                          resizeMode="contain"
                          source={isCoursepreview ? get(logo, 'attributes.url', 'https://images.netskill.com/frontend/netskill/courseDetail/NetSkill.svg') : logo?.name}
                          alt="company_logo"
                        />
                      ))}
                    </HStack>
                  </VStack>
                </Box>
              </HStack>
            </Box>
          ))}
        </ScrollView>
        {/* </HStack> */}
        {/* </ScrollView> */}
      </Box>
      {/* <Text fontFamily="Nunito Sans" fontSize={"20px"} fontWeight={"medium"} color={"#000000"}>Our program structure</Text>
            <Text fontFamily="Nunito Sans" pb={5} opacity={0.6} fontSize={"14px"} mt={3} fontWeight={"normal"} color={"#17181A"}>Our curriculum is vetted by Industry experts and delivered to maximise student learning & outcome.</Text> */}
    </Box>
  )
}
