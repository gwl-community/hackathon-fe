import * as React from 'react';
import { VStack, Image, Text, Box, HStack, FlatList } from 'native-base';
import get from "lodash.get";

export default function Overview({finalTitle, isCoursepreview = false, overData}) {
    const renderItem = ({ item }) => {
        return (
            <HStack flexDirection={{
                base: "column",
                md: "row",
            }} px={{
                base: 2,
                md: 6,
            }} py={8} flex={0.5}>
                <Box flex={{base: undefined,md:0.1}} alignItems={"center"}>
                    <Image source={{
                        uri: get(item,'media.data[0].attributes.url','https://images.netskill.com/frontend/netskill/courseDetail/Overview4.webp')
                    }} w={7} h={7} alt="course overview icon"/>
                </Box>
                <Box flex={{base: undefined,md:0.9}} flexWrap={"wrap"} pl={{
                    base: 0,
                    md: 4,
                }} >
                    <Text fontFamily="Nunito Sans" textAlign={{
                        base: "center",
                        md: "flex-start",
                    }} fontSize={{
                        base: "14px",
                        md: "16px",
                    }} color={"#17181A"} fontWeight={"700"} >{item?.title}</Text>
                    <Text fontFamily="Nunito Sans" textAlign={{
                        base: "center",
                        md: "flex-start",
                    }} pt={3} w={"100%"} fontSize={"14px"} color={"#17181A"} >{item?.description}</Text>
                </Box>
            </HStack>
        );
    };

    let result = finalTitle?.replace("Development Course", "developer");
    return (
        <Box pt={8} pb={4} px={[3,0]} justifyContent={"space-around"} >
            <Text fontFamily="Nunito Sans" fontSize={{
                base: "22px",
                md: "24px",
            }} fontWeight={"bold"}  color={"#000000"}>{get(overData,'header.title','About Fullstack Development Course')}</Text>
            <Text fontFamily="Nunito Sans" fontSize={{
                base: "15px",
                md: "14px",
            }} mt={3} fontWeight={"normal"} color={"#17181A"}>{get(overData,'header.description','')}</Text>
            <Box my={10} borderRadius={"4px"} bg={"#F4F4F4"} >
                <FlatList
                    data= {get(overData,'overview','')}
                    numColumns={2}
                    renderItem={renderItem}
                    keyExtractor={(item) => item.id}
                />
            </Box>
        </Box>
    )
}
