import {
  Box,
  Button,
  HStack,
  Image,
  Input,
  ScrollView,
  Skeleton,
  Text,
  VStack,
  useBreakpointValue,
} from "native-base";

import React from "react";
import { View } from "react-native";
import get from "lodash.get";

export default function Filterlist({ data, loading }) {
  const { buttons = [], description, title, bg = "#ffffff" } = data;
  const flexDir = useBreakpointValue({
    base: "column",
    md: "row",
  });
  return (
    <VStack
      zIndex={2}
      bg={bg}
      pl={24}
      pt={16}
      mt={-4}
      //   contentContainerStyle={{ flexWrap: "wrap", alignItems: "flex-start" }}
    >
      <Skeleton.Text isLoaded={loading} w={{ md: "55%", base: "100%" }}>
        <Text
          fontSize={{
            md: "md",
          }}
          bold
        >
          {title}
        </Text>
        <Text
          fontSize={{
            md: "xs",
          }}
        >
          {description}
        </Text>
      </Skeleton.Text>
      <Skeleton.Text
        isLoaded={loading}
        w={{
          base: "100%",
          md: "75%",
        }}
      >
        <Text
          width={{
            base: "100%",
            md: "75%",
          }}
          fontSize={{
            md: "sm",
          }}
          noOfLines={3}
        >
          {description}
        </Text>
      </Skeleton.Text>

      <HStack
        space="4"
        py={{
          md: 8,
          base: 8,
        }}
      >
        {buttons.map((button, index) => {
          let extraProps = {};
          const { leftIcon = {}, rightIcon = {} } = button;
          const leftUrl = get(leftIcon, "data.attributes.url", "");
          const leftAlternativeText = get(
            leftIcon,
            "data.attributes.alternativeText",
            ""
          );
          const rightUrl = get(rightIcon, "data.attributes.url", "");
          const rightAlternativeText = get(
            rightIcon,
            "data.attributes.alternativeText",
            ""
          );
          if (leftIcon) {
            extraProps = {
              leftIcon: (
                <Image
                  alt={"image"}
                  source={{ uri: leftUrl }}
                  alternativeText={leftAlternativeText}
                  width="24px"
                  height="24px"
                />
              ),
            };
          }
          if (rightIcon) {
            extraProps = {
              ...extraProps,
              rightIcon: (
                <Image
                  alt={"image"}
                  source={{ uri: rightUrl }}
                  alternativeText={rightAlternativeText}
                  width="24px"
                  height="24px"
                />
              ),
            };
          }
          return (
            <Skeleton rounded="md" startColor="primary.100" isLoaded={loading}>
              <Button
                
                size="md"
                bg={"white"}
                borderRadius={"4px"}
                height={"48px"}
                // shadow={"0 0 8px 0 rgba(0, 0, 0, 0.12)"}
                shadow={"2"}
                key={index}
                width="20%"
                // w={"50%"}
                {...extraProps}
              >
                {button.text}
              </Button>
            </Skeleton>
          );
        })}
      </HStack>
    </VStack>
  );
}
