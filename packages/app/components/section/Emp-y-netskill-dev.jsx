import {
    VStack,
    Text,
    Image,
    HStack,
    Box,
    View,
    Button,
    ScrollView,
    Center,
    FlatList
} from "native-base";
import React, { useState } from "react";
import { TouchableOpacity } from "react-native";
import { MaterialIcons, FontAwesome } from "@expo/vector-icons";
import { SectionSpacingX, SectionSpacingY } from "../../lib/constants";

export default function YemployeenetskillCertifiction({
    data,
    title,
    subtitle,
    shapeImage,
    mainImage,
    HandleClick,
    handleViewAll,
    showExploreButton,
}) {
    const info = [
        {
            url: 'pse',
            description: (
                <Box>
                    <Text color={"#17181a"} fontSize="16px" fontWeight={600}>Pre-screened
                    </Text>
                    <Text color={"#17181a"} fontSize="16px" fontWeight={800} fontFamily='NunitoSans_700Bold'>engineers
                    </Text>
                </Box>)
            ,
        },
        {
            url: 'jrfd',
            description: (
                <Box>
                    <Text color={"#17181a"} fontSize="16px" fontWeight={800} fontFamily='NunitoSans_700Bold'>
                        ‘Job Ready’

                    </Text>

                    <Text color={"#17181a"} fontSize="16px" fontWeight={600}>from Day 1 </Text>
                </Box>),
        },
        {
            url: 'cps',
            description: (
                <Box>
                    <Text color={"#17181a"} fontSize="16px" fontWeight={800} fontFamily='NunitoSans_700Bold'>Comprehensive
                    </Text>
                    <Text color={"#17181a"} fontSize="16px" fontWeight={600} >{`Profiles with
scoring`}
                    </Text>
                </Box>),

        },
        {
            url: 'hln',
            description: (
                <Text color={"#17181a"} fontSize="16px"><b>Hire</b> in large {'\n'}numbers

                </Text>

            ),

        }
    ]
    const renderItem = ({ item }) => {
        return (
            <HStack

                flexDirection={{
                    base: "column",
                    md: "row",
                }}
                px={{
                    base: 3,
                    md: 0,
                }}
                py={8}
                flex={0.5}
            >
                <VStack
                    borderRadius="16px"
                    width="150"
                    height="120"
                    alignItems={"left"}
                >
                    <Image
                        width='40px'
                        height={'40px'}
                        source={{ uri: item?.media[0].url }}
                        alt={'icons'}
                        resizeMode="cover"
                    />
                    <Box mt={3}>
                        <Text  color={"#17181a"} fontSize="16px" fontWeight={item?.id == (37 || 38) ? 700 :600}>{item?.title}
                        </Text>
                        <Text color={"#17181a"} fontSize="16px" fontWeight={item?.id == 36 ? 700 : 600} >{item?.description}
                        </Text>
                    </Box>
                </VStack>
            </HStack>
        );
    };
    return (

        <HStack
            pl={SectionSpacingX}
            height={['1000px', "859px"]}
            bg='#f4f4f4'
            space={'4px'}
            flexDirection={{ base: "column", md: "row" }}

        >
            <Box
                // marginLeft={{ md: '50px', base: '0' }}
                top={{ md: '100px', base: '70px' }}
            >

                <Button
                    width={'146px'}
                    height='32px'
                    borderRadius={'24px'}
                    bg='#595fff'
                    variant={'unstyled'}>
                    <Text
                        px={{ base: 2, md: 0 }}
                        fontSize={{
                            base: "14px",
                            md: "12px",
                        }}
                        fontWeight={600}
                        // fontFamily="Nunito Sans"
                        color='#fff'
                    >
                        {`FOR EMPLOYERS`}
                    </Text>
                </Button>
                <Text
                    px={{ base: 2, md: 0 }}
                    fontSize={{
                        base: 15,
                        md: "24px",
                    }}
                    width={{
                        base: "90%",
                        md: "90%",
                    }}
                    color="#262627"
                    // fontFamily="NunitoSans_600SemiBold"
                    fontWeight={700}
                    pt={5}
                    mb={2}
                >
                    {data?.header?.title ?? "Why you should hire Netskill Certified Developers?"}
                </Text>
                <Text
                    px={{ base: 2, md: 0 }}

                    fontSize={{ base: "15px", md: "16px" }}
                    fontWeight={"600"}
                    color="#262627"
                    fontFamily="Nunito Sans"
                    height={'88px'}
                    opacity={0.6}
                    //pt={2}
                    width={{ md: '491px', base: '360px' }}
                //mb={2}
                >
                    {data?.header?.description ?? `Netskill Assessments are administered on our AI-powered platform that comprehensively tests and grades engineers on multiple attributes. In addition to clearing the tests, they are ready to start contributing from Day 1 because of our industry-best curriculum and project led learning.`}
                </Text>
                <Box
                    pt={{ md: 10, base: 10 }}
                >
                    <FlatList
                        data={data?.course_content}
                        numColumns={2}
                        renderItem={renderItem}
                        keyExtractor={(item) => item.id}
                    />

                </Box>
                <Box
                    ml={{ base: '13px', md: '' }}>
                    <Button
                        onPress={HandleClick}
                        minW="201px"
                        variant="raised"
                        width={{ base: "40%", md: "17.56%" }}
                        height={{ base: '48px', md: '48px' }}
                        size="md"
                        bg="#ff5e63"
                        _text={{
                            color: "white",
                            fontSize: 15,
                            // fontFamily: "NunitoSans_600SemiBold",
                        }}
                        // fontFamily="Nunito Sans"
                        _hover={{
                            bg: "error.500",
                        }}
                    >
                        Hire Developers
                    </Button>

                </Box>
            </Box>
            <VStack
                top={["90px", "250px"]}

                alignItems={{ base: "center", md: 'none' }}
                width={{ base: "100%", md: "65%" }}
            >

                <Image
                    width={{
                        md: "540px",
                        base: "100%",
                    }}
                    height={{
                        md: "500px",
                        base: 250,
                    }}
                    source={shapeImage}
                    alt="oval"
                    resizeMode="contain"
                    position="relative"
                    ml={["40px", "90px"]}
                />
                <Image
                    width={{
                        md: "660px",
                        base: "100%",
                    }}
                    height={{
                        md: "420px",
                        base: 250,
                    }}
                    source={mainImage}
                    alt="laptop"
                    resizeMode="contain"
                    position="absolute"
                    zIndex={10}
                    top={{ base: "0px", md: "30px" }}

                />
            </VStack>
        </HStack>

    );
}




