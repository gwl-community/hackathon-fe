import {
  Box,
  Button,
  HStack,
  Image,
  Pressable,
  ScrollView,
  Stack,
  Text,
  VStack,
} from "native-base";

import { API_DOMAIN } from "../../config/constants";
import Markdown from "react-native-markdown-display";
import React from "react";
import get from "lodash.get";

const ImageSec = ({ url, alternativeText }) => {
  return url ? (
    <Image
      width={{ md: "50%", base: "100%" }}
      height={{
        base: "full",
        md: "500",
      }}
      alt={"image"}
      source={`${url}`}
      resizeMode="cover"
    />
  ) : (
    <></>
  );
};

const Content = ({
  title,
  description,
  content,
  url,
  bgColor = "#ffffff",
  smallText,
}) => {
  return (
    <VStack
      width={{ md: url ? "50%" : "100%", base: "100%" }}
      space="2"
      pl={{
        md: 16,
        base: 4,
      }}
      py={10}
      _light={{
        bg: {
          md: bgColor,
        },
      }}
      _dark={{
        bg: bgColor,
      }}
      justifyContent="center"
      alignItems={url ? "flex-start" : "center"}
    >
      <Text
        fontSize="xl"
        bold
        width={{
          base: url && "70%",
          md: url && "70%",
        }}
      >
        {title}
      </Text>
      <Text
        fontSize="xs"
        width={{
          base: url ? "80%" : "50%",
          md: url ? "80%" : "50%",
        }}
      >
        {description}
      </Text>
      {content && (
        <Stack
          width={{
            base: "80%",
            md: "80%",
          }}
        >
          <Markdown>{content}</Markdown>
        </Stack>
      )}
      <Text
        fontSize={{
          md: "sm",
        }}
      >
        {smallText}
      </Text>
    </VStack>
  );
};

export default function FeaturedRowGrp({ data }) {
  const { title, description, features = [], bgColor = "#ffffff" } = data;
  let urlCount = 0;
  return (
    <VStack
      borderRadius="lg"
      // mt={{
      //   base: "4",
      //   md: "1",
      // }}
      _light={{
        bg: {
          md: bgColor,
        },
      }}
      _dark={{
        bg: bgColor,
      }}
    >
      {features.map((item, index) => {
        const { title, description, content, media = {}, bgColor } = item;
        const url = get(media, "data.attributes.url", "");
        const alternativeText = get(
          media,
          "data.attributes.alternativeText",
          ""
        );
        if (url) {
          urlCount++;
        }
        const remainder = urlCount % 2;
        return (
          <HStack key={index}>
            {remainder == 0 ? (
              <>
                <ImageSec url={url} alternativeText={alternativeText} />
                <Content
                  title={title}
                  description={description}
                  content={content}
                  url={url}
                  bgColor={bgColor}
                />
              </>
            ) : (
              <>
                <Content
                  title={title}
                  description={description}
                  content={content}
                  url={url}
                  bgColor={bgColor}
                />
                <ImageSec url={url} alternativeText={alternativeText} />
              </>
            )}
          </HStack>
        );
      })}
    </VStack>
  );
}
