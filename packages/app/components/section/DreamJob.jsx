import { VStack, Text, HStack, Box, Image } from "native-base";
import React, { useState, useEffect } from "react";
import { Pressable } from "react-native";
// import Image from 'next/image';
const externaImageLoader = ({ src }: { src: string }) =>
  `${src}`;
function DreamJob({ title, subtitle, data }) {
  const [inwdith, setInwidth] = useState(0);
  const [show, hide] = useState(false);
  useEffect(() => {
    const newWidth = window?.innerWidth;
    setInwidth(newWidth)
  }, []);
  const showList= () => {
    return (
      <VStack justifyContent="space-between">
      {data?.map((step, index) => (
        <Box
          style={{ boxShadow: "0px 2px 12px rgba(0, 0, 0, 0.1)" }}
          height={{ base: undefined, md: 120 }}
          width={{ base: "80%", md: "100%" }}
          overflow={"hidden"}
          mx={{ md: "12px", lg: "24px" }}
          borderRadius="4px"
          alignSelf={"center"}
          mt={"10px"}
        >
          <HStack
            space={0}
            justifyContent="space-between"
            width="100%"
            height="100%"
            alignItems="center"
          >
            <VStack
              justifyContent="center"
              alignItems="center"
              w={{ base: "11%", md: "9%" }}
            >
              <Text
                fontSize={{
                  base: "24px",
                  md: "32px",
                }}
                _dark={{
                  color: "coolGray.400",
                }}
                _light={{
                  color: "coolGray.400",
                }}
                textAlign="center"
                fontFamily="Nunito Sans"
                width={"100%"}
                height={"100%"}
                fontWeight={{ base: "600", md: "700" }}
              //   px={6}
              //   my="auto"
              >
                {index + 1}
              </Text>
            </VStack>
            <VStack
              space={1}
              justifyContent="center"
              w={{ base: "69%", md: "71%" }}
            >
              <HStack flexDir={{ base: "column", md: "row" }}>
                <Text
                  fontSize={{
                    base: "18px",
                    md: "20px",
                  }}
                  _dark={{
                    color: "#17181A",
                  }}
                  fontFamily="Nunito Sans"
                  fontWeight="700"
                  _light={{
                    color: "coolGray.800",
                  }}
                  mb="2px"
                >
                  <h4 style={{ marginTop: '0px', marginBottom: '0px', fontFamily: 'Nunito Sans' }}>
                    {step?.title}
                  </h4>
                </Text>
              </HStack>
              <Text
                //mt={{md:'10px'}}
                fontSize={{
                  base: 13,
                  md: "14px",
                }}
                _dark={{
                  color: "#17181A",
                }}
                fontFamily="Nunito Sans"
                opacity="0.7"
                _light={{
                  color: "#17181A",
                }}
                fontWeight="600"
              >
                {step?.description ?? step?.content}
              </Text>
            </VStack>

            <VStack pr={{ base: 3, md: "0px" }} w={"24%"} >
              {step?.media ?
                <Box
                  alignItems="center"
                  flexDirection="row"
                  w="100%"
                >
                  {step?.media?.[0]?.url &&
                    <Image
                      alt={"image"}
                      w={"100%"}
                      height={"115px"}
                      src={step?.media?.[0]?.url}
                    />}
                  {/* {step?.media?.[1]?.url &&
                    <Image
                      alt={"image"}
                      width={'110px'}
                      height={"110px"}
                      src={step?.media?.[1]?.url}
                    />} */}
                </Box> : null
              }
            </VStack>
          </HStack>
        </Box>
      ))}
    </VStack>
    )
  }
  return (
    <VStack justifyContent="center" alignSelf={"center"} maxW="792px">
      <Pressable onPress={() => hide(!show)}>
          <Text
            fontSize={{
              base: "14px",
              md: "16px",
            }}
            fontWeight={"700"}
            fontFamily="Nunito Sans"
            textAlign="center"
            _dark={{
              color: "#17181A",
            }}
            _light={{
              color: "#17181A",
            }}
            mb={4} mr={5}
          >
            <h2
              style={{ marginTop: '0px', marginBottom: '0px' }}>
              {title} 
              {/* //\\How can Netskill Community help with your career? */}
            </h2>
          </Text>
          {inwdith < 700 &&
          <Box position={"absolute"} right={2}>
              <Image   
                alt={"image"}
                width={"22px"}
                height={"22px"}
                src={show ? 'https://images.netskill.com/frontend/upArrow.webp' : 'https://images.netskill.com/frontend/downArrow.webp'}
               />
              </Box>
              }
      </Pressable>
      <Text
        fontSize={{
          base: 15,
          md: "16px",
        }}
        _dark={{
          color: "#17181A",
        }}
        _light={{
          color: "#17181A",
        }}
        fontFamily="Nunito Sans"
        textAlign="center"
        // px={{ base: "30px", md: undefined }}
        opacity={0.6}
        mb={7}
        fontWeight={"400"}
        w={{base: "100%", md:"100%"}}
      >
        {/* Join our buzzing community of professionals & industry mentors from FAANG+ companies to discover career paths, take courses and find work opportunities! */}
        {subtitle}
      </Text>
      {(inwdith < 700 && show) ?
     showList() : (inwdith > 750) ?   showList()  :null }
    </VStack>
  );
}
export default DreamJob;
