import {
  Box,
  Button,
  HStack,
  Image,
  Pressable,
  ScrollView,
  Skeleton,
  Text,
  VStack,
} from "native-base";

import { API_DOMAIN } from "../../config/constants";
import React from "react";
import get from "lodash.get";

function Card({ item, index, mediaW, mediaH, cardW, cardH, loading }) {
  const {
    title,
    description,
    isCard,
    bgColor,
    alignItem = "flex-start",
    justifyContent = "flex-start",
    media = {},
    color,
    hasNoHeader,
  } = item;
  const url = get(media, "data.attributes.url", "");
  const alternativeText = get(media, "data.attributes.alternativeText", "");
  // <Skeleton my={8} w={cardW} p={"24px"} h={cardH}>

  return (
    <VStack
      my={8}
      borderRadius="lg"
      width={cardW}
      height={cardH}
      _light={{
        bg: bgColor,
      }}
      _dark={{
        bg: bgColor,
      }}
      p={"24px"}
      alignItems={alignItem}
      justifyContent={justifyContent}
    >
      {url && (
        <Image
          width={mediaW}
          height={mediaH}
          source={{ uri: `${url}` }}
          alt={"image"}
          resizeMode="cover"
        />
      )}

      {hasNoHeader && (
        <Text
          _light={{
            bg: "#ffffff",
          }}
          _dark={{
            bg: "#ffffff",
          }}
          width="32px"
          height="32px"
          fontSize="sm"
          bold
          borderRadius={"full"}
          p={3}
          py={1}
          mb={4}
        >
          {index + 1}
        </Text>
      )}
      <Text color={color} fontSize="sm" bold mb={2}>
        {title}
      </Text>
      <Text fontSize="xs" color={color}>
        {description}
      </Text>
    </VStack>
  );
}

export default function FeaturedColumnGrp({ data, loading }) {
  const {
    title,
    description,
    features = [],
    bgColor = "#ffffff",
    color = "#000000",
    mediaW = "225",
    mediaH = "24",
    cardW = "284",
    cardH = "243",
  } = data;

  return (
    <VStack
      // borderRadius="lg"
      // mt={{
      //   base: "4",
      //   md: "1",
      // }}
      _light={{
        bg: bgColor,
        color,
      }}
      _dark={{
        bg: bgColor,
        color,
      }}
      space="2"
      pl={{
        md: 16,
        base: 4,
      }}
      py={10}
    >
      <Skeleton.Text isLoaded={loading} w={"25%"}>
        <Text fontSize="xl" bold color={color}>
          {title}
        </Text>
      </Skeleton.Text>
      <Skeleton.Text isLoaded={loading} w={"50%"}>
        <Text fontSize="xs" color={color}>
          {description}
        </Text>
      </Skeleton.Text>
      <ScrollView
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        mx="-6"
        contentContainerStyle={{ flexGrow: 1, justifyContent: "center" }}
      >
        <HStack
          justifyContent="space-between"
          alignItems="center"
          space="2"
          mx="6"
        >
          {features.map((item, index) => {
            return (
              <Pressable key={index}>
                <Card
                  item={item}
                  key={index}
                  index={index}
                  color={color}
                  mediaW={mediaW}
                  mediaH={mediaH}
                  cardW={cardW}
                  cardH={cardH}
                  loading={loading}
                />
              </Pressable>
            );
          })}
        </HStack>
      </ScrollView>
    </VStack>
  );
}
