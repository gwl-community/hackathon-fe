import React from "react";
import { VStack, Box} from "native-base";

export default function TabView({ setTabCoord , ...props}) {
    return (
        <Box onLayout={(e) => {
            setTabCoord(e.nativeEvent.layout.y);
        }} w={"100%"}>
            <VStack>{props?.children}</VStack>
       </Box>
    )
}