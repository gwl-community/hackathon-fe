import React from "react";
import { VStack, HStack, Text, Image, Button, Pressable } from "native-base";

export default function TabListHeader({ tabList, handleChange, selectedTab }) {
    return (
        <HStack>
            {tabList?.map((item) => {
                return (
                    <Pressable mr={"25px"} onPress={() => handleChange(item?.id)} borderBottomColor={"#ff5e63"} borderBottomWidth={selectedTab === item?.id ? 2 : 0} h={"36px"}>
                        <Text color={selectedTab === item?.id ? "#ff5e63" : "#ffffff"} fontWeight={selectedTab === item?.id ? "bold" : "normal"} fontSize={"16px"}>{item?.tabName}</Text>
                    </Pressable>
                )
            })}
        </HStack>
    )
}