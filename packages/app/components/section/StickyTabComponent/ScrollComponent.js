import React, { useRef } from "react";
import { VStack } from "native-base";
import { Animated } from "react-native";

export default function ScrollComponent({handleScroll, ...props }) {
    const scrollViewRef = useRef();
    return (
        <Animated.ScrollView
            onScroll={(e) => handleScroll(e)}
            ref={scrollViewRef}
        >
            <VStack>{props?.children}</VStack>
        </Animated.ScrollView>
    )
}