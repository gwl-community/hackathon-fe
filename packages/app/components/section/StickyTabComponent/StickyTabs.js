import React, { useEffect } from "react";
import { VStack} from "native-base";
import {Animated} from "react-native";

export default function StickyTabs({ showStickyHeader, ...props }) {
    const value = new Animated.Value(0);

    useEffect(() => {
        if (showStickyHeader) {
            Animated.timing(value, {
                toValue: 1,
                duration: 0,
                useNativeDriver: false,
            }).start();
        }
        if (!showStickyHeader) {
            Animated.timing(value, {
                toValue: 0,
                duration: 0,
                useNativeDriver: true,
            }).start();
        }
    }, [showStickyHeader]);

    return (
        <Animated.View
        style={{
            alignSelf: props.align || "center",
            maxWidth: "1640px",
            position: "fixed",
            top: 0,
            width: props?.width || "100%",
            zIndex: 100,
            backgroundColor: props?.color || "#17181a",
            opacity: value,
            marginTop: props?.marginTop || 0,
            height: props?.height || undefined
        }}
    >
     <VStack>{props?.children}</VStack>
    </Animated.View>
    )
}