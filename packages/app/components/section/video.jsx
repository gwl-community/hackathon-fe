import ReactPlayer from "react-player";
import React from "react";
import { height } from "styled-system";
import { HStack } from "native-base";

export default function Video({ url, onEnded, onPause,onStart, onPlay,onProgress, onDuration, }) {
  return (
    <HStack justifyContent={"center"} height={{ base: "250px", md: "500px" }}>
      <ReactPlayer
        url={
          url  || "https://youtu.be/RFsEFNRvX_A"
        }
        controls={true}
        width={"100%"}
        // height='320px'
        height={{ base: "160px", md: "100%" }}
        onEnded={onEnded}
        onPause={onPause}
        onPlay={onPlay}
        onStart={onStart}
        onDuration={onDuration}
        onProgress={onProgress}
        config={{ file: { attributes: { controlsList: 'nodownload' } } }}
      />
    </HStack>
  );
}
