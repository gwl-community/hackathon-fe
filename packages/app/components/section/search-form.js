import { Icon, Input, Skeleton, Text, VStack } from "native-base";

import { MaterialIcons } from "@expo/vector-icons";
import React from "react";
import isEmpty from "lodash.isempty";

export default function SearchForm({ data, loading }) {
  const {
    title,
    description,
    bgColor = "#fcfaf9",
    alignItems = "flex-start",
    justifyContent = "flex-start",
    searchField = {},
    color="#ffffff"
  } = data;
  return (
    <VStack
      space="4"
      bg={bgColor}
      py={16}
      width={{ md: "100%", base: "100%" }}
      //   height={{ md: "100%", base: "100%" }}
      alignItems={alignItems}
      justifyContent={justifyContent}
      // ml={24}
    >
      <Skeleton.Text isLoaded={loading} w={{ md: "55%", base: "100%" }}>
        <Text
          fontSize={{
            md: "3xl",
          }}
          bold
          color={"#ffffff"}
        >
          {title}
        </Text>
      </Skeleton.Text>
      <Skeleton.Text isLoaded={loading} w={{ md: "55%", base: "100%" }}>
        <Text
          fontSize={{
            md: "md",
          }}
          //   bold
          color={"#ffffff"}
          width={"55%"}
        >
          {description}
        </Text>
      </Skeleton.Text>
      {!isEmpty(searchField) && (
        <Skeleton.Text
          isLoaded={loading}
          w={{
            base: "75%",
            md: "50%",
          }}
          h={"54px"}
        >
          <Input
            mt={8}
            w={{
              base: "75%",
              md: "50%",
            }}
            borderRadius={"4px"}
            backgroundColor="#0c62b1"
            color={"rgba(255, 255, 255, 0.5)"}
            borderColor="#167fe0"
            h={"54px"}
            InputLeftElement={
              <Icon
                as={<MaterialIcons name="search" />}
                size={7}
                ml="4"
                color={"white"}
                //   color="muted.400"
              />
            }
            placeholder={searchField.textFieldLabel}
          />
        </Skeleton.Text>
      )}
    </VStack>
  );
}
