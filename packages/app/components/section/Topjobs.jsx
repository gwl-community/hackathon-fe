import {
    VStack,
    Text,
    Image,
    HStack,
    Box,
    View,
    Button,
    ScrollView,
    Flex,
    Center,
    Pressable,
} from "native-base";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import React, { useRef, useState } from "react";
import { SectionSpacingX } from "../../lib/constants";
import { Entypo } from "@expo/vector-icons";

const Jobs = [
    {

        Title: "ReactJS Developer",
        company: 'GoodWorks Alpha',
        description: "GoodWorks Alpha is looking for entry-level developers with core skills in Node, MongoDb, AWS, DSA. Industry experience is a plus. ",
        icon: 'galpha',
        icon2: 'ctc',
        icon3: '100+',
        package: '6-8 LPA'


    },
    {

        Title: "Front End Developer",
        company: 'GoodWorks Alpha',
        description: "GoodWorks Alpha is creating a team of remote Front End Developers with core skills in React / Angular, HTML, CSS.",
        icon: 'galpha',
        icon2: 'ctc',
        icon3: '100+',
        package: '8-12 LPA'

    },
    {

        Title: "MERN Stack Developer",
        company: 'GoodWorks Alpha',
        description: "GoodWorks Alpha is a platform for remote developers for international jobs. Fullstack (MERN) skills needed are Node, React, HTML, CSS.",
        icon: 'galpha',
        icon2: 'ctc',
        icon3: '100+',
        package: '8-16 LPA '

    },
    {

        Title: "NodeJS Developer",
        company: 'GoodWorks Alpha',
        description: "GoodWorks Alpha is looking for entry-level developers with core skills in Node, MongoDb, AWS, DSA. Industry experience is a plus.",
        icon: 'galpha',
        icon2: 'ctc',
        icon3: '100+',
        package: '6-8 LPA'

    },
    {

        Title: "Back End Developer",
        company: 'GoodWorks Alpha',
        description: "GoodWorks Alpha is creating a team of remote Back End Developers with core skills in Node / Java, HTML, CSS.",
        icon: 'galpha',
        icon2: 'ctc',
        icon3: '100+',
        package: '8-12 LPA'

    },



]

export default function TopJobs({ data, isPlacement = false, title, props, bottomClick }) {
   
    const topJobs = data
 
    // topJobs.map((item)=> {
    // })
    const [scrollToEnd, setScrollToEnd] = useState(false);
    const scrollRef = useRef();
    return (
        <Box pl={SectionSpacingX}>

            <VStack bg={"#ffffff"} pt={"80px"} pb={"93px"} >
                <VStack pb={"22px"} width={"90%"}>
                    <Text color={'#262627'} fontSize="16px" fontWeight={"600"} fontFamily={'Nunito Sans'}>
                        <h2 style={{ marginTop: 0, marginBottom: 0 }}>
                            {isPlacement ? title : ''}
                        </h2>
                    </Text>
                </VStack>
                <ScrollView
                    ref={scrollRef}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    pagingEnabled={true}
                >
                    {/* <HStack> */}
                    {topJobs?.map((item, i) => {
                        return (
                            <Box
                                mr={"24px"}
                                bg={"#fff"}
                                ml={'2px'}
                                borderRadius="4px"
                                my={"2px"}
                                style={{ boxShadow: "0 0 6px 0 rgba(0, 0, 0, 0.1)" }}
                                maxW="384px"
                                w={["350px", "384px"]}
                                h={"260px"}
                                pt={"24px"}
                                pb={'31px'}
                                px={'24px'}
                                key={i}
                            >
                                <VStack>
                                    <HStack pb="24px">
                                        <Image size={"60px"} source={{uri : item?.clients?.[0].url}} alt="IconImage" />
                                        <Flex
                                            pl={"24px"}>
                                            <Text
                                                fontSize="14px"
                                                fontWeight="bold"
                                                color="#17181a"
                                                fontFamily={'Nunito Sans'}
                                            >
                                                <h3
                                                    style={{ marginTop: 0, marginBottom: 0 }}>{item?.title}</h3>
                                            </Text>
                                            <Text
                                                fontSize="14px"
                                                pt="4px"
                                                color="#17181a"
                                                opacity="0.5"
                                                fontFamily={'Nunito Sans'}
                                            >
                                                {item?.subTitle}
                                            </Text>
                                        </Flex>
                                    </HStack>
                                    <Text
                                        color={"#17181a"}
                                        textAlign={"left"}
                                        fontSize="14px"
                                        fontWeight="normal"
                                        fontFamily={'Nunito Sans'}
                                        
                                    >
                                        {item?.description}
                                    </Text>
                                </VStack>
                                <HStack pt={"4"} space={6}>
                                    <HStack pt={"1.5"}>
                                        <HStack pt={"0.5"}>
                                            <Image
                                                width={{
                                                    md: "16px",
                                                    base: 5,
                                                }}
                                                height={{
                                                    md: "16px",
                                                    base: 5,
                                                }}
                                                resizeMode="contain"
                                                source={{uri: item?.media[1]?.url}}
                                                alt="double_quote" />

                                        </HStack>
                                        <Text
                                            opacity={'0.8'}
                                            fontSize={'12px'}
                                            color="#17181a"
                                            pl={1}
                                            pt={0.5}>
                                            {item?.ctcTitle}
                                        </Text>
                                    </HStack>
                                    <HStack pt={"1.5"}>
                                        <HStack pt={"0.5"}>
                                            <Image
                                                width={{
                                                    md: "16px",
                                                    base: 5,
                                                }}
                                                height={{
                                                    md: "16px",
                                                    base: 5,
                                                }}
                                                resizeMode="contain"
                                                source={{uri: item?.media[0]?.url}}
                                                alt="double_quote"
                                            />
                                        </HStack>
                                        <Text
                                            opacity={'0.8'}
                                            fontSize={'12px'}
                                            color="#17181a"
                                            pl={1}
                                            pt={0.5}
                                        > {item?.job_title}
                                        </Text>
                                    </HStack>
                                </HStack>
                            </Box>
                        )
                    })}
                    {/* </HStack> */}
                </ScrollView>
                {topJobs?.length > 3 ?
                    <Pressable onPress={() => {
                        scrollRef.current.scrollTo({
                            x: !scrollToEnd ? topJobs?.length * 100 : 0,
                            y: 0,
                            animated: false,
                        })
                        setScrollToEnd(!scrollToEnd);
                    }
                    } position={"absolute"} left={scrollToEnd ? -22 : undefined} right={!scrollToEnd ? 2 : undefined} top={"250px"}>
                        <VStack justifyContent={"center"} alignItems={"center"} w={"48px"} h={"48px"} borderRadius={"30px"} bg={"white"}>
                            {/* <Entypo name={scrollToEnd ? "chevron-small-left" : "chevron-small-right"} size={"24px"} color="#ff5e63" /> */}
                        </VStack>
                    </Pressable>

                    : null}

                <VStack pt={6} pl={"1px"}>
                    <Button
                        w={['304px', "280px"]}
                        variant='raised'
                        height={{ base: '', md: '48px' }}

                        _text={{
                            color: "#ffffff",
                            fontSize: "15px",
                            // fontWeight: "600",
                        }}
                        bg="#ff5e63"
                        _hover={{
                            bg: "error.500",
                        }}
                        onPress={bottomClick}

                    >
                        Take Test To Qualify For These Jobs
                    </Button>
                </VStack>
            </VStack>
        </Box>

    );
}




