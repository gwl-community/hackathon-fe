import React, { useRef, useState, useEffect } from "react";
import {
    Box,
    HStack,
    Text,
    FlatList,
} from 'native-base';
import { Pressable, View } from "react-native"
import Moment from "react-moment"
import ShareLink from "react-linkedin-share-link";
import { Link } from "@mui/material"
import { PinterestShareButton } from "react-share";
import Image from 'next/image';
import {
    SectionSpacingX,
    SectionSpacingY,
} from "../../lib/constants";
const assetUrl = "https://images.netskill.com";
const websiteUrl = "https://blog.netskill.com/"
const externaImageLoader = ({ src }: { src: string }) =>
    `${src}`;
const MediaIcon = ({ src, alt, handleClick }) => {
    return (
        <div onClick={handleClick}>
            <Image
                loader={externaImageLoader}
                alt={"image"}
                width={"28px"}
                height={"28px"}
                layout={"fixed"}
                src={src}
            />
        </div>
    )
}
const getNetskillBlogUrl = (url) => {
    if (url) {
        if (url.includes("dev")) {
            return "https://blog-dev.netskill.com/"
        } else if (url.includes("staging")) {
            return "https://blog-staging.netskill.com/"
        } else {
            return "https://blog.netskill.com/"
        }
    } else {
        return "https://blog.netskill.com/"
    }
}

function RelatedPost() {
    const [inwdith, setInwidth] = useState(0);
    const [show, hide] = useState(false);
    useEffect(() => {
        const newWidth = window?.innerWidth;
        setInwidth(newWidth)
    }, []);
    const [selected, setSelected] = useState(1);
    const [anchorEl, setAnchorEl] = useState(null)
    const [point, setPoint] = useState(0);
    const [blogData, setBlogData] = useState([]);
    const scrollListRef = useRef();
    const slug = "reasons-to-take-up-a-full-stack-developer-course";
    let url = websiteUrl + `${slug}`;
    const open = Boolean(anchorEl);
    const fetchData = async () => {
        fetch('https://cms-blogs.netskill.com/api/articles?populate=*&pagination[page]=1&pagination[pageSize]=5&sort=createdAt:DESC')           //api for the get request
            .then(response => response.json())
            .then(data => {
                setBlogData(data?.data)
            }
            );
    }
    useEffect(() => {
        fetchData();
    }, [])
    if (typeof window !== "undefined") {
        url = getNetskillBlogUrl(window.origin) + `${slug}`
    }
    const handleFacebook = (url) => {
        if (typeof window !== "undefined")
            window.open(
                `https://www.facebook.com/sharer/sharer.php?url=${url}`,
                "_blank"
            )
    }
    const handleTwitter = (url) => {
        if (typeof window !== "undefined")
            window.open(`https://twitter.com/intent/tweet?url=${url}`, "_blank")
    }
    const handleMouseOver = (id) => {
        setAnchorEl(id)
        // setAnchorEl(event.currentTarget)
        // setAnchorEl(anchorEl ? null : event.currentTarget)
    }
    const handleMouseLeave = () => {
        setAnchorEl(null);

    }
    const handleScroll = (e) => {
        if (e?.nativeEvent?.contentOffset?.x > 600) {
            setSelected(2);
        }
        else setSelected(1);
    }
    const showList = () => {
        return (
            <FlatList
            onScroll={handleScroll}
            ref={scrollListRef}
            showsHorizontalScrollIndicator={false}
            horizontal
            style={{ width: "100%" }}
            data={blogData}
            renderItem={(data) => {
                let item = data?.item;
                return (
                    <Link href={`https://blog.netskill.com/${item?.attributes?.slug}`} underline="none" key={slug} target={"_blank"}>
                        <View onMouseEnter={() => setPoint(item?.id)} onMouseLeave={() => setPoint(0)} style={{ borderWidth: 1, borderColor: "rgba(116, 116, 118, 0.1)", width: "370px", marginRight: "30px", borderRadius: "4px" }}>
                            <Box flex={0.56}>
                                <Image
                                    loader={externaImageLoader}
                                    alt={"image"}
                                    width={"370px"}
                                    height={"245px"}
                                    layout={"fixed"}
                                    src={item?.attributes?.image?.data?.attributes?.url}
                                />
                            </Box>
                            <View style={{ paddingTop: "20px", paddingHorizontal: "28px", flex: 0.44 }}>
                                <Text fontSize={"28px"} fontWeight={"600"} color={point == item?.id ? "red.500" : "#17181A"} fontFamily={'Nunito Sans'} pb={"24px"} borderBottomWidth={"1px"} borderBottomColor={"#F2F2F2"}>{item?.attributes?.description}</Text>
                                <HStack justifyContent={"space-between"} pb={"24px"} pt={"16px"}>

                                    <Text fontSize={"16px"} fontWeight={"400"} color={"#747476"} fontFamily={'Nunito Sans'} ><Moment format="MMMM D, YYYY">{item?.attributes?.createdAt}</Moment></Text>
                                    <View
                                        onMouseEnter={() => handleMouseOver(item?.id)}
                                        onMouseLeave={handleMouseLeave}
                                        style={{ alignSelf: "center" }}
                                    >
                                        <Image
                                            loader={externaImageLoader}
                                            alt={"image"}
                                            width={"23px"}
                                            height={"23px"}
                                            layout={"fixed"}
                                            src={"https://images.netskill.com/frontend/netskill/blog/share.webp"}
                                        />
                                        {anchorEl === item?.id ?
                                            <Box
                                                padding={"4px"}
                                                borderRadius="4px"
                                                backgroundColor={"white"}
                                                position={"absolute"}
                                                right={"-40px"}
                                                bottom={"15px"}
                                            >
                                                <Box
                                                    style={{
                                                        display: "flex",
                                                        justifyContent: "center",
                                                        alignItems: "center",
                                                        // marginBottom: "24px",
                                                    }}
                                                >
                                                    <HStack
                                                        style={{
                                                            display: "flex",
                                                            justifyContent: "space-between",
                                                            width: "120px",
                                                        }}
                                                    >
                                                        <MediaIcon
                                                            src={"https://images.netskill.com/twitter2.webp"}
                                                            alt={"twitter"}
                                                            handleClick={() => handleTwitter(item?.attributes?.slug)}
                                                        />

                                                        <MediaIcon
                                                            src={"https://images.netskill.com/facebook2.webp"}
                                                            alt={"facebook"}
                                                            handleClick={() => handleFacebook(item?.attributes?.slug)}
                                                        />
                                                        <PinterestShareButton
                                                            url={`https://blog.netskill.com/${item?.attributes?.slug}`}
                                                            media={`https://blog.netskill.com/${item?.attributes?.slug}`}
                                                            description=""
                                                        >
                                                            <MediaIcon
                                                                src={
                                                                    `${assetUrl}/frontend/netskill/blog/pinterest2.webp`}
                                                                alt={"pinterest"}
                                                            />
                                                        </PinterestShareButton>
                                                        <ShareLink link={`https://blog.netskill.com/${item?.attributes?.slug}`}>
                                                            {(link) => (
                                                                <a href={link} rel="noreferrer" target="_blank">
                                                                    <MediaIcon src={"https://images.netskill.com/linkedin2.webp"} alt={"linkedin"} />
                                                                </a>
                                                            )}
                                                        </ShareLink>
                                                    </HStack>
                                                </Box>
                                            </Box>
                                            : null}

                                    </View>
                                </HStack>
                            </View>
                        </View>
                    </Link>
                )
            }}
        />
        )
    }
    return (
        <Box px={SectionSpacingX} bg={"white"} w="100%" pb={SectionSpacingY}>
            <Pressable onPress={()=>hide(!show)}>
            <HStack bg={"white"} w={"100%"}
                pb={"30px"}
            >
                <Text mr={10} fontWeight={"700"} color={"#17181A"} fontSize={"24px"} pb={"25px"} fontFamily={'Nunito Sans'}>Recent News & Insights</Text>
                {inwdith < 700  && 
                <Box pt={3}>
                  <Image
                    loader={externaImageLoader}
                    alt={"image"}
                    width={"22px"}
                    height={"22px"}
                    layout={"fixed"}
                    src={show && inwdith < 700 ? 'https://images.netskill.com/frontend/upArrow.webp' : 'https://images.netskill.com/frontend/downArrow.webp'}
                />
                </Box>}
            </HStack>
            </Pressable>
            {blogData && blogData.length > 0 && 
               inwdith > 700 ?
               showList() : show ? showList() : null
               }
            {blogData && blogData.length > 0 &&   inwdith > 700 ?
                <HStack justifyContent={"center"} alignItems={"center"}>
                    <Text fontWeight={selected == 1 ? "900" : "300"} marginTop={selected == 1 ? 0 : 0.5} color={"#17181A"}>___ </Text>
                    <Text fontWeight={selected == 2 ? "900" : "300"} marginTop={selected == 2 ? 0 : 0.5} color={"#17181A"}>  ___</Text>
                </HStack>  : show ? 
                                <HStack justifyContent={"center"} alignItems={"center"}>
                                <Text fontWeight={selected == 1 ? "900" : "300"} marginTop={selected == 1 ? 0 : 0.5} color={"#17181A"}>___ </Text>
                                <Text fontWeight={selected == 2 ? "900" : "300"} marginTop={selected == 2 ? 0 : 0.5} color={"#17181A"}>  ___</Text>
                            </HStack> : null
            }
        </Box>
    )
}
export default RelatedPost;