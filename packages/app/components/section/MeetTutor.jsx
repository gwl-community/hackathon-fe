import {
  VStack,
  Text,
  // Image,
  HStack,
  Box,
  FlatList,
  ScrollView,
} from "native-base";
import React, {useState, useEffect} from "react";
import Image from 'next/image';
import { Pressable } from "react-native";
const externaImageLoader = ({ src }: { src: string }) =>
  `${src}`;
function MeetTutor({ data, title, subtitle, meetTurorData, isMeetTutor = false }) {
  const [inwdith, setInwidth] = useState(0);
  const [show, hide] = useState(false);
  useEffect(() => {
      const newWidth = window?.innerWidth;
      setInwidth(newWidth)
  }, []);
  const showList = () => {
    return (
      <ScrollView
      contentContainerStyle={{ width: "100%" }}
      showsHorizontalScrollIndicator={false}
      horizontal={{
        base: true,
        md: false,
      }}
    >
      <HStack w={{ md: "100%" }} pt={10}>
        <FlatList
          style={{ width: "100%" }}
          contentContainerStyle={{
            // alignItems: "center",
            justifyContent: "space-between",
            alignContent: "center",
          }}
          data={isMeetTutor ? meetTurorData : data}
          // data={data}
          numColumns={3}
          renderItem={renderItem}
          keyExtractor={(item) => item.id}
        />
      </HStack>
    </ScrollView>
    )
  }
  return (
    <VStack justifyContent="center">
      <Pressable onPress={()=>hide(!show)}>
      <HStack>
      <Text
        fontSize={{
          base: "14px",
          md: "15px",
        }}
        fontWeight="700"
        fontFamily="Nunito Sans"
        color="#17181a"
        mr="6px"
      >
        <h2>
          {title}

        </h2>
     
      </Text>
      {inwdith < 700  && 
                <Box pt={4}>
                  <Image
                    loader={externaImageLoader}
                    alt={"image"}
                    width={"22px"}
                    height={"22px"}
                    layout={"fixed"}
                    src={show && inwdith < 700 ? 'https://images.netskill.com/frontend/upArrow.webp' : 'https://images.netskill.com/frontend/downArrow.webp'}
                />
                </Box>}
                </HStack>
                </Pressable>
      <Text
        fontSize={{
          base: 15,
          md: "16px",
        }}
        color="#17181A"
        fontFamily="Nunito Sans"
        opacity={0.6}
        // pt={2}
        mb={2}
      >
        {subtitle}
      </Text>
      {(inwdith < 700 && show) ?
     showList() : (inwdith > 750) ?   showList()  :null }
    </VStack>
  );
}
const renderItem = (val) => {
  const data = val?.item;
  return (
    <Box m={2} borderRadius="4px" w={{ base: 330, md: "32%" }}>
      <HStack>
        <Image
          loader={externaImageLoader}
          alt={"image"}
          width={"110px"}
          height={"130px"}
          layout={"fixed"}
          src={data?.media?.[0]?.url ?? data?.image}
        />
        <Box
          bg="#F4F4F4"
          p={2}
          width={"70%"}
          borderTopRightRadius="4px"
          borderBottomRightRadius="4px"
        >
          <VStack
            pl={"6px"}
            pt="4px"
            justifyContent={"space-between"}
            alignItems="space-between"
            h="100%"
          >
            <VStack>
              <Text
                fontSize={{ base: "15px", md: "16px" }}
                color="#000000"
                fontWeight="600"
                fontFamily="Nunito Sans"
                pb="4px"
              >
                {data?.title ?? data.name}
              </Text>
              <Text
                fontSize={{ base: "13px", md: "14px" }}
                color="#17181A"
                fontFamily="Nunito Sans"
                opacity="0.7"
              >
                {data?.subTitle ?? data.position}
              </Text>
            </VStack>
            <HStack pb="12px" pt="6px">
            {data?.company ? data?.company?.map((logo) => (
                <Box mr={"10px"} h={"30px"}>
                  <Image
                    loader={externaImageLoader}
                    alt={"image"}
                    width={logo?.width||"65px"}
                    height={logo?.height || "28px"}
                    layout={"fixed"}
                    src={logo?.url}
                  />
                </Box>
              )) :
              <Box mr={"10px"} h={"30px"}>
              <Image
                loader={externaImageLoader}
                alt={"image"}
                width={"65px"}
                height={"25px"}
                layout={"fixed"}
                src={logo?.url}
              />
            </Box>
            }
            </HStack>
          </VStack>
        </Box>
      </HStack>
    </Box>
  );
};
export default MeetTutor;
