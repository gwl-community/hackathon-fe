import React from "react";
import { HStack, Image, Text, VStack } from "native-base";
import { SectionSpacingX, SectionSpacingY } from "../../lib/constants";

export default function Tools({ image, title, subtitle, reverse = false }) {
  return (
    <VStack
      bg="#17181a"
      zIndex={2}
      w="100%"
      py={{ base: 4, md: 8 }}
      pr={
        reverse ? { base: "12px", md: 0 } : { ...SectionSpacingX, base: "12px" }
      }
      pl={
        !reverse
          ? { base: "12px", md: 0 }
          : { ...SectionSpacingX, base: "12px" }
      }
    >
      <HStack
        justifyContent={"space-between"}
        alignItems="center"
        direction={{
          base: "column-reverse",
          md: reverse ? "row-reverse" : "row",
        }}
      >
        <Image
          key={title}
          width={{
            md: "48%",
            base: "100%",
          }}
          height={{
            md: "500px",
            base: "250px",
          }}
          resizeMode="contain"
          source={{
            uri: image,
          }}
          alt={title}
        />
        <VStack
          pl={{ md: reverse ? 0 : "24px" }}
          pr={{ md: reverse ? "24px" : 0 }}
          maxW={{ md: "460" }}
        >
          <Text
            width={"100%"}
            lineHeight={"44px"}
            fontSize={{
              base: "22px",
              md: "32px",
            }}
            textAlign={{ base: "center", md: "start" }}
            color="#ffffff"
            fontWeight="600"
            maxW={"580px"}
            fontFamily="body"
            mb={"12px"}
          >
            {title}
          </Text>
          <Text
            width={"100%"}
            fontSize={{
              base: "16px",
              md: "16px",
            }}
            textAlign={{ base: "center", md: "start" }}
            color="#B0B5C0"
            fontWeight="600"
            lineHeight="22px"
            fontFamily="body"
            maxW={"580px"}
            mb={{ base: "16px", md: 0 }}
          >
            {subtitle}
          </Text>
        </VStack>
      </HStack>
    </VStack>
  );
}
