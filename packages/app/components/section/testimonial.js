import {
  Box,
  Button,
  Center,
  HStack,
  Heading,
  Image,
  Pressable,
  ScrollView,
  Stack,
  Text,
  VStack,
} from "native-base";

import { API_DOMAIN } from "../../config/constants";
import React from "react";
import get from "lodash.get";

const Card = ({ item }) => {
  const {
    authorName,
    authorTitle,
    text,
    picture = {},
    bgColor = "#ffffff",
  } = item;
  const url = get(picture, "data.attributes.url", "");
  const alternativeText = get(picture, "data.attributes.alternativeText", "");

  return (
    <Box alignItems="center">
      <Box
        maxW="350"
        rounded="lg"
        overflow="hidden"
        borderColor="coolGray.200"
        borderWidth="1"
        _dark={{
          borderColor: "coolGray.600",
          backgroundColor: bgColor,
        }}
        _web={{
          shadow: 2,
          borderWidth: 0,
        }}
        _light={{
          backgroundColor: bgColor,
        }}
        height="400"
        // alignContent={"center"}
        // justifyContent={"center"}
        py={8}
      >
        <Box>
          <Center>
            <Image
              size={100}
              resizeMode={"contain"}
              borderRadius={100}
              source={{
                uri: `${url}`,
              }}
              alt={"image"}
            />
          </Center>
        </Box>

        <Stack p="4" space={3}>
          <Stack space={2} alignItems="center">
            <Heading fontSize="md" ml="-1" bold>
              {authorName}
            </Heading>
            <Text
              // color="#262627"
          
              // _dark={{
              //   color: "warmGray.200",
              // }}
              // fontWeight="400"
              fontSize="xs"
            >
              <Center>{authorTitle}</Center>
            </Text>
          </Stack>

          <Text fontSize="sm">{`"${text}"`}</Text>
        </Stack>
      </Box>
    </Box>
  );
};

export default function FeaturedColumnGrp({ data }) {
  const { title, description, testimonials = [], bgColor = "#ffffff" } = data;

  return (
    <VStack
      // borderRadius="lg"
      mt={{
        base: "4",
        md: "1",
      }}
      _light={{
        bg: {
          md: bgColor,
        },
      }}
      _dark={{
        bg: bgColor,
      }}
      space="2"
      pl={{
        md: 16,
        base: 4,
      }}
      py={10}
    >
      <Text
        fontSize="2xl"
        // _light={{
        //   color: "coolGray.800",
        // }}
        // _dark={{
        //   color: "coolGray.50",
        // }}
        // fontWeight="medium"
        bold
      >
        {title}
      </Text>
      <Text
        // color="coolGray.600"
        // _dark={{
        //   color: "warmGray.200",
        // }}
        // fontWeight="400"
        fontSize="xs"
      >
        {description}
      </Text>

      <ScrollView
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        mx="-6"
        contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}
      >
        <HStack
          justifyContent="center"
          alignItems="center"
          space="3"
          mx="6"
          py="8"
        >
          {testimonials.map((item, index) => {
            return (
              <Pressable key={index}>
                <Card item={item} key={index} />
              </Pressable>
            );
          })}
        </HStack>
      </ScrollView>
    </VStack>
  );
}
