import {
    Box,
    Button,
    HStack,
    Image,
    Pressable,
    ScrollView,
    Text,
    VStack,
} from "native-base";

import { API_DOMAIN } from "../../config/constants";
import React from "react";
import get from "lodash.get";

function Card({ item }) {
    const {
      title,
      description,
      isCard,
      bgColor,
      alignContent,
      media = {},
      mediaW="216",
      mediaH="24"
    } = item;
    const url = get(media, "data.attributes.url", "");
    const alternativeText = get(media, "data.attributes.alternativeText", "");
    return (
      <VStack
        borderRadius="lg"
        width="284"
        height="194"
        _light={{
          bg: bgColor,
        }}
        _dark={{
          bg: bgColor,
        }}
      >
        {url && (
          <Image
            width={parseInt(mediaW)}
            height={parseInt(mediaH)}
            source={{uri:`${url}`}}
            alt={"image"}
          />
        )}
        <HStack
          px="3"
          pt="3"
          justifyContent="space-between"
          alignItems="center"
        >
          <VStack>
            <Text
              fontSize="sm"
              bold
            >
              {title}
            </Text>
            <Text
              fontSize="xs"
              width={{
                base: "33%",
                md: "40%",
              }}
            >
              {description}
            </Text>
          </VStack>
        </HStack>
      </VStack>
    );
  }
  
  export default function Footer({ data }) {
    const { title, description, features = [], bgColor = "#000000" } = data;
  
    return (
      <VStack
        _light={{
          bg: bgColor,
        }}
        _dark={{
          bg: bgColor,
        }}
        space="2"
        pl={{
          md: 16,
          base: 4,
        }}
        py={10}
      >
        <Text
          fontSize="xl"
          bold
        >
          {title}
        </Text>
        <Text
          fontSize="xs"
        >
          {description}
        </Text>
        <ScrollView
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          mx="-6"
        >
          <HStack
            justifyContent="space-between"
            alignItems="center"
            space="2"
            mx="6"
          >
            {features.map((item, index) => {
              return (
                <Pressable key={index}>
                  <Card item={item} key={index} />
                </Pressable>
              );
            })}
          </HStack>
        </ScrollView>
      </VStack>
    );
  }
  