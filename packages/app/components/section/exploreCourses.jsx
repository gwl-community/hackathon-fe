import {
  VStack,
  Text,
  Image,
  HStack,
  Box,
  View,
  Button,
  ScrollView,
  FlatList,
} from "native-base";
import React, { useEffect, useState } from "react";
import { getToken } from "../../lib/utils";
import { Link } from 'solito/link';
import {getCalclulatedDate} from "../../lib/utils";
const rightArrow = <Image source={{ uri: 'https://images.netskill.com/frontend/Arrow.svg' }} size={5} />
const rightArrowDark = <Image source={{ uri: 'https://images.netskill.com/frontend/arrow_right2.svg' }} size={5} />
function ExploreCourses({
  data,
  title,
  subtitle,
  nav,
  handleViewAll,
  hideViewAll = false,
  isHomeScreen = false,
  courseConent,
}) {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [inWidth, setInwidth] = useState();
  const checkIsLoggedIn = async () => {
    const token = await getToken();
    if (token) {
      setIsLoggedIn(true);
    }
  };
  useEffect(() => {
    const newWidth = window?.innerWidth;
    setInwidth(newWidth)
  }, []);
  useEffect(() => {
    checkIsLoggedIn();
  }, []);

  const renderItem = (val) => {
    const data = val?.item;


    return (
      <Box mb={10} m={2} borderRadius="4px" w={{ base: 330, md: "32%" }}>
        <Box
          key={data.id}
          shadow={1}
          borderRadius="4px"
          w={"100%"}
          // minW={"300"}
          maxW={"100%"}
        >
          <VStack w="100%">
            <View>
              <Image
                key={data.id}
                // borderTopRadius="lg"
                width={{
                  md: "100%",
                  base: "100%",
                }}
                height={{
                  md: "180px",
                  base: 200,
                }}
                resizeMode="cover"
                source={isHomeScreen ? data?.media?.[0].url : data?.image}
                alt="course_image"
                position="relative"
                borderTopRightRadius="4px"
                borderTopLeftRadius="4px"
              />
              <Box
                position="absolute"
                bg="#595FFF"
                top={4}
                p={2}
                zIndex={10}
                width={{ base: "35%", md: "40%" }}
                maxW="136px"
                borderTopRightRadius="4px"
                borderBottomRightRadius="4px"
              >
                <HStack>
                  <Image
                    width={{
                      md: "20%",
                      base: 5,
                    }}
                    height={{
                      md: "20px",
                      base: 5,
                    }}
                    resizeMode="contain"
                    source={{ uri: 'https://images.netskill.com/frontend/calender.webp' }}
                    alt="calendar"
                  />
                  <VStack pl={{ base: "6px", md: "6px" }}>
                    <Text fontSize={{ base: 11, md: "12px" }} fontFamily="Nunito Sans">
                      {isHomeScreen ? data.badgeTitle : 'Next Batjch'}
                    </Text>
                    <Text
                      fontFamily="Nunito Sans"
                      fontSize={{ base: 11, md: "12px" }}
                      fontWeight="800"
                    >
                      {isHomeScreen ? getCalclulatedDate(data?.badgeDescription) : getCalclulatedDate(data?.nextBatch)}
                    </Text>
                  </VStack>
                </HStack>
              </Box>
            </View>
            <Box
              p={{ base: "12px", md: "16px" }}
              bg="#1D1E21"
              key={data.title}
              borderBottomRightRadius="4px"
              borderBottomLeftRadius="4px"
            >
              <Text
                fontSize={{ base: "9.5px", md: "13.3px" }}
                fontWeight="500"
                fontFamily="Nunito Sans"
                lineHeight={"27px"}
                pb={{ base: '3px', md: "6px" }}
              //pb="6px"
              >
                <h2 style={{ marginBottom: 'auto', marginTop: 'auto', color: 'rgb(255, 255, 255)' }}>{isHomeScreen ? data.courseName : data.title}</h2>
              </Text>
              {(isHomeScreen ? courseConent : data?.details)?.map((details, index) => (
                <HStack
                  pb={index == 0 ? "18px" : "9px"}
                  pt='8px'
                  //pt={{base:'8px',md:'8px'}}
                  justifyContent={"flex-start"}
                  space={0}
                >
                  <Image
                    width={{
                      md: index == 0
                        ? "24px"
                        : index == 2
                          ? "16px"
                          : "18px",
                      base: 5,
                    }}
                    height={{
                      md: index == 0
                        ? "24px"
                        : index == 2
                          ? "16px"
                          : "18px",
                      base: 5,
                    }}
                    resizeMode="contain"
                    source={isHomeScreen ? details?.media?.[0]?.url : details.image}
                    textAlign="start"
                    alignSelf={"self-start"}
                    alt="details_image"
                    pl={0}
                  />
                  <Text
                    fontSize={{
                      base: index == 0 ? "14px" : "12px",
                      md: index == 0 ? "16px" : "12px",
                    }}
                    fontFamily="Nunito Sans"
                    pl={"8px"}
                  >
                    {details?.title}
                  </Text>
                </HStack>
              ))}
              <Image
                borderTopRadius="lg"
                width={{
                  md: "100%",
                  base: '100%',
                }}
                height={{
                  md: "20px",
                  base: 4,
                }}
                resizeMode="contain"
                source={{ uri: 'https://images.netskill.com/frontend/ractLine.webp.webp' }}
                alt="divider"
              />
              <HStack
                // space={2}
                pt={{ base: "4px", md: "12px" }}
                justifyContent="space-between"
              >
                <HStack pr={{ base: 0, md: "12px" }}>
                  <VStack alignItems={"center"}>
                    <Text
                      fontSize={{ base: 14, md: "16px" }}
                      fontWeight="600"
                      fontFamily="Nunito Sans"
                    >
                      {isHomeScreen ? data?.jobsTitle : data?.jobs}
                    </Text>
                    <Text
                      fontSize={{ base: 11, md: "12px" }}
                      color="#B0B5C0"
                      fontWeight={"500"}
                      fontFamily="Nunito Sans"
                      pt={"2px"}
                    >
                      {isHomeScreen ? data.jobsSubTitle : 'Jobs'}
                    </Text>
                  </VStack>
                  <Image
                    borderTopRadius="lg"
                    width={{
                      md:10,
                      base: 10,
                    }}
                    height={{
                      md: "100%",
                      base: 10,
                    }}
                    resizeMode="contain"
                    source={{ uri: 'https://images.netskill.com/frontend/rectVertical.webp.webp' }}
                    alt="vertical_divider"
                  />
                  <VStack  alignItems={"center"}>
                    <Text
                      fontSize={{ base: 14, md: "16px" }}
                      fontFamily="Nunito Sans"
                      fontWeight="600"
                    >
                      {isHomeScreen ? data.ctcTitle : data.maxCtc}
                    </Text>
                    <Text
                      fontSize={{ base: 11, md: "12px" }}
                      color="#B0B5C0"
                      fontWeight={"500"}
                      fontFamily="Nunito Sans"
                      pt={"2px"}
                    >
                      {isHomeScreen ? data.ctcSubTitle : 'Max CTC'}
                    </Text>
                  </VStack>
                </HStack>
                  <Box>
                    {/* <Link isExternal={true}
                      href={`https://www.netskill.com/courses`}> */}
                      <Button
                        // minW={"140"}
                        py="2.5"
                        width={{ base: "100%", md: "100%" }}
                        // size="md"
                        bg="#ff5e63"
                        variant="raised"
                        _hover={{
                          bg: "error.500",
                        }}
                        onPress={()=>window.open("https://www.netskill.com/courses")}
                      >
                        <Text
                          fontSize={{ base: 13, md: "14px" }}
                          fontFamily="Nunito Sans"
                          fontWeight={{ base: "500", md: "600" }}
                          px={3}
                        >
                          {isHomeScreen ? data.buttonText : 'View Courses'}
                        </Text>
                      </Button>

                    {/* </Link> */}


                  </Box>

              </HStack>
            </Box>
          </VStack>
        </Box>
      </Box>
    );
  };
  return (
    <VStack justifyContent="center" w="100%">
      <Text
        fontSize={{
          base: 16,
          md: "24px",
        }}
        fontFamily="Nunito Sans"
        fontWeight="700"
      >
        {title}
      </Text>
      <HStack
        justifyContent={{ base: "center", md: "space-between" }}
        flexDirection={{ base: "column", md: "row" }}
      >
        <Text
          fontSize={{
            base: 14,
            md: "16px",
          }}
          color="#B0B5C0"
          pt={2}
          mb={2}
          fontFamily="Nunito Sans"
          fontWeight="600"
        >
          {subtitle}
        </Text>

        {!hideViewAll && (
          <Link href={handleViewAll} >
            <Button
              endIcon={rightArrow}
              borderColor={"white"}
              borderWidth="1"
              h="40px"
              w="170px"
              variant="raised"
              alignSelf={"end"}
              //onPress={handleViewAll}
              justifyContent="center"
              // py="4.5"
              px="5"
              mr="3"
              _text={{
                fontSize: "14px",
                fontFamily: "Nunito Sans",
                fontWeight: "600",
                color: "#ffffff",
              }}
              _hover={{
                bg: "#ffffff",
                endIcon : rightArrowDark,
                _text: {
                  color: "#17181A",
                  fontWeight: "600",
                  fontSize: "14px",
                  fontFamily: "Nunito Sans",
                },
                
              }}
            >
              {"View all Courses    "}
            </Button>
          </Link>
        )}
      </HStack>
      <ScrollView
        contentContainerStyle={{ width: inWidth > 700 ? "100%" : null }}
        showsHorizontalScrollIndicator={false}
        horizontal={{
          base: true,
          md: false,
        }}
      >
        <HStack w="100%" pt={{ base: 4, md: 10 }}>
          <FlatList
            style={{ width: "100%" }}
            contentContainerStyle={{
              justifyContent: "space-between",
              alignContent: "center",
            }}
            data={data}
            numColumns={3}
            renderItem={renderItem}
            keyExtractor={(item) => item.id}
          />
        </HStack>
      </ScrollView>
    </VStack>
  );
}

export default ExploreCourses;
