import { Text, Image, HStack } from "native-base";
import { Pressable } from "react-native";
import React, { useEffect, useState } from "react";
const DiscussionAction = ({ itemID, image, text, textColor, upvoteClicked, downvoteClicked, upvote, reply, follow, setShowReply, setItemIdNo, followClicked }) => {
  const showReplyPage = (itemID) => {
    setItemIdNo(itemID);
    setShowReply(true);
  }
  return (
    <Pressable
      onPress={() => { upvote == true && upvoteClicked ? upvoteClicked(itemID) : upvote == false && downvoteClicked ? downvoteClicked(itemID) : reply ? showReplyPage(itemID) : followClicked ? followClicked(itemID) : null }
      }
    >
      <HStack alignItems={"center"} pr={{ base: "10px", md: "24px" }}>
        <Image
          width={{
            md: "20px",
            base: "18px",
          }}
          height={{
            md: "20px",
            base: "18px",
          }}
          source={image}
          alt="Badge"
        />
        {text && (
          <Text
            color={textColor || "#17181A"}
            fontsize={{ base: "13px", md: "14px" }}
            fontFamily={"body"}
            lineHeight="19.1px"
            pl={"4px"}
            fontWeight={400}
          >
            {text}
          </Text>
        )}
      </HStack>
    </Pressable>
  );
};

export default function DiscussionFooter({
  item,
  hideReply = false,
  hideResponse = false,
  upvoteClicked,
  downvoteClicked,
  setShowReply,
  setItemIdNo,
  statusUpdate,
  followClicked,
}) {

  return (
    <HStack mt={"18px"} ml={{base:-35, md: undefined}}>
      {statusUpdate?.upvoted?.includes(item?.id) && (
        <DiscussionAction itemID={item?.id} upvoteClicked={upvoteClicked} upvote={true}
          image={"https://images.netskill.com/tools/Upvote.svg"}
          text={item?.upvotes}
        />)}
      {!statusUpdate?.upvoted?.includes(item?.id)  && (
        <DiscussionAction itemID={item?.id} upvoteClicked={upvoteClicked} upvote={true}
          image={"https://images.netskill.com/frontend/netskill/home/upvote.webp"}
          text={item?.upvotes}
        />
      )}
      {statusUpdate?.downvoted?.includes(item?.id)  && (
        <DiscussionAction itemID={item?.id} downvoteClicked={downvoteClicked} upvote={false}
          image={"https://images.netskill.com/frontend/netskill/home/downvote_active.webp"}
          text={item?.downvotes}
        />)}
      {!statusUpdate?.downvoted?.includes(item?.id) && (
        <DiscussionAction itemID={item?.id} downvoteClicked={downvoteClicked} upvote={false}
          image={"https://images.netskill.com/frontend/netskill/home/downvote.webp"}
          text={item?.downvotes}
        />)}
      {!hideReply && (
        <DiscussionAction itemID={item?.id} reply={true} setShowReply={setShowReply} setItemIdNo={setItemIdNo}
          image={
            item?.isReplied
              ? "https://images.netskill.com/frontend/netskill/home/reply_active.webp"
              : "https://images.netskill.com/frontend/netskill/home/reply.webp"
          }
          text={item?.reply + " Replies"}
        />
      )}
          {!hideResponse && statusUpdate?.following?.includes(item?.id) && (
          <DiscussionAction itemID={item?.id} follow={true} followClicked={followClicked}
            image={"https://images.netskill.com/frontend/netskill/home/following_active.webp"}
            text={"Following"}
            textColor={"#FF5E63"}
          />
          ) 
          }
          {!hideResponse && !statusUpdate?.following?.includes(item?.id) &&  (
          <DiscussionAction itemID={item?.id} follow={true} followClicked={followClicked}
            image={"https://images.netskill.com/frontend/netskill/home/following.webp"}
            text={"Follow Response"}
          />
          )}
      
    </HStack>
  );
}
