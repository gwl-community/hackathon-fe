import React, { useEffect, useState } from "react";
import { Text, HStack, VStack, Box, Pressable } from "native-base";
import parse from 'html-react-parser';
import moment from "moment";
import DiscussionFooter from "./DiscussionFooter";
import { dot } from "app/features/CoursePreview";
import api from "../../../lib/api";
import axios from "axios";
import { baseURL } from "../../../lib/api";
import { getToken } from "../../../lib/utils";

export const NameImage = ({ text , bgColor = "#e8e8e9" }) => {
  const initials = text
  ? text
    .match(/(\b\S)?/g)
    .join("")
    .match(/(^\S|\S$)?/g)
    .join("")
    .toUpperCase()
  : null;
  return (
    <Box
      bg={bgColor}
      borderRadius="32px"
      height={"32px"}
      width="32px"
      p="4px"
      // opacity={0.1}
      alignItems="center"
      textAlign="center"
    >
      <Text
        lineHeight={"16.37px"}
        fontWeight={700}
        fontSize="12px"
        color="#17181A"
        mt={"4px"}
        alignSelf={"center"}
        textAlign="center"
        fontFamily={"body"}
      >
        {initials}
      </Text>
    </Box>
  );
};

export default function DiscussionContent({
  item,
  index,
  commentVisibility,
  toggleCommentVisibility,
  openDiscussionDetail,
  nameImageBackground,
  setShowReply,
  setItemIdNo,
  addFilter,
  getPaginationData,
  statusUpdate,
  getcurrentCardStatus,
  cmsBaseUrl
}) {

  // useEffect(() => {
  //   getcurrentCardStatus();
  // }, [])

  const upvoteClicked = async (userID) => {
    const token = await getToken();
    const config = {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`
      }
    };
    axios.get(`${cmsBaseUrl}api/discussion/votes/${userID}?type=upvote`, config)
    .then(res => {
      addFilter();
      getPaginationData();
      getcurrentCardStatus();
    }
    )
    .catch(err => console.log(err))
  };
  const downvoteClicked = async (userID) => {
    const token = await getToken();
    const config = {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`
      }
    };
    axios.get(`${cmsBaseUrl}api/discussion/votes/${userID}?type=downvote`, config)
      .then(res => {
        addFilter();
        getPaginationData();
        getcurrentCardStatus();
      }
      )
      .catch(err => console.log(err))
  }
  const followClicked = async (userID) => {
    const token = await getToken();
    const config = {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`
      }
    };
    axios.get(`${cmsBaseUrl}api/discussion/votes/${userID}?type=follow`, config)
      .then(res => {
        addFilter();
          getPaginationData();
        getcurrentCardStatus();
      }
      )
      .catch(err => console.log(err))
  }
  const showReplyPage = (itemID) => {
    setItemIdNo(itemID);
    setShowReply(true);
  }
  return (
    <Pressable onPress={()=>showReplyPage(item?.id)}>
    <Box
      key={index}
      borderColor={"#F4F4F4"}
      borderRadius="4px"
      borderWidth={"1px"}
      p={{ base: "12px", md: "24px" }}
      my={{ base: "4px", md: "8px" }}
      width="100%"
      bg="#F4F4F4"
    >
      <HStack width="90%">
        <NameImage text={item?.user?.username || "S"} bgColor={nameImageBackground} />
        <VStack
          ml={{ base: "8px", md: "16px" }}
          mr={{ base: "4px" }}
          width="100%"
        >
          <Pressable onPress={() => openDiscussionDetail()}>
            <Text
              lineHeight={"24.55px"}
              fontWeight={700}
              fontSize="18px"
              color="#17181A"
              fontFamily={"body"}
            >
              {item?.title}
            </Text>
          </Pressable>
          <HStack alignItems="center" mt="4px" mb="12px">
            {item?.user?.username ?
            <Text
              lineHeight={"16px"}
              fontWeight={600}
              fontSize="12px"
              color="#595FFF"
              fontFamily={"body"}
            >
              {item?.user?.username}
            </Text> : null}
            {/* {dot("#b0b5c0")} */}
            <Text
              lineHeight={"16px"}
              fontWeight={600}
              fontSize="12px"
              color="#595FFF"
              fontFamily={"body"}
              ml={item?.user?.username ? ["5px","25px"] : null}
            >
              {'Lesson '}{item?.lesson_number}
            </Text>
            {/* {dot("#b0b5c0")} */}

            <Text
              lineHeight={"16px"}
              fontWeight={600}
              fontSize="12px"
              color="#595FFF"
              fontFamily={"body"}
              ml={["5px","25px"]}
            >
               {moment(item?.createdAt).fromNow()}
            </Text>
          </HStack>
          {parse(item?.details)}
          <DiscussionFooter
            followClicked={followClicked}
            downvoteClicked={downvoteClicked}
            upvoteClicked={upvoteClicked}
            item={item}
            setShowReply={setShowReply}
            setItemIdNo={setItemIdNo}
            statusUpdate={statusUpdate} />
        </VStack>
      </HStack>
    </Box>
    </Pressable>
  );
}
