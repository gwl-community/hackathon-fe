import {
  VStack,
  Text,
  HStack,
  Box,
  View,
  ScrollView,
} from "native-base";
import React from "react";
import Image from 'next/image';
const externaImageLoader = ({ src }: { src: string }) =>
  `${src}`;
function StudentTestimonial({ data, title, subtitle, theme, isHomeScreen = false }) {
  return (
    <VStack>
      <Text
        color={theme === "light" ? "#17181a" : "white"}
        fontSize={{
          base: "22px",
          md: "24px",
        }}
        fontFamily="Nunito Sans"
        fontWeight="700"
      >
        {title}
      </Text>
      <Text
        fontSize={{
          base: 15,
          md: "16px",
        }}
        color="#B0B5C0"
        fontFamily="Nunito Sans"
        pt={2}
        mb={2}
      >
        {subtitle}
      </Text>
      <View width="100%">
        <ScrollView
          contentContainerStyle={{ width: "100%" }}
          showsHorizontalScrollIndicator={false}
          horizontal={{
            base: true,
            md: false,
          }}
        // width="100%"
        >
          <HStack
            pt={8}
            space={2}
            justifyContent="space-between"
            flexWrap="nowrap"
            width={{ md: "100%" }}
          >
            {data?.map((item, index) => (
              <TestimonialCard data={item} index={index} theme={theme} isCoursePreview />
            ))}
          </HStack>
        </ScrollView>
      </View>
    </VStack>
  );
}

export function TestimonialCard({ data, index, theme }) {
  return (
    <Box
      key={index}
      // shadow={1}
      bg={theme == "light" ? "#f4f4f4" : "#1D1E21"}
      borderRadius="4px"
      // w="22%"
      // w={[250, 250, '100%']}
      w={{ base: 250, md: "24%" }}
      maxW={"25%"}
      p={"16px"}
    >
      <VStack w="100%">
        <HStack>
          <Image
            loader={externaImageLoader}
            alt={"image"}
            width={"60px"}
            height={"60px"}
            layout={"fixed"}
            src={data?.media?.data?.[0]?.attributes?.url || data?.Image ||  data?.media?.[0]?.url}
          />
          <VStack pl="12px" w='100%' flexShrink={1}>
            <Text
              color={theme === "light" ? "#17181a" : "white"}
              fontSize={["15px", "16px"]}
              fontWeight="700"
              fontFamily="Nunito Sans"
              pb="2px"
            >
              {data?.title || data?.name}
            </Text>
            <Text
              color={theme === "light" ? "#17181a" : "#B0B5C0"}
              fontFamily="Nunito Sans"
              fontSize={"14px"}
            >
              {data?.subTitle || data?.position}
            </Text>
          </VStack>
        </HStack>
        <Image
            loader={externaImageLoader}
            alt={"image"}
            width={"10px"}
            height={"10px"}
            layout={"fixed"}
            src={'https://images.netskill.com/frontend/doubleQuote.svg'}
          />
        <HStack pt="7px">
          <Text
            color={theme === "light" ? "#17181a" : "white"}
            fontSize={"14px"}
            lineHeight="24px"
            fontFamily="Nunito Sans"
            pl="4px"
          >
            {data?.description || data?.about}
          </Text>
        </HStack>
      </VStack>
    </Box>
  );
}
export default StudentTestimonial;