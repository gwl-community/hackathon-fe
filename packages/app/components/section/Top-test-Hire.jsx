import {
    VStack,
    Text,
    Image,
    HStack,
    Box,
    View,
    Button,
    ScrollView,
} from "native-base";
import React from "react";
import { SectionSpacingX, SectionSpacingY } from "../../lib/constants";
import { useRouter } from 'solito/router'

function TopTests({ data, title, theme, nav, props }) {
    const rightArrow = <Image  alt={"image"} tintColor={"black"} source= {{uri:'https://images.netskill.com/frontend/Arrow.svg'}} size={5} top={0.2}/>
    const { push } = useRouter()
    const testData = data
    const ViewAll = () => {
        push('/test/fullstack-development-test')
    }
    return (
        <VStack
            zIndex={2}
            w="100%"
            py={SectionSpacingY}
            px={SectionSpacingX}
            // py={12}
            // px={20}
            bgColor="#f4f4f4"
        >
            <HStack justifyContent={'space-between'}>
                <Text
                    color={theme === "light" ? "#17181a" : "#262627"}
                    fontFamily={'Nunito Sans'}
                    fontSize={{
                        base: "22px",
                        md: "24px",
                    }}
                    fontWeight="700"
                >
                    {title || "Top Tests to Get Hired"}
                </Text>
                <Button
                    endIcon={rightArrow}
                    borderColor={"#17181A"}
                    borderWidth="1"
                    h="40px"
                    w="175px"
                    variant="raised"
                    onPress={ViewAll}
                    justifyContent="center"
                    // py="4.5"
                    px="4"
                    mr="3"
                    _text={{
                        fontFamily: "body",
                        fontSize: "15px",
                        fontWeight: "600",
                        color: "#17181A",
                    }}
                    _hover={{
                        bg: "#17181A",
                        _text: {
                            color: "#ffffff",
                            fontWeight: "600",
                            fontSize: "14px",
                            // fontFamily: "body",
                        },
                    }}
                >
                    {"View All Tests  "}
                </Button>
            </HStack>
            <View width="100%">
                <ScrollView
                    contentContainerStyle={{ width: "100%" }}
                    showsHorizontalScrollIndicator={false}
                    horizontal={{
                        base: true,
                        md: false,
                    }}
                // width="100%"
                >
                    <HStack
                        pt={8}
                        space={2}
                        justifyContent="left"
                        flexWrap="nowrap"
                        width={{ md: "100%" }}
                    >
                       
                        {testData?.map((item, index) => (
                            <TopTestsCard nav={nav} data={item} index={index} theme={theme} props={props} />
                        ))}
                    </HStack>
                </ScrollView>
            </View>
        </VStack >
    );
}

export function TopTestsCard({ data, index, theme, nav, props }) {
    const { push } = useRouter()
    const ViewDetials = (name) => {
        // let slugName = name.toLowerCase().split(' ').join('-');
        let slugName = "fullstack-development-test";
        push(`/test/${slugName}`)
    }

    return (
        <Box
            key={index}
            bg={theme == "light" ? "#f4f4f4" : "#fff"}
            borderRadius="4px"
            w={{ base: 350, md: "23.8%" }}
            maxW={"25%"}
            mr={"8px"}
            style={{ border: "solid 1px #eee" }}
        >
            <VStack w="100%">
                <HStack alignItems={'center'} h={'110px'} justifyContent={'center'} bg={'#FCFAF9'}>
                    <Image
                        width={{
                            md: 60,
                            base: 60,
                        }}
                        height={{
                            md: 60,
                            base: 60,
                        }}
                        resizeMode="contain"
                        source={data?.media?.[0]?.url}
                        alt="profile"
                    />
                </HStack>
                <Text pl="24px" pt='16px'
                    color={"#262627"}
                    fontSize={"15px"}
                    fontWeight="700"
                    fontFamily={'Nunito Sans'}
                >
                    <h3 style={{ marginBottom: 0, marginTop: 0 }}>{data?.title}</h3>
                </Text>
                        <HStack pl="24px" pt='16px'>
                            <Image
                                width={{
                                    md: "20px",
                                    base: 5,
                                }}
                                height={{
                                    md: "20px",
                                    base: 5,
                                }}
                                resizeMode="contain"
                                source={data?.jobMedia?.url}
                                alt="double_quote"
                            />
                            <Text pl={3}
                                color={"#262627"}
                                fontSize={"14px"}
                                lineHeight="24px"
                                fontFamily={'Nunito Sans'}
                            >
                                {data?.jobsTitle}
                            </Text>
                        </HStack>
                        <HStack pl="24px" pt='16px'>
                            <Image
                                width={{
                                    md: "20px",
                                    base: 5,
                                }}
                                height={{
                                    md: "20px",
                                    base: 5,
                                }}
                                resizeMode="contain"
                                source={data?.ctcMedia?.url}
                                alt="double_quote"
                            />
                            <Text pl={3}
                                color={"#262627"}
                                fontSize={"14px"}
                                lineHeight="24px"
                                fontFamily={'Nunito Sans'}
                            >
                                {data?.ctcTitle}
                            </Text>
                        </HStack>

                    {/* )
                } */}
                <HStack mt="30px" mx="24px"
                    mb='20px'>
                    <Button onPress={() =>
                        ViewDetials(data?.title)
                    }
                        width="100%" bg={'#ff5e63'} variant='raised'>
                        <Text
                            color={"#fff"}
                            fontSize={"15px"}
                            lineHeight="24px"
                        >
                            {'View Test Details'}
                        </Text>
                    </Button>
                </HStack>
            </VStack>
        </Box>
    );
}
export default TopTests;

const test = [
    {
        id: 1,
        title: "Fullstack Development Test",
        url: "https://images.netskill.com/frontend/netskill/testpage/testpage/react.svg",
        button: "View Test Details",
        list: [{
            icon: 'ctc',
            label: 'CTC upto 30LPA',
        }, {
            icon: '100+',
            label: '200+ Jobs',
        }]

    },
    {
        id: 2,
        title: "Frontend Development Test",
        url: "https://images.netskill.com/frontend/netskill/testpage/testpage/node.svg",
        button: "View Test Details",
        list: [{
            icon: 'ctc',
            label: 'CTC upto 26LPA',
        }, {
            icon: '100+',
            label: '150+ Jobs',
        }]

    },
    {
        id: 3,
        title: "Backend Development Test",
        url: "https://images.netskill.com/frontend/netskill/testpage/testpage/js.svg",
        button: "View Test Details",
        list: [{
            icon: 'ctc',
            label: 'CTC upto 30LPA',
        }, {
            icon: '100+',
            label: '250+ Jobs',
        }]

    },
    // {
    //     title: "MERN Stack",
    //     url: "https://netskill.s3.ap-south-1.amazonaws.com/frontend/netskill/testpage/testpage/java.svg",
    //     button: "View Test Details",
    //     list: [{
    //         icon: 'ctc',
    //         label: 'CTC upto 18LPA',
    //     }, {
    //         icon: '100+',
    //         label: '200+ Jobs',
    //     }]

    // }
]