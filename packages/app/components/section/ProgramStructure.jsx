import {
  VStack,
  Text,
  // Image,
  HStack,
  View,
  Button,
  Box,
  Hidden
} from "native-base";
import React, { useEffect, useState } from "react";
import { TouchableOpacity } from "react-native";
import { Link } from 'solito/link';
import Image from 'next/image';

const externaImageLoader = ({ src }: { src: string }) =>
  `${src}`;
const rightArrow = <Image
  loader={externaImageLoader}
  alt={"image"}
  width={"12x"}
  height={"12px"}
  layout={"fixed"}
  src={'https://images.netskill.com/frontend/arrow_right2.svg'}
/>
function ProgramStructure({
  data,
  title,
  subtitle,
  shapeImage,
  mainImage,
  handleViewAll,
  showExploreButton,
  isCoursepreview = false,
  dataId,
  removeJobAssistance = false,
}) {

  const [selectedAccordion, setSelectedAccordion] = useState(
    dataId || data?.[0]?.id || -1
  );

  useEffect(() => {
    if (dataId) {
      setSelectedAccordion(dataId)
    }
  }, [dataId]);

  const handleToggleAccordion = (id) => {
    if (id === selectedAccordion) setSelectedAccordion(-1);
    else setSelectedAccordion(id);
  };
  if (!selectedAccordion) {
    return ""
  }
  return (
    <VStack justifyContent="center">
      <Text
        px={{ base: 2, md: 0 }}
        fontSize={{
          base: "22px",
          md: "24px",
        }}
        fontWeight="bold"
        fontFamily="Nunito Sans"
        color="#17181A"
      >
        {title}
      </Text>
      <Text
        px={{ base: 2, md: 0 }}
        fontSize={{
          base: 15,
          md: "16px",
        }}
        color="#17181A"
        fontFamily="Nunito Sans"
        opacity={0.6}
        pt={2}
        mb={2}
      >
        {subtitle}
      </Text>
      <HStack
        pt={10}
        space={4}
        justifyContent="space-between"
        flexWrap="nowrap"
        flexDirection={{ base: "column", md: "row" }}
      >
        <VStack width={{ base: "98%", md: "35%" }}>
          {data?.map((item, index) => (
            <VStack>
              <CustomAccordion
                data={data}
                item={item}
                index={index}
                handleToggleAccordion={handleToggleAccordion}
                selectedAccordion={selectedAccordion}
                isCoursepreview={true}
                removeJobAssistance={removeJobAssistance}
              />
            </VStack>
          ))}
          {showExploreButton ? (
            <VStack mt="24px">
              <Link href={handleViewAll}>
                <Button
                  endIcon={rightArrow}
                  borderColor={"#17181A"}
                  borderWidth="1"
                  h="40px"
                  w="175px"
                  variant="raised"
                  justifyContent="center"
                  // py="4.5"
                  px="4"
                  mr="3"
                  _text={{
                    fontFamily: "Nunito Sans",
                    fontSize: "14px",
                    fontWeight: "600",
                    color: "#17181A",
                  }}
                  _hover={{
                    bg: "#17181A",
                    _text: {
                      color: "#ffffff",
                      fontWeight: "600",
                      fontSize: "14px",
                      fontFamily: "Nunito Sans",
                    },
                  }}
                >
                  {"Explore Courses   "}
                </Button>
              </Link>

            </VStack>
          ) : null}
        </VStack>
        <Hidden from="md">
        <VStack
          width={"100%"}
          mt={{ base: "12px" }}
        >
            <Image
              loader={externaImageLoader}
              alt={"image"}
              width={"360px"}
              height={"350px"}
              layout={"fixed"}
              src={mainImage}
            />
          </VStack>
        </Hidden>
        <Hidden till="md">
        <VStack
          alignItems={{ base: "center", md: undefined }}
          width={{ base: "90%", md: "65%" }}
          mt={{ base: "12px" }}
        >
          <Image
            loader={externaImageLoader}
            alt={"image"}
            width={"540px"}
            height={"500px"}
            layout={"fixed"}
            src={shapeImage}
          />
          <Box position="absolute" mt="40px" >
            <Image
              loader={externaImageLoader}
              alt={"image"}
              width={"620px"}
              height={"410px"}
              layout={"fixed"}
              src={mainImage}
            />
          </Box>
        </VStack>
        </Hidden>
      </HStack>
    </VStack>
  );
}
export function CustomAccordion({
  data,
  item,
  index,
  handleToggleAccordion,
  selectedAccordion,
  isCoursepreview,
  removeJobAssistance
}) {
  return (
    <VStack
      py={5}
      borderBottomWidth={1}
      borderBottomColor={"rgba(176, 181, 192, 0.5)"}
      key={item.id}
      px="18px"
    >
      <TouchableOpacity onPress={() => handleToggleAccordion(item?.id)}>
        <HStack alignItems={"center"} justifyContent="space-between">
          <HStack>
            <Text
              color="#17181A"
              fontSize={"16px"}
              fontWeight="600"
              fontFamily="Nunito Sans"
            >
              {item?.title}
            </Text>
            {index === data.length - 1 && !removeJobAssistance ? (
              <VStack ml={3} bg={"#595fff"} borderRadius={"20px"}>
                <Text
                  fontWeight={"600"}
                  fontSize={"14px"}
                  color={"white"}
                  fontFamily="Nunito Sans"
                  py={1}
                  px={3}
                >
                  100% Placement
                </Text>
              </VStack>
            ) : null}
          </HStack>
            <Image
              loader={externaImageLoader}
              alt={"image"}
              width={"22px"}
              height={"22px"}
              layout={"fixed"}
              src={selectedAccordion === item?.id ? 'https://images.netskill.com/frontend/upArrow.webp': 'https://images.netskill.com/frontend/downArrow.webp'}
            /> 
        </HStack>
      </TouchableOpacity>
      {selectedAccordion === item.id && (
        <View pt="16px">
          <Text color="#17181A" fontSize={"16px"} fontFamily="Nunito Sans">
            {item?.description || item?.content}
          </Text>
        </View>
      )}
    </VStack>
  );
}
export default ProgramStructure;
