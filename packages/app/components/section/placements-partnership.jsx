import {
    VStack,
    Text,
    Image,
    HStack,
    Box,
    Link,
} from "native-base";
import { SectionSpacingX } from "../../lib/constants";
import React from "react";

export default function placementsPartners(data, theme, inwdith) {
    return (
        <HStack
            flexDirection={{ base: "column", md: "row" }}
        >
            <Box
                bg="#17181a"
                h={["370px", "327px"]}
                w={["100%", "60%"]}
                pt={["25", '']}
            >
                <VStack
                    pl={SectionSpacingX}
                    top={{ base: '', md: '70px' }}
                >
                    <Text
                        fontFamily={'Nunito Sans'}
                        fontWeight={"bold"}
                        fontSize={"24px"}

                        w={["100%", "70%"]}
                        color={theme === "light" ? "#17181a" : "#fff"}
                    >{data?.data?.title}
                    </Text>
                    <Text
                        w={["100%", "95%"]}
                        pt={["15px", "15px"]}

                        fontSize={{ base: '16px', md: '16px' }}
                        color={theme === "light" ? "#17181a" : "#fff"}>

                        <Link href={"https://www.goodworksalpha.com/"}
                            isExternal
                            target="blank"
                            isUnderlined={true}
                            alignItems={"center"}
                            justifyContent="center"
                            fontSize="16px"
                            fontWeight="normal"
                            _text={{
                                fontWeight: "normal",
                                fontSize: "16px",
                                fontFamily: 'Nunito Sans'
                            }}>
                            GoodWorks Alpha </Link>
                            {data?.data?.description}
                    </Text>
                </VStack>
            </Box>
            <Image 
            position={'absolute'} 
             left={[170, '58.5%']} 
             zIndex={5000} 
             top={[380, '46%']} 
             alt={"image"}
              source={'https://images.netskill.com/frontend/interview/add.webp'} h={"40px"} w={'40px'} />
            <Box h={["200px", "327px"]} w={["100%", "40%"]} backgroundColor={"rgba(22, 127, 224, 1),rgba(48, 87, 228, 1)"}
                alignItems={'center'}
            >
                <Link href={"https://www.goodworksalpha.com/"} target="blank" isExternal>
                    <Image w={"360px"} h={'64px'} source={data?.data?.media?.url||'https://s3.ap-south-1.amazonaws.com/page-generator-images.netskill.com/partners_444ddb5915.svg'} top={['60px', '130px']} alt="Netskill partners image"/>
                </Link>
            </Box>
        </HStack>
    );
};

