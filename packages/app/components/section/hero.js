import {
  Box,
  Button,
  HStack,
  Image,
  Input,
  Skeleton,
  Text,
  VStack,
} from "native-base";

import React from "react";
import get from "lodash.get";
import isEmpty from "lodash.isempty";

export default function Hero({ data, loading }) {
  const {
    buttons = [],
    description,
    label,
    smallTextWithLink,
    title,
    bg = "#fcfaf9",
    picture = {},
    caption,
    linkText,
    badgeText,
    enquiry = {},
  } = data;
  const url = get(picture, "data.attributes.url", "");
  const alternativeText = get(picture, "data.attributes.alternativeText", "");
  return (
    <VStack
      zIndex={2}
      _light={{
        bg: bg,
      }}
      _dark={{
        bg: bg,
      }}
    >
      <HStack alignItems="center" justifyContent="space-between">
        <VStack
          space="4"
          pl={{
            md: 16,
            base: 4,
          }}
          py={10}
          width={{ md: "50%", base: "100%" }}
        >
          {badgeText && (
            <Skeleton.Text
              isLoaded={loading}
              width={{ md: "30%", base: "30%" }}
              height={"40px"}
              color={"#167fe0"}
              bg={"rgba(22, 127, 224, 0.06)"}
            >
              <Text
                fontSize={{
                  // base: "2em",
                  md: "sm",
                }}
                bg={"rgba(22, 127, 224, 0.06)"}
                color={"#167fe0"}
                borderRadius="20px"
                p={"8px"}
                // color="#ffffff"
                // fontWeight="bold"
                // letterSpacing="0.2"
                textAlign="center"
                bold
                width={{ md: "30%", base: "30%" }}
                mb={-4}
              >
                {badgeText}
              </Text>
            </Skeleton.Text>
          )}
          <Skeleton.Text isLoaded={loading} w={{ md: "55%", base: "100%" }}>
            <Text
              fontSize={{
                // base: "2em",
                md: "3xl",
              }}
              // color="#ffffff"
              // fontWeight="bold"
              // letterSpacing="0.2"
              bold
              width={{ md: "55%", base: "100%" }}
            >
              {title}
            </Text>
          </Skeleton.Text>
          <Skeleton.Text
            isLoaded={loading}
            w={{
              base: "100%",
              md: "75%",
            }}
          >
            <Text
              width={{
                base: "100%",
                md: "75%",
              }}
              fontSize={{
                // base: ".8em",
                md: "sm",
              }}
              // color="#B0B5C0"
              // fontWeight="normal"
              // letterSpacing="0.2"
              noOfLines={3}
            >
              {description}
            </Text>
          </Skeleton.Text>
          <HStack
            space="4"
            py={{
              md: 8,
              base: 8,
            }}
            // pl={{
            //   md: 16,
            //   base: 4,
            // }}
            // py={10}
          >
            {buttons.map((button, index) => {
              let extraProps = {};
              const { leftIcon = {}, rightIcon = {} } = button;
              const leftUrl = get(leftIcon, "data.attributes.url", "");
              const leftAlternativeText = get(
                leftIcon,
                "data.attributes.alternativeText",
                ""
              );
              const rightUrl = get(rightIcon, "data.attributes.url", "");
              const rightAlternativeText = get(
                rightIcon,
                "data.attributes.alternativeText",
                ""
              );
              if (leftIcon) {
                extraProps = {
                  leftIcon: (
                    <Image
                      source={{ uri: leftUrl }}
                      alternativeText={leftAlternativeText}
                      width="24px"
                      height="24px"
                      alt={"image"}
                    />
                  ),
                };
              }
              if (rightIcon) {
                extraProps = {
                  ...extraProps,
                  rightIcon: (
                    <Image
                      source={{ uri: rightUrl }}
                      alternativeText={rightAlternativeText}
                      width="24px"
                      height="24px"
                      alt={"image"}
                    />
                  ),
                };
              }
              return (
                <Skeleton
                  // px="2"
                  rounded="md"
                  startColor="primary.100"
                  isLoaded={loading}
                >
                  <Button
                    
                    size="md"
                    key={index}
                    colorScheme={button.type === "primary" && "success"}
                    variant={button.type !== "primary" ? "outline" : "solid"}
                    {...extraProps}
                  >
                    {button.text}
                  </Button>
                </Skeleton>
              );
            })}
            {!isEmpty(enquiry) && (
              <>
                <Skeleton
                  // px="2"
                  rounded="md"
                  startColor="primary.100"
                  isLoaded={loading}
                >
                  <Input size="md" placeholder={enquiry.textFieldLabel} />
                </Skeleton>
                <Skeleton
                  // px="2"
                  rounded="md"
                  startColor="primary.100"
                  isLoaded={loading}
                >
                  <Button
                    
                    size="md"
                    colorScheme={"success"}
                    variant={"solid"}
                  >
                    {enquiry.buttonLabel}
                  </Button>
                </Skeleton>
              </>
            )}
          </HStack>
          <HStack>
            <Skeleton.Text isLoaded={loading}>
              <Text
                fontSize={{
                  md: "sm",
                }}
              >
                {caption}
              </Text>
            </Skeleton.Text>
            <Skeleton.Text isLoaded={loading}>
              <Text
                fontSize={{
                  md: "sm",
                }}
                color="#1d8ce0"
              >
                {linkText}
              </Text>
            </Skeleton.Text>
          </HStack>
        </VStack>
        {url && (
          <Skeleton
            flex={1}
            w={{ md: "50%", base: "100%" }}
            h={{
              base: "full",
              md: "full",
            }}
            isLoaded={loading}
            rounded="md"
            startColor="coolGray.400"
          >
            <Image
              width={{ md: "50%", base: "100%" }}
              height={{
                base: "full",
                md: "full",
              }}
              alt={"image"}
              source={`${url}`}
              resizeMode="cover"
            />
          </Skeleton>
        )}
      </HStack>
    </VStack>
  );
}
