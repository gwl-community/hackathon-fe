import { Box, Button, HStack, Image, Text, VStack } from "native-base";
import { ImageBackground, StyleSheet, View } from "react-native";

import { API_DOMAIN } from "../../config/constants";
import React from "react";
import get from "lodash.get";

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    height: "300px",
    flexDirection: "column",
  },
});

export default function BottomActions({ data }) {
  const { buttons = [], title, bg = "#1d8ce0", bgImage = {} } = data;
  const url = get(bgImage, "data.attributes.url", "");
  const alternativeText = get(bgImage, "data.attributes.alternativeText", "");
  return (
    <View style={styles.container}>
      <ImageBackground
        source={{ uri: url }}
        // alternativeText={alternativeText}
        resizeMode="cover"
        style={styles.image}
        alternativeText={alternativeText}
      >
        <VStack
          space="4"
          // pl={{
          //   md: 16,
          //   base: 4,
          // }}
          py={16}
          // bgImage={{uri: url ? url : bg}}
          // bgImage={url}
          // source={{uri:url}}
          width={{ md: "100%", base: "100%" }}
          height={{ md: "100%", base: "100%" }}
          // _light={{
          //   // bg,
          //   backgroundImage:url
          //   // opacity: 0.8,
          // }}
          // _dark={{
          //   // bg,
          //   backgroundImage:url
          //   // opacity: 0.8,
          // }}
          alignItems="center"
          justifyContent="center"
        >
          <Text
            fontSize={{
              md: "3xl",
            }}
            bold
            color={"#ffffff"}
          >
            {title}
          </Text>

          <HStack
            space="4"
            py={{
              md: 4,
              base: 4,
            }}
          >
            {buttons.map((button, index) => {
              return (
                <Button
                  
                  size="md"
                  bg={button.type !== "primary" ? "" : "white"}
                  // border="solid 1px #262627"
                  //   bg="#ff5e63"
                  // borderColor={button.type !== "primary" ? "" : "white"}
                  // variant={button.type !== "primary"?"outline":"solid"}

                  key={index}
                >
                  {button.text}
                </Button>
              );
            })}
          </HStack>
        </VStack>
      </ImageBackground>
    </View>
  );
}
