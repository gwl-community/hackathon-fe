import { Box, FlatList, ScrollView, Text, VStack } from "native-base";
import React from "react";
import Image from 'next/image';
const externaImageLoader = ({ src }: { src: string }) =>
  `${src}`;

function HiringPartner({ data, title, subtitle, resizeMode, isHiringPartner = false, isCollege }) {
 
  const imageArr = data?.map((img) => {
    return isHiringPartner ? img?.media?.url : img
  })
 

  const renderItem = ({ item }) => {
    return (
      <Box mt={3} px={{ base: 8, md: 6 }} pb={[0, 0, 3]}>
           <Image
            loader={externaImageLoader}
            alt={"image"}
            width={isCollege? "180px" : "165px"}
            height={"70px"}
            layout={"fixed"}
            src={item}
          />
      </Box>
    );
  };

  return (
    <VStack justifyContent="center">
      <Text
        fontSize={{
          base: "14.5px",
          md: "15px",
        }}
        fontFamily="Nunito Sans"
        fontStyle={"normal"}
        fontWeight={700}
      >
        <h2>{title}</h2>
      </Text>
      <Text
        fontSize={{
          base: "15px",
          md: "16px",
        }}
        color="#B0B5C0"
        fontWeight={600}
        fontFamily="Nunito Sans"
        fontStyle="normal"
        pt={0}
        mb={2}
      >
        {subtitle}
      </Text>
      <Box pt={7} pb={0} w={"100%"} alignItems={[undefined, "center"]}>
        <ScrollView
          contentContainerStyle={{ justifyContent: "space-around" }}
          showsHorizontalScrollIndicator={false}
          horizontal={{
            base: true,
            md: true,
          }}
        >
          <FlatList
            style={{ width: "100%", paddingHorizontal: isCollege ? 40 : null }}
            data={imageArr}
            numColumns={5}
            renderItem={renderItem}
            keyExtractor={(item) => item.id}
          />
        </ScrollView>
      </Box>
    </VStack>
  );
}
export default HiringPartner;
