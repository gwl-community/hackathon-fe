import { VStack, Image } from "native-base";
import React from "react";

export default function Loader() {
  return (
    <VStack
      w="100%"
      h="100%"
      alignItems={"center"}
      justifyContent="center"
      bg="#17181a"
    >
      <Image
        h={{ base: "50%", md: "20%" }}
        w={{ base: "50%", md: "20%" }}
        alt="NetSkill"
        resizeMode="contain"
        source={{uri:"https://images.netskill.com/frontend/netskill/b2b/netskill_updated_logo.webp"}
          
        }
      />
    </VStack>
  );
}
