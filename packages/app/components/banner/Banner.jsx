import React from "react";
import { VStack, Text, Box, Button, Link } from "native-base";
import Image from 'next/image';
export default function Banner({
  title,
  height,
  buttonText,
  buttonOnClick,
  buttonWidth,
  image,
  onPressed
}) {
  const externaImageLoader = ({ src }: { src: string }) =>
    `${src}`;
  return (
    <VStack>
      <Box justifyContent="center" alignItems="center" flexDirection="row">
        <Image
          loader={externaImageLoader}
          alt={"image"}
          width={"1770px"}
          height={"300px"}
          layout={"fixed"}
          src={image ||
            "https://images.netskill.com/frontend/netskill/contact/banner-bg.webp"}
        />
        <VStack position="absolute" mt={{ base: '5px', md: '12px' }}>
          <Text
            fontSize={{ base: "12px", md: "16px" }}
            fontWeight={{ base: "600", md: "800" }}
            mt={{ base: '3px', md: '1px' }}
            // mb={{base:'',md:'0px'}}
            textAlign={"center"}
            fontFamily={"body"}
            //mb={{base:'',md:'3px'}}
            style={{ fontWeight: "800", fontFamily: "Nunito Sans" }}
          // right={0}
          >
            <h1
              style={{ marginTop: '0px', marginBottom: '0px', color: 'rgb(255, 255, 255)' }}
            >
              {title}
            </h1>
          </Text>
          {
            buttonText &&

            (
              <Link href={buttonOnClick}
                style={{ alignSelf: 'center' }}>

                <Button
                  minW={buttonWidth || "205"}
                  py="3"
                  mt={{ base: "28px", md: "47.5px" }}
                  variant="raised"
                  width="13%"
                  onPress={onPressed || null}
                  size="md"
                  bg="white"
                  alignSelf={"center"}
                  _text={{
                    color: "#ff5e63",
                    fontSize: 14,
                    fontFamily: "Nunito Sans",
                  }}
                  fontFamily="Nunito Sans"
                // _hover={{
                //   bg: "error.500",
                // }}
                >
                  {buttonText}
                </Button>
              </Link>


            )
          }
        </VStack>
      </Box>
    </VStack>
  );
}
