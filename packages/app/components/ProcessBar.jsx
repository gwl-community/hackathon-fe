
import React from 'react';
import { HStack, Icon, Text, VStack, Pressable, useColorMode, Button, Progress } from 'native-base';
import { MaterialIcons } from '@expo/vector-icons';


function ProcessBar(props) {
  return <VStack
    // mt='8px'
    marginBottom='5'
    _light={{
      bg: 'white',

    }} _dark={{
      bg: {
        base: 'coolGray.800',
        md: 'coolGray.900'
      },
      borderColor: 'coolGray.800'
    }}

    borderRadius="6">
    <HStack alignItems="center" space={{
      base: 4,
      md: 10
    }}>
      <VStack flex={1}>
        <Progress h="1.5" 
         bg="#20392f" _filledTrack={{
          bg: "#19bf79"
        }}
        max={props?.max}
        value={props?.value} 
        width="100%" 
        rounded="none"
          />
      </VStack>
    </HStack>
  </VStack>;
}

export default ProcessBar;