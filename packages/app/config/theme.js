import { extendTheme, theme as nbTheme } from "native-base";

const theme = extendTheme({
  colors: {
    // Add new color
    primary: {
      50: "#E3F2F9",
      100: "#C5E4F3",
      200: "#A2D4EC",
      300: "#7AC1E4",
      400: "#47A9DA",
      500: "#0088CC",
      600: "#007AB8",
      700: "#ff5e63",
      800: "#005885",
      900: "#003F5E",
    },
    // Redefinig only one shade, rest of the color will remain same.
    amber: {
      400: "#d97706",
    },
    _hover: {
      borderColor: "#00b",
      backgroundColor: "red"
    }
  },
  config: {
    // Changing initialColorMode to 'dark'
    initialColorMode: "dark",
  },
  // fontConfig: {
  //   NunitoSans: {
  //     // 100: {
  //     //   normal: "NunitoSans-Light",
  //     //   italic: "NunitoSans-LightItalic",
  //     // },
  //     // 200: {
  //     //   normal: "NunitoSans-Light",
  //     //   italic: "NunitoSans-LightItalic",
  //     // },
  //     // 300: {
  //     //   normal: "NunitoSans-Light",
  //     //   italic: "NunitoSans-LightItalic",
  //     // },
  //     // 400: {
  //     //   normal: "NunitoSans-Regular",
  //     //   italic: "NunitoSans-Italic",
  //     // },
  //     // 500: {
  //     //   normal: "NunitoSans-Medium",
  //     // },
  //     // 600: {
  //     //   normal: "NunitoSans-Medium",
  //     //   italic: "NunitoSans-MediumItalic",
  //     // },
  //     // // Add more variants
  //     // 700: {
  //     //   normal: "NunitoSans-Bold",
  //     // },
  //     // 800: {
  //     //   normal: "NunitoSans-Bold",
  //     //   italic: "NunitoSans-BoldItalic",
  //     // },
  //     // 900: {
  //     //   normal: "NunitoSans-Bold",
  //     //   italic: "NunitoSans-BoldItalic",
  //     // },
  //     100: {
  //       normal: "NunitoSans_200ExtraLight",
  //       italic: "NunitoSans_200ExtraLight_Italic",
  //     },
  //     200: {
  //       normal: "NunitoSans_200ExtraLight",
  //       italic: "NunitoSans_200ExtraLight_Italic",
  //     },
  //     300: {
  //       normal: "NunitoSans_300Light",
  //       italic: "NunitoSans_300Light_Italic",
  //     },
  //     400: {
  //       normal: "NunitoSans_400Regular",
  //       italic: "NunitoSans_400Regular_Italic",
  //     },
  //     500: {
  //       normal: "NunitoSans_600SemiBold",
  //       italic: "NunitoSans_600SemiBold_Italic",
  //     },
  //     600: {
  //       normal: "NunitoSans_600SemiBold",
  //       italic: "NunitoSans_600SemiBold_Italic",
  //     },
  //     // Add more variants
  //     700: {
  //       normal: "NunitoSans_700Bold",
  //       italic: "NunitoSans_700Bold_Italic",
  //     },
  //     800: {
  //       normal: "NunitoSans_800ExtraBold",
  //       italic: "NunitoSans_800ExtraBold_Italic",
  //     },
  //     900: {
  //       normal: "NunitoSans_900Black",
  //       italic: "NunitoSans_900Black_Italic",
  //     },
  //   },
  // },

  // // Make sure values below matches any of the keys in `fontConfig`
  // fonts: {
  //   heading: "NunitoSans",
  //   body: "NunitoSans",
  //   mono: "NunitoSans",
  // },
});

export default theme;
