
import { Helmet } from 'react-helmet';
import React from 'react';
const Seo = ({ title, description, pathSlug }) => {
  const url = `https://www.netskill.com/${pathSlug}`
	return (
<Helmet htmlAttributes={{ lang: 'en' }} title={title} meta={[
        {
          name: 'description',
          content: description,
        },
        {
          property: 'og:title',
          content: title,
        },
        {
          property: 'title',
          content:" Netskill Academy",
        },
        {
          name: 'google-site-verification',
          content: "tt_fasyXz2IuJtvhY4uY_ciQZq4jUNsF9c8NkmDvUlM",
        },
        // {
        //   name: 'keywords',
        //   content: keywords.join(),
        // },
		]}
    links={[
     {
          rel: 'canonical',
          href: url,
      },
    ]}
    />
 );
}
export default Seo;