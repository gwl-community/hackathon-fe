import React from "react";
import { Helmet } from "react-helmet";
// import RenderHtml from 'react-native-render-html';
import parse from 'html-react-parser';

export default function Meta({ title, description }) {
  const source =  `<Helmet><html lang="en" >
        <head>
          <meta charSet="utf-8" />
          <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1.00001,viewport-fit=cover" />
          <meta name="description" content=${description} />
          <title >${title}</title>
          <link rel="canonical" href="https://www.netskill.com/" />
          <meta name="twitter:card" content="Innovative Ed-Tech startup, built on AI that delivers personalized coaching to students & working individuals." />
          <meta property="twitter:domain" content="netskill.com" />
          <meta property="twitter:url" content="https://www.netskill.com/" />
          <meta name="twitter:title" content="Online Courses - Learn Anything, On Your Schedule | Netskill" />
          <meta name="twitter:description" content="Innovative Ed-Tech startup, built on AI that delivers personalized coaching to students & working individuals." />
          <meta name="twitter:image" content="https://images.netskill.com/NetSkill_logo.png" />
          <meta name="keywords" content="HTML, CSS, JavaScript, Fullstack Development Course, Backend Development Course, Frontend Development Course,Online Courses" />


          <meta property="og:url" content="https://www.netskill.com/" />
          <meta property="og:type" content="website" />
          <meta property="og:title" content="Online Courses - Learn Anything, On Your Schedule" />
          <meta property="og:description" content="NetSkill.com is a revolutionary Ed-tech Startup that delivers online courses to students, engineers and working individuals." />
          <meta property="og:image" content="https://images.netskill.com/NetSkill_logo.png" />

        </head>
      </html>
      </Helmet>`

  if (window) {
    return (
      <>
        {parse(source)}
      </>

      // <RenderHtml
      // source={source}/>
    );
  }
  return <></>;
}
