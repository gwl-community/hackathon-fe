import { NavigationProvider } from './navigation'
import React from 'react'
import theme from '../config/theme'
//import * as Font from 'expo-font'
// import {
//   //useFonts,
//   NunitoSans_200ExtraLight,
//   NunitoSans_200ExtraLight_Italic,
//   NunitoSans_300Light,
//   NunitoSans_300Light_Italic,
//   NunitoSans_400Regular,
//   NunitoSans_400Regular_Italic,
//   NunitoSans_600SemiBold,
//   NunitoSans_600SemiBold_Italic,
//   NunitoSans_700Bold,
//   NunitoSans_700Bold_Italic,
//   NunitoSans_800ExtraBold,
//   NunitoSans_800ExtraBold_Italic,
//   NunitoSans_900Black,
//   NunitoSans_900Black_Italic,
// } from '@expo-google-fonts/nunito-sans';
import { NativeBaseProvider,extendTheme } from 'native-base'
//import { useFonts } from 'expo-font';
// const theme = extendTheme({
 
//   fontConfig: {
//     NunitoSans: {
//       // 100: {
//       //   normal: "NunitoSans-Light",
//       //   italic: "NunitoSans-LightItalic",
//       // },
//       // 200: {
//       //   normal: "NunitoSans-Light",
//       //   italic: "NunitoSans-LightItalic",
//       // },
//       // 300: {
//       //   normal: "NunitoSans-Light",
//       //   italic: "NunitoSans-LightItalic",
//       // },
//       // 400: {
//       //   normal: "NunitoSans-Regular",
//       //   italic: "NunitoSans-Italic",
//       // },
//       // 500: {
//       //   normal: "NunitoSans-Medium",
//       // },
//       // 600: {
//       //   normal: "NunitoSans-Medium",
//       //   italic: "NunitoSans-MediumItalic",
//       // },
//       // // Add more variants
//       // 700: {
//       //   normal: "NunitoSans-Bold",
//       // },
//       // 800: {
//       //   normal: "NunitoSans-Bold",
//       //   italic: "NunitoSans-BoldItalic",
//       // },
//       // 900: {
//       //   normal: "NunitoSans-Bold",
//       //   italic: "NunitoSans-BoldItalic",
//       // },
//       100: {
//         normal: "NunitoSans_200ExtraLight",
//         italic: "NunitoSans_200ExtraLight_Italic",
//       },
//       200: {
//         normal: "NunitoSans_200ExtraLight",
//         italic: "NunitoSans_200ExtraLight_Italic",
//       },
//       300: {
//         normal: "NunitoSans_300Light",
//         italic: "NunitoSans_300Light_Italic",
//       },
//       400: {
//         normal: "NunitoSans_400Regular",
//         italic: "NunitoSans_400Regular_Italic",
//       },
//       500: {
//         normal: "NunitoSans_600SemiBold",
//         italic: "NunitoSans_600SemiBold_Italic",
//       },
//       600: {
//         normal: "NunitoSans_600SemiBold",
//         italic: "NunitoSans_600SemiBold_Italic",
//       },
//       // Add more variants
//       700: {
//         normal: "NunitoSans_700Bold",
//         italic: "NunitoSans_700Bold_Italic",
//       },
//       800: {
//         normal: "NunitoSans_800ExtraBold",
//         italic: "NunitoSans_800ExtraBold_Italic",
//       },
//       900: {
//         normal: "NunitoSans_900Black",
//         italic: "NunitoSans_900Black_Italic",
//       },
//     },
//   },
//   Text:{
    
//       fontFamily: "nunitoSans",
//       fontStyle: "normal",
//       fontWeight: "bold",
//       fontSize: 40,
//       lineHeight: "56px",
//       color: "#ffffff",
    

//   },

//   // Make sure values below matches any of the keys in `fontConfig`
//   fonts: {
//     heading: "NunitoSans",
//     body: "NunitoSans",
//     mono: "NunitoSans",
//   },
//   // Make sure values below matches any of the keys in `fontConfig`

// });

export function Provider({ children }) {
  // let [fontsLoaded, error] = useFonts({
  //   NunitoSans_400Regular,
  //   NunitoSans_600SemiBold,
  //   NunitoSans_700Bold,
  //   NunitoSans_800ExtraBold,
  // });
  return (
    <NativeBaseProvider theme={theme}>
    <NavigationProvider isSSR>
      {children}
    </NavigationProvider>
    </NativeBaseProvider>
  )
}
