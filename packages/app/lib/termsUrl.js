export const TermsUrl = (host) => {
  let urlString = 'dev-v2'
  if (
    
    host?.includes('www.netskill.com') ||
    host?.includes('https://netskill.com')
  ) {
    urlString = 'www'
  }
  let termsUrl = `https://${urlString}.netskill.com/terms`
  let privacyUrl = `https://${urlString}.netskill.com/privacy`
  return { termsUrl, privacyUrl }
}
