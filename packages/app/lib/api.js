import axios from "axios";
import { getToken } from "./utils";
let env = "-dev";
export let environment = 'DEV';
if ( typeof window !== 'undefined') {
  const origin = (typeof window).origin;
  if (origin?.includes("staging")) {
    env = "-staging";
    environment = 'DEV';
  } else if (
    origin?.includes("prod") ||
    origin?.includes("www.netskill.com") ||
    origin?.includes("https://netskill.com")
  ) {
    environment = "PRODUCTION";
    env = "";
  }
}
export const currentEnv = env;
export const baseURL = `https://cms${env}.netskill.com/`;
const api = axios.create({ baseURL });

api.interceptors.request.use(
  async (config) => {
    const token = await getToken();
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export const toFormData = (object) => {
  const formData = new FormData();
  Object.keys(object).forEach((key) => {
    formData.append(key, object[key]);
  });
  return formData;
};

export default api;
