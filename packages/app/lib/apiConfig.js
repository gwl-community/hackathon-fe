export const getBaseUrls=(host)=>{
    let urlString= "-dev";
    let environment = 'DEV';
    if (host?.includes("staging")) {
        urlString= "-staging";
        environment = 'DEV';
    } else if (
        host?.includes("prod") ||
        host?.includes("www.netskill.com") ||
        host?.includes("https://netskill.com")
    ) {
        environment = "PRODUCTION";
        urlString= "";
    }
    let cmsBaseUrl=`https://cms${urlString}.netskill.com/`
    let pageGeneratorBaseUrl=`https://page-generator${urlString}.netskill.com/`;

    return {cmsBaseUrl,pageGeneratorBaseUrl}
}