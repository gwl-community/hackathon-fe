import axios from "axios";
import { getToken } from "./utils";
let env = "-dev";
export let environment = 'DEV';
if (typeof window !== 'undefined') {
  const origin = (typeof window).origin;
  if (origin?.includes("staging")) {
    env = "-staging";
    environment = 'DEV';
  } else if (
    origin?.includes("prod") ||
    origin?.includes("www.netskill.com") ||
    origin?.includes("https://netskill.com")
  ) {
    environment = "PRODUCTION";
    env = "";
    // const script = window.document.createElement("script")
    // script.src = `https://www.googletagmanager.com/gtag/js?id=G-F2YCLTGE2B`;
    // script.async = true
    // window.document.body.appendChild(script)
    // window.dataLayer = window.dataLayer || []
    // gtag('js', new Date())
    // gtag('config', 'G-F2YCLTGE2B')
    // gtag('event',
    //   'test', {
    //   event_category: 'Netskill',
    //   event_label: 'home-page'
    // }
    // )
  }
}

export const currentEnv = env;
export const baseURL = `https://page-generator${env}.netskill.com/`;
const page_generator_api = axios.create({ baseURL });

page_generator_api.interceptors.request.use(
  async (config) => {
    const token = await getToken();
   
    // if (!config.headers.Authorization && token != null) {
    //   config.headers.Authorization = `Bearer ${token}`;
    // }

    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);
export const toFormData = (object) => {
  const formData = new FormData();
  Object.keys(object).forEach((key) => {
    formData.append(key, object[key]);
  });
  return formData;
};

export default page_generator_api;
