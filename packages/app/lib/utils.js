import AsyncStorage from "@react-native-async-storage/async-storage";
import { monthsIn3Words } from "./constants";
import moment from "moment";

export const storeData = async (key, value) => {
  try {
    await AsyncStorage.setItem(key, value);
  } catch (e) {
    console.log("error while storing data in async storage");
  }
};

export const getData = async (key, isObject) => {
  try {
    const value = await AsyncStorage.getItem(key);
    return isObject ? JSON.parse(value) : value;
  } catch (e) {
    console.log("error while reading data in async storage");
  }
};
let keys = ['token', 'userData'];
export const clearData = () => {
  try {
    // try {
    //   await AsyncStorage.clear();
    // } catch (e) {
    //   console.log("error while clearing data in async storage");
    // }
    AsyncStorage.multiRemove(keys).then((res) => {
     
    });
  }
  catch (exception) {
    console.log(exception)
  }
};

export const getToken = async () => {
  try {
    const value = await AsyncStorage.getItem("token");
    return value;
  } catch (e) {
    console.log("error while reading data in async storage");
  }
};

export const validateEmail = (email) => {
  var regExp = /\S+@\S+\.\S+/;
  return !regExp.test(email);
};
export const validatePhone = (phone) => {
  var regExp = /^[0-9]{1,10}$/;
  return !regExp.test(phone) || phone?.length < 10;
};

export const convertDate = (date) => {
  if (date) {
    let day = date.substring(8, 10);
    let month = date.substring(5, 7);
    let year = date.substring(0, 4);
    return day + " " + monthsIn3Words[parseInt(month) - 1] + ", " + year;
  }
  return date;
};

export const getCalclulatedDate = (x) => {
  let d = new Date(x);
  if(d < new Date()){
  d = new Date();
  d.setDate(d.getDate() + (((1 + 7 - d.getDay()) % 7) || 7));
  d = moment(d).format('DD MMM, YYYY')
  }
  else if(d >= new Date()) {
    d=x;
  }
  return d
}