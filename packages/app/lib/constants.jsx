export const SectionSpacingY = { base: 12, md: 20 };
export const SectionSpacingX = {
  lg: "120px",
  md: "20px",
  base: 3,
};
export const openInNewTab = url => {
  window.open(url, '_blank', 'noopener,noreferrer');
};
export const API_DOMAIN = `https://page-generator-dev.netskill.com`
export const monthsIn3Words = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec",
];

export const steps = [
  {
    title: "Select a course",
    content:
      "Choose a career path, sign up for a mastery course. Multiple payment options, EMIs, scholarships available. We never turn down a deserving candidate.",
    bg_image:
      "https://images.netskill.com/frontend/netskill/home/step1_bg.svg",
    image:
      "https://images.netskill.com/frontend/netskill/home/step1.svg",
  },
  {
    title: "Learn at your pace, guided by mentors",
    content:
      "Top-quality video content guided by our powerful AI-platform. Connect with mentors to clear doubts. Practice, Test & Repeat till you are perfect.",
    image:
      "https://images.netskill.com/frontend/netskill/home/step2.svg",
    bg_image:
      "https://images.netskill.com/frontend/netskill/home/step2_bg.svg",
  },
  {
    title: "Get certified and unlock placements",
    content:
      "Finish the course, crack the tests, get certified and unlock matched placement opportunities. Get interview prep guidance.",
    image:
      "https://images.netskill.com/frontend/netskill/home/step3.svg",
    bg_image:
      "https://images.netskill.com/frontend/netskill/home/step3_bg.svg",
  },
  {
    title: "Get hired",
    content:
      "100+ top companies globally are hiring on Netskill. Now, it’s your time to shine and get that top job.",
    image:
      "https://images.netskill.com/frontend/netskill/home/step4.svg",
    bg_image:
      "https://images.netskill.com/frontend/netskill/home/step4_bg.svg",
    showButton: true,
  },
];
export const college_steps = [
  {
    title: "Register for Netskill Enterprise Account for Colleges",
    content:
      "The AI-driven platform helps you onboard students at bulk, provide courses, monitor their progress and performance.",
    bg_image:
      "https://images.netskill.com/frontend/netskill/home/step1_bg.svg",
    image:
      "https://images.netskill.com/frontend/netskill/home/step1.svg",
  },
  {
    title: "Assign Courses to Students",
    content:
      "Students take self-paced courses along with mentor & tutor support. Once they clear all the unit level assessments, they qualify for placements at top companies.",
    image:
      "https://images.netskill.com/frontend/netskill/home/step2.svg",
    bg_image:
      "https://images.netskill.com/frontend/netskill/home/step2_bg.svg",
  },
  {
    title: "Degree Projects & Internships",
    content:
      "Students get access to live projects & internships on Netskill platform while completing their courses. This enables them to get hands-on and job ready.",
    image:
      "https://images.netskill.com/frontend/netskill/home/step3.svg",
    bg_image:
      "https://images.netskill.com/frontend/netskill/home/step3_bg.svg",
  },
  {
    title: "Job Placements",
    content:
      "Get access to 100s of top companies who hire certified students on Netskill. Your students now are on par with the top colleges across the world.",
    image:
      "https://images.netskill.com/frontend/netskill/home/step4.svg",
    bg_image:
      "https://images.netskill.com/frontend/netskill/home/step4_bg.svg",
    showButton: true,
  },
];
export const placement_steps = [
  {
    title: "Take a Netskill Placement Test",
    content:
      "You can choose the role or skill that you prefer, take a test at your convenience.",
    bg_image:
      "https://images.netskill.com/frontend/netskill/home/step1_bg.svg",
    image:
      "https://images.netskill.com/frontend/netskill/home/step1.svg",
  },
  {
    title: "Unlock Jobs",
    content:
      "Once you clear the tests, you will qualify for the job interviews at top companies. 100+ top companies are recruiting from Netskill.",
    image:
      "https://images.netskill.com/frontend/netskill/home/step2.svg",
    bg_image:
      "https://images.netskill.com/frontend/netskill/home/step2_bg.svg",
  },
  {
    title: "Select the Best Job",
    content:
      "Depending on the offers you get, you can choose the best company & job that fits you.",
    image:
      "https://images.netskill.com/frontend/netskill/home/step3.svg",
    bg_image:
      "https://images.netskill.com/frontend/netskill/home/step3_bg.svg",
  },
  {
    title: "Courses to help you perform better",
    content:
      "No problem if you don’t perform well, you can take one of Netskill courses to upskill & attempt the test again.",
    image:
      "https://images.netskill.com/frontend/netskill/home/step4.svg",
    bg_image:
      "https://images.netskill.com/frontend/netskill/home/step4_bg.svg",
    showButton: false,
  },
];
export const programStructure = [
  {
    id: 1,
    title: "Video Lessons",
    content:
      "Our byte-sized videos are 5-10 minutes each, intended to optimise student's engagement and grasping power. Further, build proficiency with practice tests.",
  },
  {
    id: 2,
    title: "Quiz & Assessments",
    content:
      "Take a quiz after every lesson to validate your learning. Weekly & Unit level assessments help you unlock newer units and jobs.",
  },
  {
    id: 3,
    title: "Mentor Support",
    content:
      "Mentors guide you to understand the concepts and crack tests with Weekly guided exercises & industry projects.",
  },
  {
    id: 4,
    title: "Doubt Resolution",
    content:
      "Ask questions & get responses from mentors & students. If you need further help, connect with your designated mentor.",
  },
  {
    id: 5,
    title: "Interview Prep & Mock",
    content:
      "Our platform provides you coding challenges, mock interviews & assessments to ensure you crack the job interview process.",
  },
  {
    id: 6,
    title: "Job Placements",
    content:
      "Once you crack all the unit level assessments & complete industry projects, we will assist you with jobs. We have a 100% track record!",
  },
];

export const hiringPartners = [
  "https://images.netskill.com/frontend/netskill/home/goodwork.svg",
  "https://images.netskill.com/frontend/netskill/home/flipkrt.svg",
  "https://images.netskill.com/frontend/netskill/home/dec.svg",
  "https://images.netskill.com/frontend/netskill/home/cf.svg",
  "https://images.netskill.com/frontend/netskill/home/mb.svg",
  "https://images.netskill.com/frontend/netskill/home/cred.svg",
  "https://images.netskill.com/frontend/netskill/home/myntra.svg",
  "https://images.netskill.com/frontend/netskill/home/phonepe.svg",
  "https://images.netskill.com/frontend/netskill/home/mahindra.svg",
  "https://images.netskill.com/frontend/netskill/home/licious.svg",
];
export const collegePartners = [
  "https://images.netskill.com/frontend/netskill/b2b/bitm_logo.webp",
  "https://images.netskill.com/frontend/netskill/b2b/rv_logo.webp",
  "https://images.netskill.com/frontend/netskill/b2b/bitm_logo.webp",
  "https://images.netskill.com/frontend/netskill/b2b/rv_logo.webp",
  "https://images.netskill.com/frontend/netskill/b2b/bitm_logo.webp",
];
